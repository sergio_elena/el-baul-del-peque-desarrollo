{if $page_name == "index"}
{if $theme_settings.newsletter == 1}
<!-- Block Newsletter module-->
<div id="newsletter_block_ext" class="block">
	<div class="col social">
		<div class="content">
			<img class="soc_img" src="{$image_soc}" alt="" />
			<div class="opacity_bg"></div>
			<div class="opaque">
				<div class="indent">
				<h4>{l s='Socialize With Us!' mod='pk_newsletter'}</h4>
				<p>{l s='Subscribe to the latest news from your favorite brands.' mod='pk_newsletter'}</p>
				<ul>
					<li class="youtube"><a class="icon" href="http://www.youtube.com/user/{$youtube_url}"></a></li>
					<li class="twitter"><a class="icon" href="https://twitter.com/#!/{$twitter_url}"></a></li>
					<li class="facebook"><a class="icon" href="http://www.facebook.com/{$facebook_url}"></a></li>
					<li class="google_plus"><a class="icon" href="https://plus.google.com/u/0/{$gplus_url}/posts"></a></li>
				</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="col nwltr">
		<div class="content">
			<img class="newsletter_img" src="{$image_news}" alt="" />
			<div class="opacity_bg"></div>
			<div class="opaque">
				<div class="indent">
				<h4>{l s='Become aVIP member' mod='pk_newsletter'}</h4>
				{if isset($msg) && $msg}
					<span class="{if $nw_error}warning_inline{else}success_inline{/if}">{$msg}</span>
				{else}
					<p>{l s='Sign up to get exclusive offers from your favorite brands!' mod='pk_newsletter'}</p>
				{/if}
				<form action="{$link->getPageLink('index')}" method="post" class="newsletter_form">
					<div class="ind">
						{* @todo use jquery (focusin, focusout) instead of onblur and onfocus *}
						<input type="text" name="email" size="18" 
							value="{if isset($value) && $value}{$value}{else}{l s='your e-mail' mod='pk_newsletter'}{/if}" 
							onfocus="javascript:if(this.value=='{l s='your e-mail' mod='pk_newsletter'}')this.value='';" 
							onblur="javascript:if(this.value=='')this.value='{l s='your e-mail' mod='pk_newsletter'}';" 
							class="inputNew" />
						<!--<select name="action">
							<option value="0"{if isset($action) && $action == 0} selected="selected"{/if}>{l s='Subscribe' mod='blocknewsletter'}</option>
							<option value="1"{if isset($action) && $action == 1} selected="selected"{/if}>{l s='Unsubscribe' mod='blocknewsletter'}</option>
						</select>-->
							<input type="submit" value="{l s='Send' mod='pk_newsletter'}" class="minibutton" name="submitNewsletter" />
						<input type="hidden" name="action" value="0" />
					</div>
				</form>
				</div>
			</div>
		</div>
		
	</div>
	<div class="col promo">
		<a href="{$adv_link}" title="{$adv_title}">
			<img src="{$image}" alt="{$adv_title}" title="{$adv_title}" width="406" height="406" />
		</a>
	</div>
</div>
<!-- /Block Newsletter module-->
{/if}
{/if}