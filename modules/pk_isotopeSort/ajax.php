<?php
require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');
include(dirname(__FILE__).'/pk_isotopeSort.php');

$isotopeSort = new pk_isotopeSort();

if (Tools::getValue('type')) {
	$isotopeSort->ajaxCall(Tools::getValue('type'));
}
if (Tools::getValue('cID')) {
	$isotopeSort->getProductsFromCategory(Tools::getValue('cID'));
}
if (Tools::getValue('pID')) {
	$isotopeSort->saveData(Tools::getValue('pID'));
}
if (Tools::getValue('rem_pID')) {
	$isotopeSort->removeData(Tools::getValue('rem_pID'));
}
?>