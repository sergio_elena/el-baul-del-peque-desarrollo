<?php

if (!defined('_PS_VERSION_'))
	exit;

class pk_isotopeSort extends Module
{
	private $_html = '';
	private $_postErrors = array();

	function __construct()
	{
		$this->name = 'pk_isotopeSort';
		$this->tab = 'front_office_features';
		$this->version = '1.4';
		$this->author = '';
		$this->need_instance = 0;
		$this->DBtable = _DB_PREFIX_.'pk_isotope';
		$this->bootstrap = true;

		parent::__construct();

		$this->displayName = $this->l('Isotope Filter');
		$this->description = $this->l('Displays featured, new, special products on your homepage.');
	}

	function install()
	{
		if (!Configuration::updateValue('ISOTOPE_NBR', 8) || 
			!Configuration::updateValue('ISOTOPE_ADD_METHOD', 1) || 
			!parent::install() || 
			//!$this->registerHook('displayHome') || 
			!$this->registerHook('hook_home_05') || 
			!$this->registerHook('header'))
			return false;

		Db::getInstance()->Execute('DROP TABLE IF EXISTS `'.$this->DBtable.'`');

		if (!Db::getInstance()->Execute('
				CREATE TABLE `'.$this->DBtable.'` (
					`id` int(10) unsigned NOT NULL AUTO_INCREMENT, 
					`data` VARCHAR(100), 
					PRIMARY KEY (`id`)
				) ENGINE = '._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;'))
				return false;
		if (!Db::getInstance()->Execute('
				INSERT INTO `'.$this->DBtable.'` (
					`id`, `data`
				) VALUES (1,1),(2,2);'))
				return false;
		return true;
	}

	public function getContent()
	{
		$output = "";
		if (Tools::isSubmit('submitHomeFeatured'))
		{
			$nbr = (int)(Tools::getValue('nbr'));
			$admethod = (int)(Tools::getValue('admethod'));
			Configuration::updateValue('ISOTOPE_ADD_METHOD', (int)($admethod));

			if (!$nbr OR $nbr <= 0 OR !Validate::isInt($nbr))
				$errors[] = $this->l('An invalid number of products has been specified.');
			else
				Configuration::updateValue('ISOTOPE_NBR', (int)($nbr));
			if (isset($errors) AND sizeof($errors))
				$output .= $this->displayError(implode('<br />', $errors));
			else
				$output .= $this->displayConfirmation($this->l('Your settings have been updated.'));
		}
		return $output.$this->displayForm();
	}

	public function displayForm()
	{
		$meth = (int)(Configuration::get('ISOTOPE_ADD_METHOD'));

		if (isset($this->name[Context::getContext()->language->id]))
			$id_lang = Context::getContext()->language->id;
		else
			$id_lang = (int)Configuration::get('PS_LANG_DEFAULT');

		$categories = Category::getCategories(intval($id_lang), false);		
		$sql = 'SELECT data FROM `'.$this->DBtable.'`';
		$data = Db::getInstance()->executeS($sql);		

		$htmlPrd = '';

		foreach ($data as $key => $item) {

			$htmlPrd .= $this->htmlCodeProducts($item["data"], "");		
			
		}

		$output = '
		<style>.hide {display:none} .l {width:250px}</style>
		<form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" method="post">
			<fieldset><legend><img src="'.$this->_path.'logo.gif" alt="" title="" />'.$this->l('Settings').'</legend>
				<label class="l" for="auto_admethod">Automatically add products to the homepage</label>
				<input type="radio" name="admethod" id="auto_admethod" value="1" '.(($meth == 1) ? 'checked ' : '').'/><br/><br/><br/>
				<label class="l" for="manual_admethod">Manually add products to the homepage</label>
				<input type="radio" name="admethod" id="manual_admethod" value="0" '.(($meth == 0) ? 'checked ' : '').'/><br/><br/>
				<div class="automatical_method'.(($meth == 0) ? ' hide ' : '').'">					
					<label>'.$this->l('Define the number of products.').'</label>
					<input type="text" size="5" name="nbr" value="'.Tools::safeOutput(Tools::getValue('nbr', (int)(Configuration::get('ISOTOPE_NBR')))).'" />
						<p class="clear">'.$this->l('Define the number of each type of products that you would like to display on homepage (default: 8). Total you will have the number multiplied to 3 (3 types new/special/featured)' ).'</p>
				</div>
				<div class="manually_method'.(($meth == 1) ? ' hide ' : '').'">
					<div class="categoriesArea">
					'.$this->displayCategoriesSelect($categories, 0).'						
					</div>
					<div id="allprdcts" class="productsArea">
						<label>' . $this->l('All Products from category') . '</label>
						<div class="container"></div>
					</div>
					<div id="selectedprdcts" class="productsArea">
						<label>' . $this->l('Selected Products') . '</label>
						<div class="container">'.$htmlPrd.'</div>
					</div>					
					<script>
					$(document).ready(function() {  
					    $("#categList").click(function() {    	
							var cID = $("#categList").val();
							$.ajax({
							    type: "POST",
							    url: "'._MODULE_DIR_.$this->name.'/ajax.php?cID="+cID,
							    success: function(result){
							      if (result == "0") {
							        console.log("no data")
							      } else {
									$("#allprdcts .container").html(result);
							      }
							    }
							});				
					    });
						$("#allprdcts").on("click", ".prodSection", function () {
							var pID = $(this).data("pid");								
							var res = "";
							$.ajax({
							    type: "POST",
							    url: "'._MODULE_DIR_.$this->name.'/ajax.php?pID="+pID,
							    success: function(result){
							      if (result == "0") {
							        res = result;
							        console.log("no data");
							      } else {
							      	res = result;
							      }
							    }
							});
							$(this).clone().appendTo("#selectedprdcts .container");
					    });
						$("#selectedprdcts").on("click", ".prodSection", function () {
							var pID = $(this).data("pid");								
							var res = "";
							$.ajax({
							    type: "POST",
							    url: "'._MODULE_DIR_.$this->name.'/ajax.php?rem_pID="+pID,
							    success: function(result){
							      if (result == "0") {
							        res = result;
							      } else {
							      	res = result;
							      }
							    }
							});
							$(this).remove();
						});
					});
					</script>
				</div><br/>
				<div class="clear submitButton"><input type="submit" name="submitHomeFeatured" value="'.$this->l('Save').'" class="button" /></div>
			</fieldset>
		</form>';
		$this->context->controller->addCSS(($this->_path).'css/bo_isotopeFilter.css', 'all');
		return $output;
	}

	public function ajaxCall() {

		$nb = (int)(Configuration::get('ISOTOPE_NBR'));
		$category = new Category(Context::getContext()->shop->getCategory(), (int)Context::getContext()->language->id);
		$products["featured"] = $category->getProducts((int)Context::getContext()->language->id, 1, ($nb ? $nb : 8));
		$products["new"]=Product::getNewProducts((int)Context::getContext()->language->id, 0, ($nb ? $nb : 8));

		if (!empty($products)) {
			foreach($products as $product) {
				if (!empty($product)) {
					foreach($product as $data) {
						$pr_html="";
			//				print_r($data);
						$imageData = Image::getCover($data['id_product']);
						$imgLink = $this->context->link->getImageLink($data['link_rewrite'], (int)$data['product_id'].'-'.(int)$imageData['id_image'], 'home_default');
						
						$pr_html.="<li class=\"ajax_block_product new_products isotope-hidden isotope-item\" >";
						$pr_html.="<a href=\"".$data['link']."\" title=\"".$data['name']."\" class=\"product_image\">";
							$pr_html.="<img src=\"".$imgLink."\" alt=\"".$data['name']."\"></a>";
						$pr_html.="<h5 class=\"s_title_block\">";
							$pr_html.="<a href=\"".$data['link']."\" title=\"".$data['name']."\">".$data['name']."</a>";
						$pr_html.="</h5> <div class=\"product_desc\">";
							$pr_html.=$data['description_short'];
						$pr_html.="</div><div>";
							$pr_html.="<p class=\"price_container\"><span class=\"price\">".Product::getPriceStatic((int)$data['id_product'], true, NULL)."</span></p>";
									$pr_html.="<a class=\"exclusive ajax_add_to_cart_button\" rel=\"ajax_id_product_1\" href=\"".$data['link']."\" title=\"Add to cart\">Add to cart</a></div></li>";
						print_r($pr_html);
																		
					}
				}
			}
		}
	

	}

	public function hookDisplayHeader($params)
	{
		$this->hookHeader($params);
	}

	public function hookHeader($params)
	{
		$this->context->controller->addCSS(($this->_path).'css/'.$this->name.'.css', 'all');
		$this->context->controller->addJS($this->_path.'js/jquery.isotope.min.js');

	}
	public function isNew($id) {
		$result = Db::getInstance()->executeS('
			SELECT p.id_product
			FROM `'._DB_PREFIX_.'product` p
			'.Shop::addSqlAssociation('product', 'p').'
			WHERE p.id_product = '.(int)$id.'
			AND DATEDIFF(
				product_shop.`date_add`,
				DATE_SUB(
					NOW(),
					INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY
				)
			) > 0
		');
		if (count($result) > 0) {
			return "new_product";
		} else {
			return "";
		}
	}
	public function hookHook_home_05($params)
	//public function hookDisplayHome($params)
	{		
		$rootCategory = Category::getRootCategory();
		$idLang = (int)Context::getContext()->language->id;
		$categoriesArray = array('0' => array('id_category' => $rootCategory->id_category));
		$bestsellersList = array();

		if (Configuration::get('ISOTOPE_ADD_METHOD') == 0) {

			$sql = 'SELECT data FROM `'.$this->DBtable.'`';			
			$data = Db::getInstance()->executeS($sql);		
			foreach ($data as $k => $value) {
				$listID[$k] = $value["data"];
			}
			$uniqueListID = array_unique($listID);

			$link = new Link();
			
			foreach ($uniqueListID as $k => $productID) {

				$product = new Product($productID, true, $this->context->language->id);
				$prdcts["unsorted"][$k] = get_object_vars($product);
				$prdcts["unsorted"][$k]["id_product"] = $productID;
				$cover = Image::getCover($productID);
				$img = /*$productID.'-'.*/(int)$cover["id_image"];				
				$prdcts["unsorted"][$k]["id_image"] = $img;
				$prdcts["unsorted"][$k]["link"] = $link->getProductLink($product);
			
			}
			$products["unsorted"] = Product::getProductsProperties((int)$idLang, $prdcts["unsorted"]);	
			
			// get all bestsellers ID		
			if (!empty($products["unsorted"])) {
				foreach ($products["unsorted"] as $key => $value) {
					$bestsellersList[] = $value["id_product"];
				}							
			} else {
				$bestsellersList[] = "";
			}
			
		} else {
			
			$category = new Category(Context::getContext()->shop->getCategory(), (int)Context::getContext()->language->id);
			$nb = (int)(Configuration::get('ISOTOPE_NBR'));
			$orderWay = 'ASC';
			$orderBy = null;
			$products["featured"] = $category->getProducts((int)Context::getContext()->language->id, 1, ($nb ? $nb : 10));
			$products["new"]=Product::getNewProducts((int)Context::getContext()->language->id, 0, ($nb ? $nb : 10), false, $orderBy, $orderWay);
			$products["special"] = Product::getPricesDrop($idLang, 0, ($nb ? $nb : 10), false, $orderBy, $orderWay); 				
			//$products["bestsellers"] = ProductSale::getBestSales($idLang, 0, ($nb ? $nb : 10));
			
			// get all bestsellers ID		
			/*if (!empty($products["bestsellers"])) {
				foreach ($products["bestsellers"] as $key => $value) {
					$bestsellersList[] = $value["id_product"];
				}							
			} else {
				$bestsellersList[] = "";
			}*/
						
		}	

		foreach ($products as $key=>$product) {
			if (!empty($product)) {
				foreach ($product as $k => $val) {
					$p[$val["id_product"]] = $val;
				}
			} 
		}
		// sort products by types
		$fea_counter = $new_counter = $spe_counter = $bes_counter = 1;	
		$nb = (int)(Configuration::get('ISOTOPE_NBR'));
		foreach ($p as $k=>$data) {
			if (!empty($data)) {				
				$type[$data["id_product"]] = "";					
				if ($fea_counter <= $nb) {
					if (Product::idIsOnCategoryId($data["id_product"], $categoriesArray)) {						
						$type[$data["id_product"]] .= " featured_product";
						$fea_counter++;
					} else {
						$type[$data["id_product"]] .= "";
					}						
				}
				if ($new_counter <= $nb) {
					if ($this->isNew($data["id_product"])) {
						$type[$data["id_product"]] .= " new_product"; 
						$new_counter++;
					} else {
						$type[$data["id_product"]] .= "";
					}
				}
				if ($spe_counter <= $nb) {
					if (!empty($data["specific_prices"])) {
						$type[$data["id_product"]] .= " special_product"; 
						$spe_counter++;
					} else {
						$type[$data["id_product"]] .= "";				
					}
				}
				if ($bes_counter <= $nb) {
					if (in_array($data["id_product"], $bestsellersList)) { 
						$type[$data["id_product"]] .= " bestsellers";
						$bes_counter++;
					} else {
						$type[$data["id_product"]] .= "";	
					}
				}				
				$productReadyList[$data["id_product"]] = $data;
			} else {
				$productReadyList[$k] = "";
			}
		} 
		shuffle($productReadyList);

		$this->smarty->assign(array(
			'isotope_products' => $productReadyList,
			'add_prod_display' => Configuration::get('PS_ATTRIBUTE_CATEGORY_DISPLAY'),
			'isotope_type' => $type,
			'moduleName' => $this->name
		));

		return $this->display(__FILE__, 'pk_isotopeSort.tpl');
	}

	private function recurseCategory($categories, $current, $id_category = 1, $id_selected = 1) {

		global $currentIndex;

		$this->_html .= '<option value="' . $id_category . '"' . (($id_selected == $id_category) ? ' selected="selected"' : '') . '>' . str_repeat('&nbsp;', $current ['infos'] ['level_depth'] * 5) . (_PS_VERSION_ < 1.4 ? self::hideCategoryPosition(stripslashes($current ['infos'] ['name'])) : stripslashes($current ['infos'] ['name'])) . '</option>';
		if (isset($categories [$id_category]))
			foreach ( $categories [$id_category] as $key => $row )
				$this->recurseCategory($categories, $categories [$id_category] [$key], $key, $id_selected);
	}
	private function displayCategoriesSelect($categories, $selected) {
		$this->_html = '';
		$this->_html .= '<label>' . $this->l('Categories') . '</label>
      			<select multiple name="id_category" id="categList">';
						$this->recurseCategory($categories, $categories [0] [1], 1, $selected);
		$this->_html .= '</select>';
		return $this->_html;
	}

	private function htmlCodeCategories($prod_id) {

		$orderBy = Tools::getProductsOrder('by', Tools::getValue('orderby'));
		$langID = (int)Context::getContext()->language->id;
		$html = '';

		$products = Product::getProducts($langID, 0, 0, $orderBy, "ASC", $prod_id, true);

		foreach ($products as $key => $data) {
			$html .= $this->htmlCodeProducts($data["id_product"], "");							

		}

		return $html;
		
	}
	private function htmlCodeProducts($prod_id, $front) {

		$cover = Image::getCover($prod_id);		
		$img = $prod_id.'-'.(int)$cover["id_image"];
		$productName = Product::getProductName($prod_id);

		$sql = 'SELECT link_rewrite, description_short FROM `'._DB_PREFIX_.'product_lang` WHERE id_product='.$prod_id.' AND id_shop='.Context::getContext()->shop->id.' AND id_lang='.Context::getContext()->language->id;		

		$prodData = Db::getInstance()->executeS($sql);		

		
		$html = '';

		if ($front == "front") {

					

		} else {			

			$html = "<div data-pid=\"".$prod_id."\" class=\"prodSection\" title=\"".$productName."\"><img src=\"".$this->context->link->getImageLink($prodData[0]["link_rewrite"], $img, 'small_default')."\" alt=\"\" /><span>".substr($productName, 0, 8)."...</span></div>";
			
		}

		return $html;

	}
	public function saveData($pID) {
			
		Db::getInstance()->Execute('INSERT INTO `'.$this->DBtable.'` (`data`) VALUES ('.$pID.');');

	}

	public function removeData($rem_pID) {

		Db::getInstance()->Execute('DELETE FROM `'.$this->DBtable.'` WHERE data = '.$rem_pID);	

	}

	public function getProductsFromCategory($cID) {

		$ids = explode(",", $cID);		
		$html = '';

		foreach ($ids as $id) {
			$html .= $this->htmlCodeCategories($id);
		}

		print_r($html);
	}
	
	public function hookHook_home_01($params)
	{
		return $this->hookHook_home_05($params);
	}
	public function hookHook_home_02($params)
	{
		return $this->hookHook_home_05($params);
	}
	public function hookHook_home_03($params)
	{
		return $this->hookHook_home_05($params);
	}
	public function hookHook_home_04($params)
	{
		return $this->hookHook_home_05($params);
	}
	public function hookHook_home_06($params)
	{
		return $this->hookHook_home_05($params);
	}	
	public function hookHook_home_07($params)
	{
		return $this->hookHook_home_05($params);
	}
}
