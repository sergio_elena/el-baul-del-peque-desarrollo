{if $page_name == "index"}
{if $theme_settings.isotope == 1}
<!-- MODULE IsotopeSort -->
<div id="isotopeSort">
	<h4 class="title_block" style="display:none">{l s='Featured products' mod='pk_isotopeSort'}</h4>
	{if isset($isotope_products) AND $isotope_products}
        <div class="option-combo">
      <ul class="filter option-set clearfix" data-filter-group="type"> 
          <li><a href="#" data-filter-value="" class="selected">{l s='All' mod='pk_isotopeSort'}</a></li>
          <li><a href="#" data-filter-value=".featured_product">{l s='Featured' mod='pk_isotopeSort'}</a></li>
          <li><a href="#" data-filter-value=".special_product">{l s='Special' mod='pk_isotopeSort'}</a></li> 
          <li><a href="#" data-filter-value=".new_product">{l s='Latest' mod='pk_isotopeSort'}</a></li>
          <!--<li><a href="#" data-filter-value=".bestsellers">{l s='Bestsellers' mod='pk_isotopeSort'}</a></li>-->
      </ul>
    </div>        

		<div class="block_content virgin">
			<ul id="isotope" class="isotope">
            {foreach from=$isotope_products item=product key=p name=isotopeSortProduct}
       			<li class="ajax_block_product {$isotope_type[$product['id_product']]}">
					<a href="{$product.link}" title="{$product.name|escape:html:'UTF-8'}" class="product_image">
						<img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_alysum')}" alt="{$product.name|escape:html:'UTF-8'}" />
						{if isset($product.new) && $product.new == 1}<span class="new">{l s='New' mod='pk_isotopeSort'}</span>{/if}
					</a>					
					<div class="isotope_bottom_block">					
					<h5 class="s_title_block">
						<a href="{$product.link}" title="{$product.name|truncate:50:'...'|escape:'htmlall':'UTF-8'}">{$product.name|truncate:17:'...'|escape:'htmlall':'UTF-8'}</a>
					</h5>
						{if $product.show_price AND !isset($restricted_country_mode) AND !$PS_CATALOG_MODE}
							<p class="price_container">
								{if ($product.reduction > 0)}
								<span class="price isotope-old-price">{convertPrice price=$product.price_without_reduction}</span>	
								{/if}
								<span class="price">{if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}</span></p>
						{/if}
						
						{if ($product.id_product_attribute == 0 OR (isset($add_prod_display) AND ($add_prod_display == 1))) AND $product.available_for_order AND !isset($restricted_country_mode) AND $product.minimal_quantity == 1 AND $product.customizable != 2 AND !$PS_CATALOG_MODE}
							{if ($product.quantity > 0 OR $product.allow_oosp)}
								<a class="exclusive ajax_add_to_cart_button" rel="ajax_id_product_{$product.id_product}" href="{$link->getPageLink('cart')}?qty=1&amp;id_product={$product.id_product}&amp;token={$static_token}&amp;add" title="{l s='Add to cart' mod='pk_isotopeSort'}">{l s='Add to cart' mod='pk_isotopeSort'}</a>	
							{else}
								<a href="{$product.link}" class="button exclusive">{l s='View' mod='pk_isotopeSort'}</a>
							{/if}	
						{else}
						<a href="{$product.link}" class="button exclusive">{l s='View' mod='pk_isotopeSort'}</a>
						{/if}
					</div>
				</li>
			{/foreach}
			</ul>
		</div>
	{else}
		<p>{l s='No featured products' mod='pk_isotopeSort'}</p>
	{/if}
</div>
<script>
function getProducts() {
	$.ajax({
	    type: 'POST',
	    url: baseDir + 'modules/pk_isotopeSort/ajax.php',
	    success: function(result){
	      if (result == '0') {
	        console.log("no data")
	      } else {
			  
				$('#isotope').prepend(result);			
	      }
	    }
	});
}

$(window).load(function() {
// cache container
	var $container = $('#isotope');

	// initialize isotope
	$container.isotope({
	  // options...
	});

// filter items when filter link is clicked
$('.filter a').click(function(){
  var selector = $(this).attr('data-filter-value');
  $container.isotope({ filter: selector });
  $('.filter a.selected').removeClass('selected');
  $(this).addClass('selected');
  return false;
});
   
	
});

</script>
<!-- /MODULE IsotopeSort -->
{/if}
{/if}