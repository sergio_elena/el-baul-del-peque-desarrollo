<!-- begin twitter widget -->
<div id="twitter-feed" class="block">
  <div class="block_content">
    <h4 class="dropdown-cntrl">{l s='Latest Tweets' mod='pk_twitterwidget'}</h4>
    <div class="dropdown-content">
      <div class="tweet"></div>
    </div>  
  </div>
</div>
<script>
{literal}
/*jQuery(function($){ 
    var twitterSlider = function(){  
        $(".tweet_list").flexisel({
      pref: "twt",
          visibleItems: 1,
          animationSpeed: 1000,
          autoPlay: true,
          autoPlaySpeed: 4000,            
          pauseOnHover: true,
          enableResponsiveBreakpoints: true,
          responsiveBreakpoints: { 
              portrait: { 
                  changePoint:480,
                  visibleItems: 1
              }, 
              landscape: { 
                  changePoint:640,
                  visibleItems: 1
              },
              tablet: { 
                  changePoint:768,
                  visibleItems: 1
              }
          }
      });
            
    }; */     
    $('.tweet').tweet({
        join_text: "auto",
          username: '{/literal}{$username}{literal}',
          avatar_size: 0,
          count: {/literal}{$count}{literal},
          auto_join_text_default: "",
          auto_join_text_ed: "",
          auto_join_text_ing: "",
          auto_join_text_reply: "",
          auto_join_text_url: "",
          loading_text: "loading tweets...",
          modpath: '{/literal}{$tw_this_path}{literal}',
        // loaded: twitterSlider
    });     
//});
{/literal}
</script>
<!-- end twitter widget -->