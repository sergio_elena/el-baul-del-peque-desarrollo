<?php

if (!defined('_CAN_LOAD_FILES_'))
	exit;

class pk_blockfacebooklike extends Module
{
	private $_html = '';
	private $_postErrors = array();

	public function __construct()
	{
		$this->name = 'pk_blockfacebooklike';
		$this->tab = 'front_office_features';
		$this->version = '2.1';
		$this->author = '';

		parent::__construct();

		$this->displayName = $this->l('Alysum Facebook LikeBox');
		$this->description = $this->l('Put your Facebook fan page like box on your site.');
		$this->cacheName = _PS_MODULE_DIR_.$this->name.'/fans_'.(int)Context::getContext()->shop->id.'.tpl';
	}

	public function install()
	{
		if (!parent::install() OR 
			!$this->registerHook('footer') OR 
			!$this->registerHook('header') OR
			!Configuration::updateValue('PLLB_URL', 'prestashop') OR		
			!Configuration::updateValue('PLLB_TITLE', 'Facebook') OR		
			!Configuration::updateValue('PLLB_FACES', 1) OR
			!Configuration::updateValue('PLLB_COMPANY_NAME', 0) OR
			!Configuration::updateValue('PLLB_COMPANY_LOGO', 0) OR 
			!Configuration::updateValue('PLLB_NUM', 6))
			return false;
		return true;
	}

	public function uninstall()
	{
		if (!parent::uninstall() OR
		!Configuration::deleteByName('PLLB_URL') OR		
		!Configuration::deleteByName('PLLB_NUM') OR
		!Configuration::deleteByName('PLLB_TITLE') OR	
		!Configuration::deleteByName('PLLB_COMPANY_NAME') OR
		!Configuration::deleteByName('PLLB_COMPANY_LOGO') OR 	
		!Configuration::deleteByName('PLLB_FACES'))
			return false;
		return true;
	}
	
	private function _postProcess()
	{
		Configuration::updateValue('PLLB_URL', Tools::getValue('url'));
		Configuration::updateValue('PLLB_TITLE', Tools::getValue('title'));
		Configuration::updateValue('PLLB_FACES', Tools::getValue('faces'));
		Configuration::updateValue('PLLB_COMPANY_NAME', Tools::getValue('company_name'));
		Configuration::updateValue('PLLB_COMPANY_LOGO', Tools::getValue('company_logo'));	
		Configuration::updateValue('PLLB_NUM', Tools::getValue('num'));	
				
		
		$this->_html .= '<div class="conf confirm">'.$this->l('Settings updated').'</div>';
	}
	
	public function getContent()
	{
		$this->_html .= '<h2>'.$this->displayName.'</h2>';
		$msg = "";
		if (Tools::isSubmit('submit'))
		{			
			
			if (!sizeof($this->_postErrors))
				$this->_postProcess();
			else
			{
				foreach ($this->_postErrors AS $err)
				{
					$this->_html .= '<div class="alert error">'.$err.'</div>';
				}
			}
			$msg = $this->cleanCache();
		}
		
		$this->_displayForm($msg);
		
		return $this->_html;
	}
	
	private function _displayForm($message)
	{
		if (!($languages = Language::getLanguages()))
			 return false;		
		$this->_html .= '
		'.$message.'
		<form action="'.$_SERVER['REQUEST_URI'].'" method="post">
			<fieldset>			
				<legend><img src="../img/admin/cog.gif" alt="" class="middle" />'.$this->l('Settings').'</legend>				
				<label>'.$this->l('Box Title').'</label>
				<div class="margin-form">
					<input size="60" type="text" name="title" value="'.Tools::getValue('title', Configuration::get('PLLB_TITLE')).'"/>
					<p class="clear">'.$this->l('The title of box on the front page').'</p>
				</div>
				<label>'.$this->l('Facebook Page Name').'</label>
				<div class="margin-form">
					<input size="60" type="text" name="url" value="'.Tools::getValue('url', Configuration::get('PLLB_URL')).'"/>
					<p class="clear">'.$this->l('The name of the Facebook Page').'</p>
				</div>
				<label>'.$this->l('Face number').'</label>
				<div class="margin-form">
					<input size="6" type="text" name="num" value="'.Tools::getValue('num', Configuration::get('PLLB_NUM')).'"/>
					<p class="clear">'.$this->l('Nubmer of visible faces').'</p>
				</div>						
				<label>'.$this->l('Show Faces').'</label>
				<div class="margin-form">
					<input type="checkbox" name="faces" '.(Tools::getValue('faces', Configuration::get('PLLB_FACES')) ? "value='true' checked='checked'" : "value='false'").' />
				</div>
				<label>'.$this->l('Show Company name').'</label>
				<div class="margin-form">
					<input type="checkbox" name="company_name" '.(Tools::getValue('company_name', Configuration::get('PLLB_COMPANY_NAME')) ? "value='true' checked='checked'" : "value='false'").' />
				</div>
				<label>'.$this->l('Show Company Logo').'</label>
				<div class="margin-form">
					<input type="checkbox" name="company_logo" value="'.(Tools::getValue('company_logo', Configuration::get('PLLB_COMPANY_LOGO')) ? "true" : "false").'"'.(Tools::getValue('company_logo', Configuration::get('PLLB_COMPANY_LOGO')) ? "checked='checked'" : "").' />
				</div>
				
			<input type="submit" name="submit" value="'.$this->l('Update').'" class="button" />
			</fieldset>
		</form>';
	}
	
// -----------------------------------------------------------------	
	public function getFacebookData($name) {

		$facebookInfo = json_decode(file_get_contents('https://graph.facebook.com/'.$name));		
		
		Configuration::updateValue('PLLB_DATA_NAME', $facebookInfo->name);
		Configuration::updateValue('PLLB_DATA_LIKES', $facebookInfo->likes);
		Configuration::updateValue('PLLB_DATA_ID', $facebookInfo->id);

	}
	public function getAttribute($attrib, $tag){
		  //get attribute from html tag
		  $re = '/'.$attrib.'=["\']?([^"\' ]*)["\' ]/is';
		  preg_match($re, $tag, $match);
		  
		  if($match){
		    return urldecode($match[1]);
		  }else {
		    return false;
		  }
	}

	public function cleanCache() {

		//$pth = $_SERVER["DOCUMENT_ROOT"].$this->_path.'fans.tpl';
		if (!unlink($this->cacheName)) {
			$cleaned = "<div class='alert error'>Cache doesn\'t cleaned</div>";
        } else {
			$cleaned = "<div class=\"conf confirm\">Cache has been cleaned</div>";	            	
        }		   		                    		
		return $cleaned;
	}

	public function hookFooter()
	{
		
		$facebook_username = Configuration::get('PLLB_URL');
		$cacheName = $this->cacheName;
		$err = "";
		// generate the cache version if it doesn't exist or it's too old!
		$ageInSeconds = 3600; // 1 hour

		if(!file_exists($cacheName) || (filemtime($cacheName) + $ageInSeconds < time())) {	
			//$dom = simplexml_load_file($cacheName);
			$curl = 'https://www.facebook.com/plugins/likebox.php?href=https://www.facebook.com/'.$facebook_username.'&connections='.Configuration::get('PLLB_NUM');
			$ch = curl_init($curl);
			
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20100101 Firefox/15.0.1");
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		    $result = curl_exec($ch);
		    curl_close($ch);
		    
		    $doc = new DOMDocument('1.0', 'utf-8');
			@$doc->loadHTML('<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />' . $result);
			//print_r($doc->saveHTML());
			$peopleList = array();
			$i=0;

			foreach ($doc->getElementsByTagName('ul')->item(0)->childNodes as $child) {
		    	$raw = $doc->saveXML($child);
		    	$li = preg_replace("/<li[^>]+\>/i", "", $raw);
		    	$peopleList[$i] = preg_replace("/<\/li>/i", "", $li);			    				    	
				$i++;
			}
			if (!$fileHolder = @fopen($cacheName, 'w')) {
				$err = "Can't open settings file! \"".$cacheName."\"<br/>";
			} else {			
				foreach ($peopleList as $key => $code) {
					$name = $this->getAttribute('title', $code);
					$nm = substr($name, 0, 7);
					//print_r(strlen($nm));echo "\n";
					if (strlen($nm) != strlen($name)) $nm = $nm."...";

					$image = $this->getAttribute('src', $code);
					$link = $this->getAttribute('href', $code);

					$protocols = array("http:","https:"); 
					$img_link = str_replace($protocols, "", $image);

					//$data = file_get_contents($image);
					//echo "<div style='display:none' class='image'>".$image."</div>";
					//echo "<div style='display:none' class='data'>".$data."</div>";
					//$img_in_base64 = 'data:image/jpg;base64,'.base64_encode($data);
					//echo "<div style='display:none' class='base64'><img src=\"".$img_in_base64." alt=\"\" /></div>";
					if ($link != "") {
						$wrapper = "<a href=\"".$link."\" title=\"".$name."\" target=\"_blank\"><img src=\"".$img_link."\" alt=\"\" /></a>";
					} else 	{
						$wrapper = "<span title=\"".$name."\"><img src=\"".$img_link."\" alt=\"\" /></span>";
					}

					$li = "<li class='face_".$key."'>".$wrapper."<div class=\"fb_name\">".$nm."</div></li>";
					if (!fwrite($fileHolder, $li)) {
						$err = "Can't write settings! ".$cacheName."<br/>";
					}				
				}				
				fclose($fileHolder);	
				$this->getFacebookData($facebook_username);
			}
		}	

		$FB_data["name"] = Configuration::get('PLLB_DATA_NAME');
		$FB_data["likes"] = Configuration::get('PLLB_DATA_LIKES');
		$FB_data["id"] = Configuration::get('PLLB_DATA_ID');

		$this->smarty->assign(array(
			'FB_page_URL' => 'https://www.facebook.com/'.$facebook_username,
			'FB_data' => $FB_data,
			'show_faces' => Configuration::get('PLLB_FACES'),
			'FB_title' => Configuration::get('PLLB_TITLE'),
			'company_name' => Configuration::get('PLLB_COMPANY_NAME'),
			'company_logo' => Configuration::get('PLLB_COMPANY_LOGO'),
			'modulePath' => $this->cacheName,
			'err' => $err
		));

		return $this->display(__FILE__, $this->name.'.tpl');
		
	}
	public function hookHeader($params)
	{
		$this->context->controller->addCSS($this->_path.$this->name.'.css', 'all');
	}
	
		
	public function hookRightColumn($params)
	{
		return $this->hookFooter($params);
	}

	public function hookLeftColumn($params)
	{
		return $this->hookFooter($params);
	}

		
}

