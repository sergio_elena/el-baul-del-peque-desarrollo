<div class="block facebook-box">
	<h4 class="dropdown-cntrl">{l s='Facebook' mod='blockfacebooklike'}</h4>
	<div class="dropdown-content">
		<a href="{$FB_page_URL}" target="_blank" class="likeButton">{l s='Like' mod='blockfacebooklike'}</a>
		<div class="block_content">
			<div class="fb_info_top">
				{if $company_logo}
				<img src="https://graph.facebook.com/{$FB_data.id}/picture" alt="" class="fb_avatar" />
				{/if}
				<div class="fb_info">
					{if $company_name}
					<div>{$FB_data.name}</div>
					{/if}				
				</div>
			</div>
			<div class="fb_fans">{l s='%s people like' sprintf=$FB_data.likes mod='blockfacebooklike'} <a href="{$FB_page_URL}" target="_blank">{$FB_data.name}</a>.</div>
			{if $show_faces}
			{$err}
			<ul class="fb_followers">
				{if file_exists($modulePath)} 
					{include file="$modulePath"}
				{/if}
			</ul>
			{/if}
		</div>
	</div>
</div>
