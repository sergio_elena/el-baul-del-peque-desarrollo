{*
* Copyright (C) 2012  S.C Minic Studio S.R.L.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
* @author     S.C Minic Studio S.R.L.
* @copyright  Copyright S.C Minic Studio S.R.L. 2012. All rights reserved.
* @license    GPLv2 License http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
* @version    v3.0.0
*}
<div id="sequence-theme">
    {if $minicSlider.options.buttons == 1}
    <ul class="controls">
        <li class="status"></li>
        <li class="prev"></li>
        <li class="pause"></li>
        <li class="next"></li>
    </ul>
    {/if}
    {if $minicSlider.options.control == 1}
    <ul class="nav">
        {foreach from=$slides.$lang_iso item=image name=singleimage}
        <li data-id="{$image.id_slide}"></li>
        {/foreach}
    </ul>
    {/if}
    <div id="sequence">
        <ul>
            {foreach from=$slides.$lang_iso key=iso item=slide}        
            <li class="animate-in" id="slide{$slide.id_slide}" data-id="{$slide.id_slide}">                          
                <img src="{$minicSlider.path.images}{$slide.image}" class="slide{$slide.id_slide} mainImg" {if $slide.alt}alt="{$slide.alt}"{/if} {if $slide.title != '' or $slide.caption != ''}title="{$image.title}"{/if} />
                {if $subImages[$slide.lang_iso][$slide.id_slide][0] != ""}
                    {foreach $subImages[$slide.lang_iso][$slide.id_slide] key=imgId item=imgName}
                        <img src="{$minicSlider.path.images}{$imgName}" class="slideImg{$imgId}" alt="{$imgId}" />
                    {/foreach}
                {/if}
                <div id="htmlcaption_{$slide.id_slide}" class="slide-text-{$slide.id_slide} slide-text">
                    <h3>{$slide.title}</h3>
                    <p>{$slide.caption}</p>
                    {if ($slide.url != "")}<a href="{$slide.url}" class="button">{l s='Details' mod='pk_sequenceminicslider'}</a>{/if}
                </div>
            </li>
            {/foreach}            
        </ul>
    </div>
</div>