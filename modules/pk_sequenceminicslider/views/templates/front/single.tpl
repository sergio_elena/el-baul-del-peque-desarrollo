{*
* Copyright (C) 2012  S.C Minic Studio S.R.L.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
* @author     S.C Minic Studio S.R.L.
* @copyright  Copyright S.C Minic Studio S.R.L. 2012. All rights reserved.
* @license    GPLv2 License http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
* @version    v3.0.0
*}
<div id="sequence-theme">
    {if $minicSlider.options.buttons == 1}
    <ul class="controls">
        <li class="status"></li>
        <li class="prev"></li>
        <li class="pause"></li>
        <li class="next"></li>
    </ul>
    {/if}
    {if $minicSlider.options.control == 1}
    <ul class="nav">
        {foreach from=$slides.$defLang item=image name=singleimage}
        <li data-id="{$image.id_slide}"></li>
        {/foreach}
    </ul>
    {/if}
    <div id="sequence">
        <ul>
            {foreach from=$slides.$defLang item=image name=singleimage}            
            <li class="animate-in" id="slide{$image.id_slide}" data-id="{$image.id_slide}" data-info="shop{$image.id_shop}langiso{$image.lang_iso}">
                <img src="{$minicSlider.path.images}{$image.image}" class="slide{$image.id_slide} mainImg" {if $image.alt}alt="{$image.alt}"{/if} {if $image.title != '' or $image.caption != ''}title="{$image.title}"{/if} />
                {if $subImages[$image.lang_iso][$image.id_slide][0] != ""}
                    {foreach $subImages[$image.lang_iso][$image.id_slide] key=imgId item=imgName}
                        <img src="{$minicSlider.path.images}{$imgName}" class="slideImg{$imgId}" alt="{$imgId}" />
                    {/foreach}
                {/if}
                <div id="htmlcaption_{$image.id_slide}" class="slide-text-{$image.id_slide} slide-text">
                    <h3>{$image.title}</h3>
                    <p>{$image.caption}</p>
                    {if ($image.url != "")}<a href="{$image.url}" class="button">{l s='Details' mod='pk_sequenceminicslider'}</a>{/if}
                </div>
            </li>
            {/foreach}            
        </ul>
    </div>
</div>