{*
* Copyright (C) 2012  S.C Minic Studio S.R.L.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
* @author     S.C Minic Studio S.R.L.
* @copyright  Copyright S.C Minic Studio S.R.L. 2012. All rights reserved.
* @license    GPLv2 License http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
* @version    v3.0.0
*}
{if $page_name == "index"}
    {if $theme_settings.sequence == 1}
    <!-- MINIC SLIDER -->    
    {include file="{$minicSlider.tpl}"}
    <script type="text/javascript">
        $(document).ready(function(){
            var noOfPlays = 0;
            var options = {
                preloader: true,
                nextButton: true,
                prevButton: true,
                animateStartingFrameIn: true,
                reverseAnimationsWhenNavigatingBackwards: false,
                numericKeysGoToFrames: false,
                swipeNavigation: true,
                pauseOnHover: {if $minicSlider.options.hover == 1}true{else}false{/if},
                autoPlay: {if $minicSlider.options.manual == 1}true{else}false{/if},
                autoPlayDirection: 1,
                autoPlayDelay: {if $minicSlider.options.pause != ''}{$minicSlider.options.pause}{else}5000{/if},
            };

            var sequence = $("#sequence").sequence(options).data("sequence");
            var wdth = $("#sequence-theme .nav").width();
            var wdth2 = $("#sequence-theme .controls").width();
            $("#sequence-theme .nav").css({ "left": "50%", "marginLeft": -(wdth/2) });        
            $("#sequence-theme .controls").css({ "left": "50%", "marginLeft": -(wdth2/2) });        

            function autoChangePagination() {
                $("#sequence-theme .nav li").removeClass("active");
                $("#sequence-theme .nav li:nth-child("+sequence.nextFrameID+")").addClass("active");
            }

            sequence.beforeNextFrameAnimatesIn = function() {
                if(sequence.nextFrameID == 4) {
                    noOfPlays = 1;
                }
                if(sequence.nextFrameID == 1 && noOfPlays  > 0) {
                    sequence.stopAutoPlay();
                }
                autoChangePagination();
            }

            $("#sequence-theme .nav").on("click", "li", function() {
                sequence.startAutoPlay();
                sequence.settings.pauseOnHover = true;
                $("#sequence-theme .nav li").removeClass("active");
                $(this).addClass("active");

                sequence.nextFrameID = $(this).index()+1;
                sequence.goTo(sequence.nextFrameID);
            });
        });
    </script>
    {/if}
{/if}