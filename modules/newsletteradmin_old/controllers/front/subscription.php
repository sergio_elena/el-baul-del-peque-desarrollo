<?php

class NewsletteradminSubscriptionModuleFrontController extends ModuleFrontController
{
	private $message = '';

	public function postProcess()
	{
		$email = base64_decode($_GET['SID']);
		$this->message = $this->module->newsletterRegistration($email);
		$this->url = __PS_BASE_URI__;
	}

	public function initContent()
	{
		parent::initContent();

		$this->context->smarty->assign('url', $this->url);
		$this->context->smarty->assign('message', $this->message);
		$this->setTemplate('subscription.tpl');
	}
}
