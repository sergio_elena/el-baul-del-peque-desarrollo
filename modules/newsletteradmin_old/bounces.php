<?php
/**
 * Suppression des adresses en erreur dans les listes importées  
 * @category admin 
 * @copyright eolia@o2switch.net 
 * @Author Eolia  21/12/2013  compatible PS 1.5.x.x Only! 
 * @version 2.0
 *
 */
include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/functions.php');
$filename = 'BOUNCES';
	$Key = Configuration::get('NEWSLETTER_KEY_CODE');
	if (@$_GET['key'] != $Key) die('Bad request...Wrong key.');
	else 
	{
		if(intval(Configuration::get('PS_REWRITING_SETTINGS')) === 1)
			$rewrited_url = __PS_BASE_URI__;

		Db::getInstance()->Execute('CREATE TABLE IF NOT EXISTS `temp` (`email` VARCHAR(255) NOT NULL PRIMARY KEY) ENGINE=MyISAM DEFAULT CHARSET=utf8') or die(mysql_error());
		
		echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml">
				<head>
					<meta http-equiv="content-type" content="text/html; charset=utf-8" />
					<link rel="stylesheet" type="text/css" href="views/css/newsletteradmin.css"/>
					<title>'.trans('Bounces cleaning').'</title>
				</head>
					<body>
						<div class="newsfield" >
						<center>
						<h1>'.trans('Cleaning email addresses imported invalid in the database').'</h1>
						'.trans('No customer address is deleted').'.
						<hr /> 
						<br/>';
						
		if(isset($_POST['bye'])) 
		{ 
			Db::getInstance()->Execute( 'TRUNCATE TABLE temp') or die (mysql_error());
			echo trans('Operation canceled').'! <meta HTTP-EQUIV="Refresh" content="2">';
		
		}
		
		if($_POST == null) 
		{
			$_POST['in'] = null;
			echo trans('Please copy/paste your text below with').' <a href="#" title="'.trans('Bounce e-mail is electronic mail that is returned to the sender because it cannot be delivered for some reason. There are two kinds of bounce e-mail: hard bounce and soft bounce. - Hard bounce e-mail is permanently bounced back to the sender because the address is invalid. - Soft bounce e-mail is recognized by the recipient\'s mail server but is returned to the sender because the recipient\'s mailbox is full, the mail server is temporarily unavailable, or the recipient no longer has an e-mail account at that address.').'">bounces</a> '.trans('to analyze').':
				<form action="" method="post">
				<textarea cols="40" rows="15" style="width:80%" name="in">'.$_POST['in'].'</textarea><br/><br/>
				<input class="button" type="submit" value="'.trans('Extract emails').'" />
				</form>';
		}
		
		if(isset($_POST['in'])) 
		{ 
			$emails = email_parser($_POST['in']);
			
			if(!empty($emails))
			{
				
				foreach ($emails as $key)
					$insertion = Db::getInstance()->Execute("INSERT IGNORE INTO `temp`(`email`) VALUES('$key')") or die (mysql_error());

				if($insertion)
				{ 
					echo '<br/><fieldset style="width:80%"><legend><b> '.trans('Summary import email addresses').': </b></legend><br/>
					<img src="img/ok.png" width="32" height="32" alt="Ok"/><br/><sup>'.trans('The insertion of data in the database is completed successfully').'.</sup><br /><br/>
					'.trans('Number of addresses added in the base').' : <b>'.count($emails).'</b> <br />
					<br/><pre>'.implode("\n", $emails).'</pre><br/><br/><br/>
					<form action="" method="post">
					<input type="submit" value="'.trans('Clean database').'" name="delete" /><br/>
					<input type="hidden" value="'.count($emails).'" name="nb" /><br/>
					<i> '.trans('Warning this action is irreversible').'!</i><br/><br/>
					<input type="submit" value="'.trans('No, I\'m not sure...').'" name="bye" /><br/>
					</form>
					</fieldset>';
				}	
			}
			else 
			{
				echo '<br/> '.trans('Your text does not contain any email address').' ! <meta HTTP-EQUIV="Refresh" content="2">';
				unset($_POST);
			}
		}
		
		if(isset($_POST['delete'])) 
		{ 
		Db::getInstance()->Execute('SET SQL_BIG_SELECTS=1');
			$sql1= Db::getInstance()->Execute('DELETE imp FROM '._DB_PREFIX_.'mailing_import as imp, temp WHERE imp.email = temp.email AND imp.id_shop = '.$_GET['id_shop']);
			$supp = Db::getInstance()->Affected_Rows($sql1);
			$sql2= Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'customer,temp SET newsletter = 0 WHERE temp.email = '._DB_PREFIX_.'customer.email');
			$news = Db::getInstance()->Affected_Rows($sql2);
			echo trans('Result of the operation on the database').': '._DB_PREFIX_.'mailing_import
			<br/>
			'.trans('Deleted entries').': '.$supp.'/'.$_POST['nb'].'<br/><br/>
			'.trans('Result of the operation on the database').': '._DB_PREFIX_.'customer
			<br/>
			'.trans('Customers accounts updated').': '.$news.'/'.$_POST['nb'];
			Db::getInstance()->Execute('TRUNCATE TABLE temp') or die (mysql_error());
			unset($_POST);
			echo '<meta HTTP-EQUIV="Refresh" content="20">';
			
			
		}
	}	
		echo '			
						</center>
						<div style="font-size:11px;text-align:right; color:grey"> Eolia.o2switch.net &copy;</div>
					</div>
				</body>
			</html>';
?>