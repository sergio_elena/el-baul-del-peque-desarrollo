<?php

include_once(dirname(__FILE__).'/../../../tools/swift/Swift.php');
include_once(dirname(__FILE__).'/../../../tools/swift/Swift/Connection/SMTP.php');
include_once(dirname(__FILE__).'/../../../tools/swift/Swift/Connection/NativeMail.php');
include_once(dirname(__FILE__).'/../../../tools/swift/Swift/Plugin/Decorator.php');

class NewsMail
{
	const TYPE_HTML = 1;
	const TYPE_TEXT = 2;
	const TYPE_BOTH = 3;

	public static function Send($id_lang, $template, $subject, $template_vars, $to,
		$to_name = null, $from = null, $from_name = null, $file_attachment = null, $mode_smtp = null,
		$template_path, $die = false, $id_shop = null, $bcc = null, $configuration = null)
	{
	if($configuration == null) {
		$configuration = Configuration::getMultiple(array(
			'PS_SHOP_EMAIL',
			'PS_MAIL_METHOD',
			'PS_MAIL_SERVER',
			'PS_MAIL_USER',
			'PS_MAIL_PASSWD',
			'PS_SHOP_NAME',
			'PS_MAIL_SMTP_ENCRYPTION',
			'PS_MAIL_SMTP_PORT',
			'PS_MAIL_TYPE',
			'PS_MAIL_DOMAIN'
		), null, null, $id_shop);
	}		

		if (!isset($configuration['PS_MAIL_SMTP_ENCRYPTION']))
			$configuration['PS_MAIL_SMTP_ENCRYPTION'] = 'off';
		if (!isset($configuration['PS_MAIL_SMTP_PORT']))
			$configuration['PS_MAIL_SMTP_PORT'] = 'default';


		//Verify the Return-Path	
		if (!isset($address))
			$address = null;

		if (!is_array($template_vars))
			$template_vars = array();


		/* Construct multiple recipients list if needed */
		$to_list = new Swift_RecipientList();
		$to_plugin = $to;
		$to_list->addTo($to, $to_name);
		$to = $to_list;
		
		try {
			/* Connect with the appropriate configuration */
			if ($configuration['PS_MAIL_METHOD'] == 2)
			{
				if (empty($configuration['PS_MAIL_SERVER']) || empty($configuration['PS_MAIL_SMTP_PORT']))
				{
					die('Error: invalid SMTP server or SMTP port');
				}
				$connection = new Swift_Connection_SMTP($configuration['PS_MAIL_SERVER'], $configuration['PS_MAIL_SMTP_PORT'],
					($configuration['PS_MAIL_SMTP_ENCRYPTION'] == 'ssl') ? Swift_Connection_SMTP::ENC_SSL :
					(($configuration['PS_MAIL_SMTP_ENCRYPTION'] == 'tls') ? Swift_Connection_SMTP::ENC_TLS : Swift_Connection_SMTP::ENC_OFF));
				$connection->setTimeout(4);
				if (!$connection)
					return false;
				if (!empty($configuration['PS_MAIL_USER']))
					$connection->setUsername($configuration['PS_MAIL_USER']);
				if (!empty($configuration['PS_MAIL_PASSWD']))
					$connection->setPassword($configuration['PS_MAIL_PASSWD']);
			}
			else
				$connection = new Swift_Connection_NativeMail();

			if (!$connection)
				return false;
			$swift = new Swift($connection, $configuration['PS_MAIL_DOMAIN']);
			/* Get templates content */
			$template_html = file_get_contents(dirname(__FILE__).'/../mails/newsletter.html');
			$template_txt = strip_tags(html_entity_decode(file_get_contents(dirname(__FILE__).'/../mails/newsletter.txt'), null, 'utf-8'));

			$message = new Swift_Message($subject);

			$message->setCharset('utf-8');

			/* Set Message-ID - getmypid() is blocked on some hosting */
			$message->setId(NewsMail::generateId());
/* set a return-path to manage bounces feedbacks*/
//			if($address != null)
//				$message->headers->set("Return-Path", $address);
			$message->headers->setEncoding('Q');



			$swift->attachPlugin(new Swift_Plugin_Decorator(array($to_plugin => $template_vars)), 'decorator');
			if ($configuration['PS_MAIL_TYPE'] == NewsMail::TYPE_BOTH || $configuration['PS_MAIL_TYPE'] == NewsMail::TYPE_TEXT)
				$message->attach(new Swift_Message_Part($template_txt, 'text/plain', '8bit', 'utf-8'));
			if ($configuration['PS_MAIL_TYPE'] == NewsMail::TYPE_BOTH || $configuration['PS_MAIL_TYPE'] == NewsMail::TYPE_HTML)
				$message->attach(new Swift_Message_Part($template_html, 'text/html', '8bit', 'utf-8'));

			/* Send mail */
			$send = $swift->send($message, $to, new Swift_Address($from, $from_name));
			$swift->disconnect();
			

			return $send;
		}
		catch (Swift_Exception $e) {
			echo $e;
			return false;
		}
	}

	/* Rewrite of Swift_Message::generateId() without getmypid() */
	protected static function generateId($idstring = null)
	{
		$midparams =  array(
			"utctime" => gmstrftime("%Y%m%d%H%M%S"),
			"randint" => mt_rand(),
			"customstr" => (preg_match("/^(?<!\\.)[a-z0-9\\.]+(?!\\.)\$/iD", $idstring) ? $idstring : "swift") ,
			"hostname" => (isset($_SERVER["SERVER_NAME"]) ? $_SERVER["SERVER_NAME"] : php_uname("n")),
		);
		return vsprintf("<%s.%d.%s@%s>", $midparams);
	}

}
