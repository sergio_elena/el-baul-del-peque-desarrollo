$(document).ready(function () {

	var is_touch_device = 'ontouchstart' in document.documentElement;
    if (is_touch_device) {

		$('.flexmenu').click(function(){
			var th = $(this);
			var wdth = $( window ).width();			
			if (wdth < 728) {

				if (th.hasClass("mobile-visible")) {
					th.removeClass("mobile-visible");
				} else {
					th.addClass("mobile-visible");   	        	            
				}
			}
		});

		$('.opener').click(function(){
			var th = $(this).closest("li");
			if (th.hasClass("flexHover")) {
				setTimeout(function() {
				   th.removeClass("flexHover");				      
				}, 200);
			} else {
				setTimeout(function() {
		        	th.addClass("flexHover");		        	         
		    	}, 100);
			}
			return false;
		});

	} else {
			
	    	$(".desctop-scr .flexmenuitem").live({
			    mouseenter: function () {
			    	var th = $(this);
		        	checksubmenuZindex(th);
			        th.addClass("flexHover");   	        	            
			    },
			    mouseleave: function () {
			    	var th = $(this);
					th.removeClass("flexHover");
			    }
			});

	    	$(".mobile-scr .flexmenu").live("click",function(){
	    		var th = $(this);
	    		var clNm = "mobile-visible";
				if (th.hasClass(clNm)) {
			        th.removeClass(clNm); 
				} else {
					th.addClass(clNm);
				}			    
			});

			$(".opener").live("click",function(){
				var th = $(this).closest("li");
				if (th.hasClass("flexHover")) {
					th.removeClass("flexHover");
				} else {
		        	th.addClass("flexHover");   	        	            
				}
				return false;
			});			

	}

	function checksubmenuZindex(th) {
		var zIn = new Array();
		$.each($(".flexmenuitem"), function( index, value ) {
			zIn[index] = $(this).find(".submenu").css("z-index");
			$(this).removeClass('flexHover');
		});
		var highest = getHighest(zIn);
		var h = parseInt(highest);    
	    th.find(".submenu").css({"z-index": h+2});
	}

	function getHighest(array) {
	    var max = 0;
	    for (var i = 0; i < array.length; i++) {	    	
	        if (array[i] > max)
	            max = array[i];
	    }
	    return max;
	}
	
	
   /* var stickyNavTop = $('.flexmenu-container').offset().top;

	var stickyNav = function(){
		var scrollTop = $(window).scrollTop();
		     
		if (scrollTop > stickyNavTop) { 
		    $('.flexmenu-container').addClass('sticky');
		} else {
		    $('.flexmenu-container').removeClass('sticky'); 
		}
	};

	stickyNav();	

	var currentBreakpoint; // default's to blank so it's always analysed on first load
    var didResize  = true; // default's to true so it's always analysed on first load
    // on window resize, set the didResize to true
    $(window).resize(function() { didResize = true; });

    // every 1/2 second, check if the browser was resized
    // we throttled this because some browsers fire the resize even continuously during resize
    // that causes excessive processing, this helps limit that
    setInterval(function() {
        if(didResize) {

            didResize = false;
            var newBreakpoint = $(window).width();

            if (newBreakpoint > 1000) 
                newBreakpoint = "breakpoint_1";
            else if ((newBreakpoint <= 1000) && (newBreakpoint >= 768)) 
                newBreakpoint = "breakpoint_2";
            else if ((newBreakpoint <= 767) && (newBreakpoint >= 480)) 
                newBreakpoint = "breakpoint_3";
            else if (newBreakpoint <= 479) 
                newBreakpoint = "breakpoint_4";

            if (currentBreakpoint != newBreakpoint) {            

                if (newBreakpoint === 'breakpoint_1') {// min-width: 1000px

                    currentBreakpoint = 'breakpoint_1';
                   	$(window).scroll(function() {
						stickyNav();
					});

                }            
                if (newBreakpoint === 'breakpoint_2') {//max-width: 1000px
                    currentBreakpoint = 'breakpoint_2';
          			$(window).scroll(function() {
						stickyNav();
					});

                }               
                if (newBreakpoint === 'breakpoint_3') {// max-width: 768px
                    currentBreakpoint = 'breakpoint_3';

                               

                }               
                if (newBreakpoint === 'breakpoint_4') {//max-width: 479px
                    currentBreakpoint = 'breakpoint_4';

                    

                }   
            }
        }
      }, 500);     */   

});