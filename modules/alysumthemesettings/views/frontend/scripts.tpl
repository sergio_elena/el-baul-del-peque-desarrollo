<script type="text/javascript">
{literal}
$(document).ready(function(){
	{/literal}
	{if $theme_settings.browser != "IE"}
	{literal}
	//$("body, #layered_form > div > div > ul").niceScroll();
	{/literal}
	{/if}
	{if $theme_settings.sett_panel == 1}{literal}

		//$('#switch').click(function(){ // theme settings panel			
	var tc = $('.theme_controller');
	var swt = $('#switch');
	var tcW = tc.width();
	var swtW = swt.width();
	
	swt.live('click', function() {			
		if (tc.hasClass('main_closed')){			
			swt.animate({ right: 0 }, 300, function() {
				tc.removeClass('main_closed').addClass('main_opened');
				tc.animate({ left:0 }, 300, "easeOutExpo");
			});				
			$.cookie("panel", ""); // set cookie
		} else {				
			tc.animate({ left: -tcW }, 300, "easeOutExpo", function(){
				tc.removeClass('main_opened').addClass('main_closed');					
				swt.animate({ right: -swtW }, 300);
			});
			$.cookie("panel", "closed"); // set cookie
		}			
		return false;
	});	

	if($.cookie("panel") != "closed") {
		tc.delay(500).fadeTo("slow", 1);
		tc.delay(1000).animate({ left:0 }, 300, function(){
			tc.removeClass('main_closed').addClass('main_opened');
			swt.animate({ right: 0 }, 300);
		});
	}

	{/literal}
	{/if}{literal}		

		var selectors_bg = 				'{/literal}{$theme_settings.selectors.bg}{literal}';
		var selectors_additional_bg = 	'{/literal}{$theme_settings.selectors.additionalBg}{literal}';
		var selectors_buttons = 		'{/literal}{$theme_settings.selectors.buttons}{literal}';
		var selectors_logo = 			'{/literal}{$theme_settings.selectors.logo}{literal}';
		var selectors_headings = 		'{/literal}{$theme_settings.selectors.headings}{literal}';		
		var selectors_subheadings = 	'{/literal}{$theme_settings.selectors.subheadings}{literal}';		
		var selectors_links = 			'{/literal}{$theme_settings.selectors.links}{literal}';
		var selectors_text = 			'{/literal}{$theme_settings.selectors.text}{literal}';
		var selectors_links = 			'{/literal}{$theme_settings.selectors.links}{literal}';
		var selectors_first_color = 	'{/literal}{$theme_settings.selectors.first_color}{literal}';
		var selectors_second_color = 	'{/literal}{$theme_settings.selectors.second_color}{literal}';
		var selectors_third_color = 	'{/literal}{$theme_settings.selectors.third_color}{literal}';
		var selectors_first_color_back ='{/literal}{$theme_settings.selectors.first_color_back}{literal}';
		var selectors_second_color_back ='{/literal}{$theme_settings.selectors.second_color_back}{literal}';
		var selectors_third_color_back ='{/literal}{$theme_settings.selectors.third_color_back}{literal}';

		/* -------------------------------------------------- PATTERNS  -- */
		
		$("#ptrns .var").click(function() {
			var patt_ID = $(this).data("pid");
	         $("#pattern").removeClass().addClass("back_"+patt_ID); // set class
	         $.cookie("pattern", "back_"+patt_ID); // set cookie
	    });

		if($.cookie("pattern")) { // read pattern cookie
			$("#pattern").removeClass().addClass($.cookie("pattern"));
		}	
		
		/* -------------------------------------------------- COLORS -- */	

		// 															#######	LOGO COLOR
		$('#logo_color').bind('change', function () {
          $(selectors_logo).css('color', $(this).val());
          $.cookie("logo_color", $(this).val()); // set cookie
        });		
		if($.cookie("logo_color")) { // read cookie
			$(selectors_logo).css('color', $.cookie("logo_color"));
			$('#icp_logo_color').css('backgroundColor', $.cookie("logo_color"));
		}		

		// 															#######	BACKGROUND COLOR
        $('#back_color').bind('change', function () {
          $(selectors_bg+', '+selectors_additional_bg).css('backgroundColor', $(this).val());
          $.cookie("background_color", $(this).val()); // set cookie
        });		
		if($.cookie("background_color")) { // read background color cookie
			$(selectors_bg+', '+selectors_additional_bg).css('backgroundColor', $.cookie("background_color"));
			$('#icp_back_color').css('backgroundColor', $.cookie("background_color"));
		}

		// 															#######	BUTTONS COLOR
		$('#buttons_color').bind('change', function () {
          $(selectors_buttons).css('background-color', $(this).val());
          $.cookie("buttons_color", $(this).val()); // set cookie
        });		
		if($.cookie("buttons_color")) { // read background color cookie
			$(selectors_buttons).css('backgroundColor', $.cookie("buttons_color"));
			$('#icp_buttons_color').css('backgroundColor', $.cookie("buttons_color"));
		}
		// 															#######	BUTTONS TEXT COLOR
		$('#buttons_text_color').bind('change', function () {
          $(selectors_buttons).css('color', $(this).val());
          $.cookie("buttons_text_color", $(this).val()); // set cookie
        });		
		if($.cookie("buttons_text_color")) { // read background color cookie
			$(selectors_buttons).css('color', $.cookie("buttons_text_color"));
			$('#icp_buttons_text_color').css('backgroundColor', $.cookie("buttons_text_color"));
		}

		// 															#######	HEADINGS COLOR
		$('#headings_color').bind('change', function () {
          $(selectors_headings).css('color', $(this).val());
          $.cookie("headings_color", $(this).val()); // set cookie
        });		
		if($.cookie("headings_color")) { // read cookie
			$(selectors_headings).css('color', $.cookie("headings_color"));
			$('#icp_headings_color').css('backgroundColor', $.cookie("headings_color"));
		}

		$('#subheadings_color').bind('change', function () {
          $(selectors_subheadings).css('color', $(this).val());
          $.cookie("subheadings_color", $(this).val()); // set cookie
        });		
		if($.cookie("subheadings_color")) { // read cookie
			$(selectors_subheadings).css('color', $.cookie("subheadings_color"));
			$('#icp_subheadings_color').css('backgroundColor', $.cookie("subheadings_color"));
		}

		// 															#######	TEXT COLOR
		$('#text_color').bind('change', function () {
          $(selectors_text).css('color', $(this).val());
          $.cookie("text_color", $(this).val()); // set cookie
        });		
		if($.cookie("text_color")) { // read cookie
			$(selectors_text).css('color', $.cookie("text_color"));
			$('#icp_text_color').css('backgroundColor', $.cookie("text_color"));
		}

		// 															#######	LINKS COLOR
		$('#links_color').bind('change', function () {
          $(selectors_links).css('color', $(this).val());
          $.cookie("links_color", $(this).val()); // set cookie
        });		
		if($.cookie("links_color")) { // read cookie
			$(selectors_links).css('color', $.cookie("links_color"));
			$('#icp_links_color').css('backgroundColor', $.cookie("links_color"));
		}
		// 															#######	COLOR 1
		$('#first_color').bind('change', function () {
          $(selectors_first_color).css('color', $(this).val());
          $(selectors_first_color_back).css('backgroundColor', $(this).val());
          $.cookie("first_color", $(this).val()); // set cookie
        });	
		if($.cookie("first_color")) { // read cookie
			$(selectors_first_color).css('color', $.cookie("first_color"));
			$(selectors_first_color_back).css('backgroundColor', $.cookie("first_color"));
			$('#icp_first_color').css('backgroundColor', $.cookie("first_color"));
		}
		// 															#######	COLOR 2
		$('#second_color').bind('change', function () {
          $(selectors_second_color).css('color', $(this).val());
          $(selectors_second_color_back).css('backgroundColor', $(this).val());
          $.cookie("second_color", $(this).val()); // set cookie
        });		
		if($.cookie("second_color")) { // read cookie
			$(selectors_second_color).css('color', $.cookie("second_color"));
			$(selectors_second_color_back).css('backgroundColor', $.cookie("second_color"));
			$('#icp_second_color').css('backgroundColor', $.cookie("second_color"));
		}
		// 															#######	COLOR 3
		$('#third_color').bind('change', function () {
          $(selectors_third_color).css('color', $(this).val());
          $(selectors_third_color_back).css('backgroundColor', $(this).val());
          $.cookie("third_color", $(this).val()); // set cookie
        });		
		if($.cookie("third_color")) { // read cookie
			$(selectors_third_color).css('color', $.cookie("third_color"));
			$(selectors_third_color_back).css('backgroundColor', $.cookie("third_color"));
			$('#icp_third_color').css('backgroundColor', $.cookie("third_color"));
		}	
		/* -------------------------------------------------- FONTS  -- */

		// 															#######	LOGO FONT SIZE
		$('#logosize').change(function(){ // changing logo fonts
			var size = $("option:selected", this).val();
			$(selectors_logo).css({'fontSize': size+'px'});
			$.cookie("logo_size", size); // set cookie
		});

		if($.cookie("logo_size")) { // read logo_size cookie
			var size = $.cookie("logo_size");
			$(selectors_logo).css({'fontSize': size+'px'});
		}

		// 															#######	LOGO FONT
		$('#logofont').change(function(){ // changing logo fonts
	        var gFontVal = $("option:selected", this).val();
			var gFontName = gFontVal.split(':');
			$.cookie("logo_font", gFontVal); // set cookie
			if ($('head').find('link#logofont_link').length < 1){
				$('head').append('<link id="logofont_link" rel="stylesheet" type="text/css" href="" />');
			}
			$('link#logofont_link').attr({href:'{/literal}{$theme_settings.protocol}{literal}fonts.googleapis.com/css?family=' + gFontName});						

			$("#logofont_style").remove();
			$('head').append('<style id="logofont_style" type="text/css">'+selectors_logo+' {font-family:' + gFontVal + ' !important; }</style>');

		});
		// 															#######	LOGO FONT COOKIE
		if($.cookie("logo_font")) { // read logo_font cookie
			var logo_font = $.cookie("logo_font");
			if ($('head').find('link#logofont_link').length < 1){
				$('head').append('<link id="logofont_link" rel="stylesheet" type="text/css" href="" />');
			}
			$('link#logofont_link').attr({href:'{/literal}{$theme_settings.protocol}{literal}fonts.googleapis.com/css?family=' + logo_font});	

			$("#logofont_style").remove();

			$('head').append('<style id="logofont_style" type="text/css">'+selectors_logo+' {font-family :' + logo_font + ' !important;}</style>');
		}

		/*if($.cookie("slogan_font")) { // read logo_font cookie
			var slogan_font = $.cookie("slogan_font");
			if ($('head').find('link#sloganfont_link').length < 1){
				$('head').append('<link id="sloganfont_link" rel="stylesheet" type="text/css" href="" />');
			}
			$('link#sloganfont_link').attr({href:'httpfonts.googleapis.com/css?family=' + slogan_font});	

			$("#sloganfont_style").remove();

			$('head').append('<style id="sloganfont_style" type="text/css">'+selectors_slogan+' {font-family :' + slogan_font + ' !important;}</style>');
		}*/

		// 															#######	HEADINGS FONT
		$('#headingfont').change(function(){ // changing logo fonts
	        var gFontVal = $("option:selected", this).val();
			var gFontName = gFontVal.split(':');
			$.cookie("headings_font", gFontVal); // set cookie
			if ($('head').find('link#headings_link').length < 1){
				$('head').append('<link id="headings_link" rel="stylesheet" type="text/css" href="" />');
			}
			$('link#headings_link').attr({href:'{/literal}{$theme_settings.protocol}{literal}fonts.googleapis.com/css?family=' + gFontName});						

			$("#headings_style").remove();
			$('head').append('<style id="headings_style" type="text/css">'+selectors_headings+' {font-family:' + gFontVal + ' !important; }</style>');

		});

		// 															#######	HEADINGS FONT COOKIE
		if($.cookie("headings_font")) { // read logo_font cookie
			if ($('head').find('link#headings_link').length < 1){
				$('head').append('<link id="headings_link" rel="stylesheet" type="text/css" href="" />');
			}
			$('link#headings_link').attr({href:'{/literal}{$theme_settings.protocol}{literal}fonts.googleapis.com/css?family=' + $.cookie("headings_font")});	

			$("#headings_style").remove();
			$('head').append('<style id="headings_style" type="text/css">'+selectors_headings+' {font-family :' + $.cookie("headings_font") + ' !important;}</style>');
		}

		$('#subheadingfont').change(function(){ // changing logo fonts
	        var gFontVal = $("option:selected", this).val();
			var gFontName = gFontVal.split(':');
			$.cookie("subheadings_font", gFontVal); // set cookie
			if ($('head').find('link#subheadings_link').length < 1){
				$('head').append('<link id="subheadings_link" rel="stylesheet" type="text/css" href="" />');
			}
			$('link#subheadings_link').attr({href:'{/literal}{$theme_settings.protocol}{literal}fonts.googleapis.com/css?family=' + gFontName});						

			$("#subheadings_style").remove();
			$('head').append('<style id="subheadings_style" type="text/css">'+selectors_subheadings+' {font-family:' + gFontVal + ' !important; }</style>');

		});

		// 															#######	subHEADINGS FONT COOKIE
		if($.cookie("subheadings_font")) { // read logo_font cookie
			if ($('head').find('link#subheadings_link').length < 1){
				$('head').append('<link id="subheadings_link" rel="stylesheet" type="text/css" href="" />');
			}
			$('link#subheadings_link').attr({href:'{/literal}{$theme_settings.protocol}{literal}fonts.googleapis.com/css?family=' + $.cookie("subheadings_font")});	

			$("#subheadings_style").remove();
			$('head').append('<style id="subheadings_style" type="text/css">'+selectors_subheadings+' {font-family :' + $.cookie("subheadings_font") + ' !important;}</style>');
		}

		// 															#######	TEXT FONT
		$('#textfont').change(function(){ // changing logo fonts
	        var gFontVal = $("option:selected", this).val();
			var gFontName = gFontVal.split(':');
			$.cookie("text_font", gFontVal); // set cookie
			if ($('head').find('link#text_link').length < 1){
				$('head').append('<link id="text_link" rel="stylesheet" type="text/css" href="" />');
			}
			$('link#text_link').attr({href:'{/literal}{$theme_settings.protocol}{literal}fonts.googleapis.com/css?family=' + gFontName});						

			$("#text_style").remove();
			$('head').append('<style id="text_style" type="text/css">'+selectors_text+' {font-family:' + gFontVal + ' !important; }</style>');

		});

		// 															#######	TEXT FONT COOKIE
		if($.cookie("text_font")) { // read logo_font cookie
			if ($('head').find('link#text_link').length < 1){
				$('head').append('<link id="text_link" rel="stylesheet" type="text/css" href="" />');
			}
			$('link#text_link').attr({href:'{/literal}{$theme_settings.protocol}{literal}fonts.googleapis.com/css?family=' + $.cookie("text_font")});	

			$("#text_style").remove();

			$('head').append('<style id="text_style" type="text/css">'+selectors_text+' {font-family :' + $.cookie("text_font") + ' !important;}</style>');
		}

		// 															#######	BUTTONS FONT
		$('#buttonfont').change(function(){ // changing logo fonts
		    var gFontVal = $("option:selected", this).val();
			var gFontName = gFontVal.split(':'); // get font name
			$.cookie("button_font", gFontVal); // set cookie
			if ($('head').find('link#buttonfont_link').length < 1){
				$('head').append('<link id="buttonfont_link" rel="stylesheet" type="text/css" href="" />');
			}
			$('link#buttonfont_link').attr({href:'{/literal}{$theme_settings.protocol}{literal}fonts.googleapis.com/css?family=' + gFontName});// put <link> tag to <head>

			$("style#buttonfont_styles").remove(); // remove old <style> tag from <head>
			$('head').append('<style id="buttonfont_styles" type="text/css">'+selectors_buttons+' {font-family :' + gFontVal + ' !important; }</style>'); // add new <style>
		});		
		// 															#######	BUTTONS FONT COOKIE
		if($.cookie("button_font")) { // read logo_font cookie
			if ($('head').find('link#buttonfont_link').length < 1){
				$('head').append('<link id="buttonfont_link" rel="stylesheet" type="text/css" href="" />');
			}
			$('link#buttonfont_link').attr({href:'{/literal}{$theme_settings.protocol}{literal}fonts.googleapis.com/css?family=' + $.cookie("button_font")});	

			$("#buttonfont_styles").remove();
			$('head').append('<style id="buttonfont_styles" type="text/css">'+selectors_buttons+' {font-family :' + $.cookie("button_font") + ' !important;}</style>');
		}		

		/* -------------------------------------------------- COLORS COOKIES -- ############################################# */
		
		if($.cookie("font_size")) { // read font size cookie
			var size = $.cookie("font_size");
			$("a.logo").css('fontSize', size+'px');
		}
		
		if($.cookie("buttons_color")) { // read buttons color cookie
			var buttons_color = $.cookie("buttons_color");
			//$(backs).css('backgroundColor', '#' + buttons_color);
			$('#colorSelector_buttons div').css('backgroundColor', '#' + buttons_color);
		} else {
			{/literal}
				{if isset($theme_settings.buttons_color)}
			{literal}
			//	$(backs).css('backgroundColor', '#{/literal}{$theme_settings.buttons_color}{literal}');			
			{/literal}
				{/if}
			{literal}
		}
		if($.cookie("headings_color")) { // read headings color cookie
			var headings_color = $.cookie("headings_color");
			$(selectors_headings).css('color', '#' + headings_color);
			$('#colorSelector_headings div').css('backgroundColor', '#' + headings_color);
		} else {
			{/literal}
				{if isset($theme_settings.headings_color)}
			{literal}
				$(selectors_headings).css('color', '#{/literal}{$theme_settings.headings_color}{literal}');
				$('#colorSelector_headings div').css('backgroundColor', '#{/literal}{$theme_settings.headings_color}{literal}');
			{/literal}
				{/if}
			{literal}
		}
		if($.cookie("links_color")) { // read links color cookie
			var links_color = $.cookie("links_color");
			$(selectors_links).css('color', '#' + links_color);			
			$('#links_color div').css('backgroundColor', '#' + links_color);
		} else {
			{/literal}
				{if isset($theme_settings.links_color)}
			{literal}
				$(selectors_links).css('color', '#{/literal}{$theme_settings.links_color}{literal}');
			{/literal}
				{/if}
			{literal}
		}
		if($.cookie("text_color")) { // read text color cookie
			var text_color = $.cookie("text_color");
			$(selectors_text).css('color', '#' + text_color);			
			$('#text_color div').css('backgroundColor', '#' + text_color);
		} else {
			{/literal}
				{if isset($theme_settings.text_color)}
			{literal}
				$(selectors_text).css('color', '#{/literal}{$theme_settings.text_color}{literal}');
			{/literal}
				{/if}
			{literal}
		}		

		$('.reset_cookie').click(function(){ // reset customer styles by click to the reset button
	        $.cookie("pattern", null);
	        $.cookie("logo_color", null);
	        $.cookie("background_color", null);
	        $.cookie("buttons_color", null);
	        $.cookie("buttons_text_color", null);
	        $.cookie("headings_color", null);
	        $.cookie("subheadings_color", null);
	        $.cookie("text_color", null);
	        $.cookie("logo", null);
	        $.cookie("logo_size", null);
	        $.cookie("logo_font", null);
	        $.cookie("slogan_font", null);
	        $.cookie("headings_font", null);
	        $.cookie("subheadings_font", null);
	        $.cookie("text_font", null);
	        $.cookie("button_font", null);
	        $.cookie("links_color", null);
	        $.cookie("text_color", null);
	        $.cookie("listingView", null);	        
	        $.cookie("productImageSize", null);	
	        $.cookie("third_color", null);	
	        $.cookie("second_color", null);	
	        $.cookie("first_color", null);
	        $.cookie("scr", null);		                
	        $("style#logofont_styles").remove();
	        $("style#headings_style").remove();
	        $("style#subheadings_style").remove();
	        $("style#text_style").remove();
	        $("style#buttonfont_styles").remove();
	        location.reload();
		});		
		
		/* ------------------------------- PRODUCT LISTING VIEW ----*/
		{/literal}
		{if 
			($page_name == "category") || 
			($page_name == "prices-drop") || 
			($page_name == "new-products") || 
			($page_name == "best-sales") || 
			($page_name == "search") || 
			($page_name == "manufacturer")}
		{literal}			
	        if($.cookie("listingView")) { // read cookie
				var listingView = $.cookie("listingView");
				$('#product_list-container').removeClass("view_list view_grid").addClass(listingView); // set product view class
				$(".pk_view").removeClass("act_btn"); // set class
				$("#"+listingView).addClass("act_btn");
				if (listingView == "view_list") {					
					$(".pk_size").addClass('hidden')
				} else if (listingView == "view_grid") {
					$(".pk_size").removeClass('hidden')
				}				
			} 
			/*	Product images sizes 	*/			
	        if($.cookie("productImageSize")) { // read cookie
				var productImageSize = $.cookie("productImageSize");
				$('#product_list-container').removeClass("view_big view_small").addClass(productImageSize); // set product image size
				$(".pk_size").removeClass("act_btn"); // set class
				$("#"+productImageSize).addClass("act_btn");
			} 
			$.get(baseDir+'modules/alysumthemesettings/ajax.php', { theme_settings: "get" }, function(data) {
				var opt = JSON.parse(data);
				if (opt.view == 0) {								  		
			  		if(!($.cookie("listingView"))) { // if cookie is not set
			  			$("#product_list").addClass("view_grid");
			  			$(".pk_size").removeClass('hidden')
			  		}
			  	} else {			  		
			  		if(!($.cookie("listingView"))) { // if cookie is not set
			  			$("#product_list").addClass("view_list");
				  		$(".pk_size").addClass('hidden')
					}
			  	}
			  	if(!($.cookie("productImageSize"))) { // if cookie is not set
				  	if (opt.productImageSize == 1) {
				  		$("#product_list").addClass("view_big");
				  	} else {
				  		$("#product_list").addClass("view_small");
				  	}	
				}			  	
			});
		{/literal}
		{/if}
		{literal}

		
		// ToolTips		
		//applies to all elements with title attribute. Change to ".class[title]" to select only elements with specific .class and title		
		{/literal}
		{if $theme_settings.tooltips == 221 && $theme_settings.browser == "" }{literal}
			var is_touch_device = 'ontouchstart' in document.documentElement;
    		if (!is_touch_device) {
				$("[title]").style_my_tooltips();
			}
			{/literal}
		{/if}
		// ScrollTo
		{if ($theme_settings.toTop == 1) && (!$content_only)}{literal}
			$(window).scroll(function () {
				var position = $("#scrollTop").offset();
				//$("#scrollTop").text(position.top);
				if (position.top < 800) {
					$("#scrollTop").fadeOut(600);
				} else {
					$("#scrollTop").fadeIn(600);
				}
		    });
			$("#scrollTop").click(function(){
				$.scrollTo( 0, 800 );
			});{/literal}	
		{/if}

		{if ($theme_settings.sticky_menu == 1) && (!$content_only)}
			var stickyNavTop = $('.flexmenu').offset();

			var stickyNav = function(){
				var scrollTop = $(window).scrollTop();
			     
				if (scrollTop > stickyNavTop.top) { 
				    $('.flexmenu').addClass('sticky');
				} else {
				    $('.flexmenu').removeClass('sticky'); 
				}
			};

			stickyNav();
			$(window).scroll(function() {
				stickyNav();
			});
		{/if}
		{literal}
});
{/literal}
</script>