{if $theme_settings.socials_in_footer == 1}
<div id="socialnetworks">
<ul class="socialnetworks_menu">
{foreach from=$soc item=s key=name}
<li class="{$name}"><a title="{$name}" target="_blank" href="{$s}"><img src="{$module_dir}images/social_icons/{$name}.png" alt="" /></a></li>
{/foreach}
</ul>
</div>
{/if}
{if $theme_settings.pay_in_footer == 1}
<div id="payment-icons">
<ul>
{foreach from=$pay item=s key=name}
<li class="{$name}"><img src="{$module_dir}images/payment_icons/32/{$name}.png" alt="" /></li>
{/foreach}
</ul>
</div>
{/if}