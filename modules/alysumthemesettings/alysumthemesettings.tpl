{if $theme_settings.toTop == 1}
<div id="scrollTop"><a href="#white_bg"></a></div>
{/if}
{if $theme_settings.sett_panel == 1}	
	<div class="theme_controller main_closed">	
		<div id="navigation" class="theme_sett_heading">
            <ul>
                <li class="selected ts-ic01"><a data-sid="1" href="#"></a></li>
                <li class="ts-ic02"><a data-sid="2" href="#"></a></li>
                <li class="ts-ic03"><a data-sid="3" href="#"></a></li>
                <li class="ts-ic04"><a data-sid="4" href="#"></a></li>
            </ul>
        </div>
		<div id="switch"><div id="handler" class="closed"></div></div>
		<div class="ts-wrapper">
			<ul id="identifier">			
				<li class="section" data-cid="1">
					<div class="section-wrapper">			
						<div class="va-content">
							<h5>{l s='Patterns' mod='avenathemesettings'}</h5>
							<div class="sect clearfix" id="ptrns">				
								{section name=foo start=1 loop=$theme_settings.patternsQuantity+1 step=1}
									<div id="back_{$smarty.section.foo.index-1}" data-pid="{$smarty.section.foo.index-1}" class="var{if $smarty.section.foo.index%6}{else} last_item{/if}"></div>
								{/section}				
							</div>
						</div>
						<div class="va-content clearfix">
							<h5>{l s='Background color' mod='avenathemesettings'}</h5>
							<div class="flRight">
								<input type="color" name="back_color" data-text="hidden" class="colorpicker" id="back_color" value="{$theme_settings.back_color}" />							
							</div>
						</div>
						<div class="clearfix">
							<a href="#" class="reset_cookie button" title="{l s='Reset all custom styles' mod='avenathemesettings'}">{l s='Reset' mod='avenathemesettings'}</a>
						</div>
					</div>
				</li>
				<li class="section" data-cid="2">
					<h5>{l s='Use layout you like' mod='avenathemesettings'}</h5>	
					<div class="section-wrapper">
						<div class="va-content">
							<div class="slide_section">
								<input type="radio" id="wide_scr" {if $theme_settings.widescreen == "1"}checked="checked"{/if} name="scr" value="1"  />
								<label id="lbl_wide" class="scrlabel wide{if $theme_settings.widescreen == 1} slctd{/if}" for="wide_scr"></label>
								{l s='WideScreen' mod='avenathemesettings'}
							</div>
							<div class="slide_section">
								<input type="radio" id="boxed_scr" {if $theme_settings.widescreen == "0"}checked="checked"{/if} name="scr" value="0" />
								<label id="lbl_box" class="scrlabel boxed{if $theme_settings.widescreen == 0} slctd{/if}" for="boxed_scr"></label>
								{l s='Boxed Layout' mod='avenathemesettings'}
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="va-content">
							<div class="slide_section">
								<input type="radio" id="left_col" {if $theme_settings.column == "left"}checked="checked"{/if} name="columns" value="left"  />
								<label class="sliderlabel left_col{if $theme_settings.column == "left"} slctd{/if}" for="left_col"></label>
								{l s='Left Column' mod='avenathemesettings'}
							</div>
							<div class="slide_section">
								<input type="radio" id="right_col" {if $theme_settings.column == "right"}checked="checked"{/if} name="columns" value="right" />
								<label class="sliderlabel right_col{if $theme_settings.column == "right"} slctd{/if}" for="right_col"></label>
								{l s='Right Column' mod='avenathemesettings'}
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix">
							<a href="#" class="reset_cookie button" title="{l s='Reset all custom styles' mod='avenathemesettings'}">{l s='Reset' mod='avenathemesettings'}</a>
						</div>
					</div>
				</li>
				<li class="section theme-colors" data-cid="3">
					<h5>{l s='Create new color scheme' mod='avenathemesettings'}</h5>
					<div class="section-wrapper">
						<div class="va-content">
							<div class="col01">
								<input type="color" name="first_color" data-hex="true" class="colorpicker" id="first_color" value="{$theme_settings.first_color}" /><br/>
								{l s='Color 1' mod='avenathemesettings'}
							</div>
							<div class="col02">
								<input type="color" name="second_color" data-hex="true" class="colorpicker" id="second_color" value="{$theme_settings.second_color}" /><br/>
								{l s='Color 2' mod='avenathemesettings'}
							</div>
							<div class="col01">
								<input type="color" name="third_color" data-hex="true" class="colorpicker" id="third_color" value="{$theme_settings.third_color}" /><br/>
								{l s='Color 3' mod='avenathemesettings'}
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix">
							<a href="#" class="reset_cookie button" title="{l s='Reset all custom styles' mod='avenathemesettings'}">{l s='Reset' mod='avenathemesettings'}</a>
						</div>
					</div>
				</li>	
				<li class="section typogr" data-cid="4">
					<h5>{l s='Change text styles of the shop' mod='avenathemesettings'}</h5>
					<div class="section-wrapper">			
						<div class="va-content">
							<h6>{l s='Logo' mod='avenathemesettings'}</h6>
							<div class="controller slct">
								<div class="flLeft">
									<select name="logofont" id="logofont" class="chzn-select">
									{foreach from=$theme_settings.fonts_list key=k item=v}
										{if $theme_settings.logo_font == $v}{$selected = 'selected'}{else}{$selected = ''}{/if}
										<option {$selected} value="{$v}">{$v}</option>
									{/foreach}
									</select>
								</div>
								<div class="flRight">
						            <input type="color" name="logo_color" data-hex="true" class="colorpicker" id="logo_color" value="{$theme_settings.logo_color}" />
								</div>
								<div class="clearfix"></div>
							</div>					
							<h6>{l s='Main Font' mod='avenathemesettings'}</h6>								
							<div class="controller">								
								<div class="controller slct">
									<div class="flLeft">
										<select name="headingfont" id="headingfont" class="chzn-select">
										{foreach from=$theme_settings.fonts_list key=k item=v}
											{if $theme_settings.heading_font == $v}{$selected = 'selected'}{else}{$selected = ''}{/if}
											<option {$selected} value="{$v}">{$v}</option>
										{/foreach}
										</select>
									</div>
									<div class="flRight">
							            <input type="color" name="headings_color" data-hex="true" class="colorpicker" id="headings_color" value="{$theme_settings.headings_color}" />
									</div>
									<div class="clearfix"></div>
								</div>	
							</div>
							<h6>{l s='Secondary Font' mod='avenathemesettings'}</h6>								
							<div class="controller sec_font">								
								<div class="flLeft">
									<select name="subheadingfont" id="subheadingfont" class="chzn-select">
									{foreach from=$theme_settings.fonts_list key=k item=v}
										{if $theme_settings.subheading_font == $v}{$selected = 'selected'}{else}{$selected = ''}{/if}
										<option {$selected} value="{$v}">{$v}</option>
									{/foreach}
									</select>
								</div>
								<div class="flRight">
						            <input type="color" name="subheadings_color" data-hex="true" class="colorpicker" id="subheadings_color" value="{$theme_settings.subheadings_color}" />
								</div>
								<div class="clearfix"></div>
							</div>
							<h6>{l s='Buttons Font' mod='avenathemesettings'}:</h6>
							<div class="controller butt_font">								
								<div class="flLeft">
									<select name="buttonfont" id="buttonfont" class="chzn-select">
									{foreach from=$theme_settings.fonts_list key=k item=v}
										{if $theme_settings.buttons_font == $v}{$selected = 'selected'}{else}{$selected = ''}{/if}
										<option {$selected} value="{$v}">{$v}</option>
									{/foreach}
									</select>
								</div>		
								<div class="flRight">
									<input type="color" name="buttons_color" data-hex="true" class="colorpicker" id="buttons_color" value="{$theme_settings.buttons_color}" />
								</div>
								<div class="clearfix"></div>
							</div>	
						</div>						
						<div class="clearfix">
							<a href="#" class="reset_cookie button" title="{l s='Reset all custom styles' mod='avenathemesettings'}">{l s='Reset' mod='avenathemesettings'}</a>
						</div>					
					</div>
				</li>
			</ul>			
		</div>
	</div>
{/if}