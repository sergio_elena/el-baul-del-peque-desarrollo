CREATE TABLE IF NOT EXISTS `PREFIX_pk_theme_settings` (
`id_setting` int(10) unsigned NOT NULL AUTO_INCREMENT,
`id_shop` int(10) unsigned NOT NULL,
`name` VARCHAR(50),
`value` VARCHAR(9999),
PRIMARY KEY (`id_setting`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_pk_product_extratabs` (
`id_pet` int(10) unsigned NOT NULL AUTO_INCREMENT,
`id_product` INT(10) UNSIGNED NOT NULL,
`shop_id` INT(2) UNSIGNED NOT NULL,
`lang_id` INT(2) UNSIGNED NOT NULL,
`video` VARCHAR(100) NOT NULL,
`custom_tab_name` VARCHAR(100) NOT NULL,
`custom_tab` TEXT NOT NULL,
PRIMARY KEY (`id_pet`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;
## PROMO MINIC SLIDER

#DROP TABLE IF EXISTS `PREFIX_minic_slider`;
CREATE TABLE IF NOT EXISTS `PREFIX_minic_slider` (
`id_slide` int(10) unsigned NOT NULL AUTO_INCREMENT,
`id_shop` int(10) unsigned NOT NULL,
`id_lang` int(10) unsigned NOT NULL,
`id_order` int(10) unsigned NOT NULL,
`lang_iso` VARCHAR(5),
`title` VARCHAR(100),
`url` VARCHAR(100),
`target` tinyint(1) unsigned NOT NULL DEFAULT 0,
`image` VARCHAR(100),
`alt` VARCHAR(100),
`caption` VARCHAR(300),
`active` tinyint(1) unsigned NOT NULL DEFAULT 1,
PRIMARY KEY (`id_slide`, `id_shop`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;

## SEQUENCE SLIDER

#DROP TABLE IF EXISTS `PREFIX_minic_slider_adv`;
CREATE TABLE IF NOT EXISTS `PREFIX_minic_slider_adv` (
`id_slide` int(10) unsigned NOT NULL AUTO_INCREMENT,
`id_shop` int(10) unsigned NOT NULL,
`id_lang` int(10) unsigned NOT NULL,
`id_order` int(10) unsigned NOT NULL,
`lang_iso` VARCHAR(5),
`title` VARCHAR(100),
`url` VARCHAR(100),
`target` tinyint(1) unsigned NOT NULL DEFAULT 0,
`image` VARCHAR(100),
`subimages` TEXT(255),
`subimages_sett` TEXT(255),
`slide_text_sett` TEXT(255),					
`text_width` VARCHAR(5),
`alt` VARCHAR(100),
`caption` VARCHAR(300),
`active` tinyint(1) unsigned NOT NULL DEFAULT 1,
PRIMARY KEY (`id_slide`, `id_shop`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;

## MINIC SLIDER

#DROP TABLE IF EXISTS `PREFIX_minic_slider_nivo`;
CREATE TABLE IF NOT EXISTS `PREFIX_minic_slider_nivo` (
`id_slide` int(10) unsigned NOT NULL AUTO_INCREMENT,
`id_shop` int(10) unsigned NOT NULL,
`id_lang` int(10) unsigned NOT NULL,
`id_order` int(10) unsigned NOT NULL,
`lang_iso` VARCHAR(5),
`title` VARCHAR(100),
`url` VARCHAR(100),
`target` tinyint(1) unsigned NOT NULL DEFAULT 0,
`image` VARCHAR(100),
`alt` VARCHAR(100),
`caption` VARCHAR(300),
`active` tinyint(1) unsigned NOT NULL DEFAULT 1,
PRIMARY KEY (`id_slide`, `id_shop`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;


## AWSLIDER SLIDER
#DROP TABLE IF EXISTS `PREFIX_pk_awShowcaseSlider`;
CREATE TABLE IF NOT EXISTS `PREFIX_pk_awShowcaseSlider` (
`id_slide` int(10) unsigned NOT NULL AUTO_INCREMENT,
`id_shop` int(10) unsigned NOT NULL,
`id_lang` int(10) unsigned NOT NULL,
`id_order` int(10) unsigned NOT NULL,
`lang_iso` VARCHAR(5),
`title` VARCHAR(100),
`url` VARCHAR(100),
`video_url` VARCHAR(100),
`video` tinyint(1) unsigned NOT NULL DEFAULT 0,
`target` tinyint(1) unsigned NOT NULL DEFAULT 0,
`image` VARCHAR(100),
`alt` VARCHAR(100),
`caption` VARCHAR(300),
`active` tinyint(1) unsigned NOT NULL DEFAULT 1,
PRIMARY KEY (`id_slide`, `id_shop`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;


#DROP TABLE IF EXISTS `PREFIX_pk_awShowcaseSlider_points`;
CREATE TABLE IF NOT EXISTS `PREFIX_pk_awShowcaseSlider_points` (
  `id_rec` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_coord` varchar(100) DEFAULT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `id_slide` int(10) unsigned NOT NULL,
  `coordinateX` int(10) unsigned NOT NULL,
  `coordinateY` int(10) unsigned NOT NULL,
  `point_type` varchar(10) DEFAULT NULL,
  `product_name` varchar(100) DEFAULT NULL,
  `product_link_rewrite` varchar(100) DEFAULT NULL,
  `product_image` varchar(100) DEFAULT NULL,
  `product_image_link` varchar(400) DEFAULT NULL,
  `id_product` int(10) DEFAULT NULL,
  `point_text` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_rec`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11;


#DROP TABLE IF EXISTS `PREFIX_pk_small_slider`;
CREATE TABLE IF NOT EXISTS `PREFIX_pk_small_slider` (
  `id_slide` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `id_order` int(10) unsigned NOT NULL,
  `lang_iso` varchar(5) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `target` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `image` varchar(100) DEFAULT NULL,
  `alt` varchar(100) DEFAULT NULL,
  `caption` varchar(300) DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_slide`,`id_shop`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10;