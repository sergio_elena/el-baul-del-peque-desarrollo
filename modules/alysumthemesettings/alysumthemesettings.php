<?php
/**
 * $ModDesc
 * 
 * @version		1.3
 * @package		modules
 * @copyright	Copyright (C) March 2014 http://promokit.eu <@email:support@promokit.eu>. All rights reserved.
 * @license		GNU General Public License version 2
 */
if (!defined('_PS_VERSION_'))
	exit;

class alysumthemesettings extends Module
{

	public $pattern = '/^([A-Z_]*)[0-9]+/';
	public $page_name = '';

	public function __construct()
	{
		$this->version = "1.3";
		$this->versions = 'ATS v.'.$this->version.' | Alysum theme v.3.0 | PS v.'._PS_VERSION_;		
		$this->name = 'alysumthemesettings';		
		$this->tab = 'front_office_features';
		$this->author = 'promokit.eu';
		$this->need_instance = 0;
		$this->maxSize = 81;
		$this->bootstrap = false;

		parent::__construct();

		$this->displayName = $this->l('Alysum Theme Settings');
		$this->description = $this->l('Add extended theme settings');

		$this->config = $this->local_path.'config.json';
		$this->mailArchive = "http://promokit.eu/share/alysum/"._PS_VERSION_."/Mail.zip";

		$this->patternsQuantity = 24;

		$string = file_get_contents($this->config);
		$json_arr = json_decode($string, true);	

		$this->defaultFonts = array(
			"logo" => $json_arr["logo_font"],
			"slogan" => $json_arr["slogan_font"],
			"buttons" => $json_arr["buttons_font"],
			"headings" => $json_arr["heading_font"],
			"text" => $json_arr["text_font"]
		);
		$this->systemFonts = array("Arial", "Tahoma", "Georgia", "Times New Roman", "Verdana", "FontAwesome", "LMRoman10Regular", "LMRomanDunhill10Regular", "TrajanProRegular", "LMRomanCaps10-Regular", "Serif12BetaRgRegular", "lm_roman_demi_10regular");

		$this->selectors = array(
			"logo_container" => "body #header #header_logo",
			"logo" => "#header_logo .logo",
			"slogan" => "#header_logo .slogan",			
			"headings" => "#columns h1, #columns h1 a, #columns h2, #columns h2 a, #columns h3, #columns h3 a, #columns h4, #columns h4 a, #columns h5, #columns h5 a, h2.productscategory_h2, .layered_subtitle, #isotopeSort h5",			
			"subheadings" => "",
			"text" => "p",
			"links" => "#page a",
			"bg" => "body, #header_user #shopping_cart, #footer",
			"buttons" => "input.button_mini, input.button_small, input.button, input.button_large, input.button_mini_disabled, input.button_small_disabled, input.button_disabled, input.button_large_disabled, input.exclusive_mini, input.exclusive_small, input.exclusive, input.exclusive_large, input.exclusive_mini_disabled, input.exclusive_small_disabled, input.exclusive_disabled, input.exclusive_large_disabled, #page a.button_mini, #page a.button_small, #page a.button, #page a.button_large, #page a.exclusive_mini, #page a.exclusive_small, #page a.exclusive, #page a.exclusive_large, span.button_mini, span.button_small, span.button, span.button_large, span.exclusive_mini, span.exclusive_small, span.exclusive, span.exclusive_large, span.exclusive_large_disabled, .cart_navigation .button, .cart_navigation .button_large, .cart_voucher .submit input.button, .promo_section li .button, #tab_4 .button, button.exclusive",
			"buttons_hover" => "input.button_mini:hover, input.button_small:hover, input.button:hover, input.button_large:hover, input.button_mini_disabled:hover, input.button_small_disabled:hover, input.button_disabled:hover, input.button_large_disabled:hover, input.exclusive_mini:hover, input.exclusive_small:hover, input.exclusive:hover, input.exclusive_large:hover, input.exclusive_mini_disabled:hover, input.exclusive_small_disabled:hover, input.exclusive_disabled:hover, input.exclusive_large_disabled:hover, a.button_mini:hover, a.button_small:hover, a.button:hover, a.button_large:hover, a.exclusive_mini:hover, a.exclusive_small:hover, a.exclusive:hover, a.exclusive_large:hover, span.button_mini:hover, span.button_small:hover, span.button:hover, span.button_large:hover, span.exclusive_mini:hover, span.exclusive_small:hover, span.exclusive:hover, span.exclusive_large:hover, span.exclusive_large_disabled:hover, .cart_navigation .button:hover, .cart_navigation .button_large:hover, .cart_voucher .submit input.button:hover, #idTab4 ul li .ajax_add_to_cart_button:hover, .promo_section li .button:hover, #tab_4 .button:hover, button.exclusive:hover",
			"first_color_back" => "",
			"first_color" => "",
			"second_color" => "",
			"second_color_back" => "",
			"third_color_back" => "",
			"third_color" => "",
			"logo_container_admin" => ".logo-container",
			"logo_admin" => "#logofont_example",
			"slogan_admin" => "#sloganfont_example",
			"headings_admin" => ".tabcontent #heading-example h5",
			"subheadings_admin" => ".tabcontent #heading-example h6",
			"text_admin" => ".tabcontent #text-example",
			"links_admin" => ".tabcontent #link-example a",
			"custom_heading" => "",
			"additionalBg" => "body #pattern",
		);
		$this->tabs = array("General Settings", "Background","Logo","Buttons","Typography & Colors","Home Page", "Category page", "Product Page", "Custom CSS", "Email Settings", "Social Accounts", "Coming Soon Page", "Payment Icons");

		$this->bImage = $this->errors = "";
		$this->noImage = "No image selected";
		$this->settingsFile = $this->local_path."css/customsettings".(int)Context::getContext()->shop->id.".css";		

	}


	public function install()
	{
	
    	$allLanguages = Language::getLanguages(false);				
		$id_shop = (int)Shop::getContextShopID();
		$tablesfile = 'sql/install.sql';
		$datafile = 'sql/installData.sql';			
		$msg .= '<div class="conf confirm">'.$this->l('Demo Data Installed Successfully').'</div>';
		if (!file_exists(dirname(__FILE__).'/'.$tablesfile)) 
			$msg = '<div class="conf error">'.$this->l('There is no sql file.').'</div>';
		else if (!$sql = file_get_contents(dirname(__FILE__).'/'.$tablesfile)) {
			$msg = '<div class="conf error">'.$this->l('There is no sql code.').'</div>';
		} else {
			$queries = str_replace(array('PREFIX_', 'ENGINE_TYPE'), array(_DB_PREFIX_, _MYSQL_ENGINE_), $sql);
			$queries = preg_split("/;\s*[\r\n]+/", $queries);
			foreach ($queries AS $query)
				if($query)
					if(!Db::getInstance()->execute(trim($query)))
						$msg = '<div class="conf error">'.$this->l('Error in SQL syntax of Tables').'</div>';

		}
		if (!file_exists(dirname(__FILE__).'/'.$datafile)) 
			$msg = '<div class="conf error">'.$this->l('There is no sql file.').'</div>';
		else if (!$sql = file_get_contents(dirname(__FILE__).'/'.$datafile)) {
			$msg = '<div class="conf error">'.$this->l('There is no sql code.').'</div>';
		} else {
			$queries = "";
			$id1 = 1;$id2 = 2;$id3 = 3;$id4 = 4;$id5 = 5;
			foreach ($allLanguages as $key => $lang) {					
				$queries .= str_replace(array('PREFIX_', 'IDLANG', 'LANGISO', 'IDSHOP', 'ID1', 'ID2', 'ID3', 'ID4', 'ID5'), array(_DB_PREFIX_, $lang["id_lang"], $lang["iso_code"], $id_shop, $id1, $id2, $id3, $id4, $id5), $sql);
				$id1=$id1+5;$id2=$id2+5;$id3=$id3+5;$id4=$id4+5;$id5=$id5+5;
			}

			$queries = preg_split("/;\s*[\r\n]+/", $queries);
			foreach ($queries AS $query)
				if($query)
					if(!Db::getInstance()->execute(trim($query)))
						$msg = '<div class="conf error">'.$this->l('Error in SQL syntax of Data').'</div>';

		}	

		Configuration::updateValue('PS_ALLOW_MOBILE_DEVICE', 0); // disable default mobile theme
		Configuration::updateValue('PS_NAVIGATION_PIPE', "/"); // change default pipe in breadcrumb
		Configuration::updateValue('FOOTER_POWEREDBY', 0); // disable powerby footer option
		Configuration::updateValue('FOOTER_CMS', NULL);	// disable all cms links in footer

		if (parent::install() && 
			$this->registerHook('displayHeader') &&
			$this->registerHook('displayAdminHomeQuickLinks') &&			
			$this->registerHook('freePosition') &&
			$this->registerHook('productVideo') &&
			$this->registerHook('displayAdminProductsExtra') &&
			$this->registerHook('actionProductUpdate') &&
            $this->registerHook('productTab') &&
            $this->registerHook('productTabContent') &&
            $this->registerHook('comingsoon') &&
            $this->registerHook('footer_bottom') /*&&            
            $this->runSql($sql)*/ &&
            $this->importDefaultData()) {
            $this->emailFilesUpdate();
			return true;	
		} else {			
			$this->uninstall();
			return false;		
		}
	}

	public function uninstall() {
        $sql = array();
		$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'pk_theme_settings`';
        if (!parent::uninstall() OR
            !$this->runSql($sql)) {
            return FALSE;
        } else {
        	unlink(_PS_OVERRIDE_DIR_.'classes/Mail.php');
        	$this->clearCache(_PS_CACHE_DIR_.'class_index.php');
        	return TRUE;
        }        
    }

    public function importDefaultData($add = false) {

    	$sql = array();
    	$sid = (int)Shop::getContextShopID();    	    	

		$string = file_get_contents($this->config);
		$json_arr = json_decode($string, true);

		if($json_arr === null) {			
			return false;
		} else {
			if ($add == true) {
	    		$s = $this->getOptions();
	    		$json_arr = array_diff_key($json_arr, $s);
	    	}
			$sqlPart = 'INSERT INTO `'._DB_PREFIX_.'pk_theme_settings` (`id_shop`, `name`, `value`) VALUES ';
			$num = count($json_arr);

			$counter = 1;		
			foreach ($json_arr as $name => $value) {
				if ($counter == $num) {$coma = ";";} else {$coma = ",";}
			    $sqlPart .= "(".$sid.", \"".$name."\", \"".$value."\")".$coma;
			    $counter++;
			}
			$sql[] = $sqlPart;
			$this->runSql($sql);
			return true;
		}
	}

    public function clearCache($name) {
    	
		if (!unlink($name)) {
			$cleared = "<div class=\"conf error\">Cache doesn't cleared</div>";
        } else {
			$cleared = "<div class=\"conf confirm\">Cache cleared</div>";	            	
        }		   		                    		
		return $cleared;
	}	

	public function runSql($sql) {
        foreach ($sql as $s) {
			if (!Db::getInstance()->Execute($s)) return false;
        }
        return true;
    }

	public function fontslist() {
		include_once(dirname(__FILE__).'/fontlist.php');
		return get_fonts();
	}

	public function savePosition($num) {
		$sql = array();
		$sql[] = 'UPDATE `'._DB_PREFIX_.'pk_theme_settings` SET value = "'.$num.'" WHERE name = "tab_number" AND id_shop = '.(int)Context::getContext()->shop->id.';';
		$this->runSql($sql);
	}

	public function getContent()
	{			
	    $s = $this->getOptions();
	    $sid = (int)Context::getContext()->shop->id;
		$lid = $this->context->language->id;	    
				
		$this->context->controller->addJS(_PS_JS_DIR_.'jquery/plugins/jquery.colorpicker.js');		
		$this->context->controller->addJS(($this->_path).'js/ts_scripts.js'); // add JS to back office
		$this->context->controller->addCSS(($this->_path).'css/themesettings_admin.css'); // add CSS to back office
	
		$msg = $err = $output = '';

		if (Tools::isSubmit('submitDeleteImgConf')) {

			if($this->context->employee->id_profile == 1) {

				$res = $this->deleteImg($s["back_image"], "back_image", (int)(Tools::getValue("tab_number")), $sid, $lid);

				if ($res == "") {$msg .= '<div class="conf confirm">'.$this->l('Image removed').'</div>';}
					else {$msg .= '<div class="conf error">'.$res.'</div>';}

			} else {
				$msg .= '<div class="conf error">'.$this->l('Access denied').'</div>';
			}

		}

		if (Tools::isSubmit('submitDeleteEmailImg')) {

			if($this->context->employee->id_profile == 1) {

				$res = $this->deleteImg($s["email_image"], "email_image", (int)(Tools::getValue("tab_number")), $sid, $lid);

				if ($res == "") {$msg .= '<div class="conf confirm">'.$this->l('Image removed').'</div>';}
					else {$msg .= '<div class="conf error">'.$res.'</div>';}

			} else {
				$msg .= '<div class="conf error">'.$this->l('Access denied').'</div>';
			}

		}
			

		if (Tools::isSubmit('resetThemeSettings'))
		{	
			$sql = array();
			$sql[] = 'DELETE FROM `'._DB_PREFIX_.'pk_theme_settings` WHERE id_shop='.(int)Shop::getContextShopID().';';
			$this->runSql($sql);

			$this->importDefaultData();
			$this->cssWriter();
			$msg = '<div class="conf confirm">'.$this->l('Settings reseted').'</div>';

		}				
		
		if (Tools::isSubmit('back_image_upload')) {	

			if($this->context->employee->id_profile == 1) {

				$img = $this->addImage($_FILES, "back_image", $sid, $lid, (int)(Tools::getValue('tab_number')));				
				$msg .= $img["error"].'<div class="conf confirm">'.$this->l('Settings updated').'</div>';	

			} else {
				$msg .= '<div class="conf error">'.$this->l('Access denied').'</div>';			
			}

		}

		if (Tools::isSubmit('email_image_upload')) {	

			if($this->context->employee->id_profile == 1) {
				$img = $this->addImage($_FILES, "email_image", $sid, $lid, (int)(Tools::getValue('tab_number')));				
				$msg .= $img["error"].'<div class="conf confirm">'.$this->l('Settings updated').'</div>';	

			} else {
				$msg .= '<div class="conf error">'.$this->l('Access denied').'</div>';			
			}

		}

		if (Tools::isSubmit('submitThemeSettings'))
		{
			if($this->context->employee->id_profile == 1) {

				$s = $this->getOptions();
				$sid = (int)Context::getContext()->shop->id;

				// check if no data for current shop - import it.
				$check = Db::getInstance()->ExecuteS('SELECT id_setting FROM `'._DB_PREFIX_.'pk_theme_settings` WHERE name = "responsive" AND id_shop = '.$sid.';');
				
        		if (empty($check)) $this->importDefaultData();	

				$sql = array();
				foreach ($_POST as $key => $value) {
					if ($key != "maintenance") {
						$sql[] = 'UPDATE `'._DB_PREFIX_.'pk_theme_settings` SET value = "'.$value.'" WHERE name = "'.$key.'" AND id_shop = '.$sid.';';
					} elseif ($key == "maintenance") {
						Configuration::updateValue('PS_SHOP_ENABLE', $value);						
					}					
					if (($key == "ready_year") || ($key == "ready_month") || ($key == "ready_day") || ($key == "ready_hour") || ($key == "ready_min")) {
						if ($s[$key] != $value) {
							$sql[] = 'UPDATE `'._DB_PREFIX_.'pk_theme_settings` SET value = "'.round(microtime(true)).'" WHERE name = "date_set" AND id_shop = '.$sid.';';
						}
					}
				}
				$this->runSql($sql);					

				// ######## write settings to file							
				$this->cssWriter();								
				if ($this->errors) { $errors = '<div class="conf error">'.$this->errors.'</div>'; } else $errors = "";
				$msg .= $errors.'<div class="conf confirm">'.$this->l('Settings updated').'</div>';	
			} else {
				$msg .= '<div class="conf error">'.$this->l('Access denied').'</div>';	
			}
					
		}

		if (Tools::isSubmit('fixEmails')) {	

			if($this->context->employee->id_profile == 1) {
				
				$msg = $this->emailFilesUpdate();		
				$this->savePosition((int)(Tools::getValue('tab_number')));				

			} else {
				$msg .= '<div class="conf error">'.$this->l('Access denied').'</div>';			
			}
			

		}

		if (Tools::isSubmit('clear_css_cache')) {	

			if($this->context->employee->id_profile == 1) {
				
				$e = array_map('unlink', glob(_PS_ALL_THEMES_DIR_.$this->context->shop->theme_name."/cache/*.css"));
				if (!empty($e)) {
					$msg .= "<div class=\"conf confirm\">Cache has been cleaned</div>";
				} else {
					$msg .= "<div class=\"conf error\">Unable to clean cache</div>";
				}

			} else {
				$msg .= '<div class="conf error">'.$this->l('Access denied').'</div>';			
			}

		}

		if (Tools::isSubmit('fixhook')) {	
			if (!Hook::getIdByName("comingsoon")) {
				if (!$this->registerHook('comingsoon')) {
					$msg .= '<div class="conf confirm">'.$this->l('Can\'t create hook').'</div>';	
				} else {
					$msg .= $this->InstallToComingSoon();					
				}
			} else {
				$msg .= $this->InstallToComingSoon();
			}
		}
		if (Tools::isSubmit('aupdate')) {			
			$this->importDefaultData(true);
		}

		if (Tools::isSubmit('sendnotification')) {
			$id_lang = $this->context->language->id;
			$id_shop = (int)$this->context->shop->id;

			if (!$fileHolder = @fopen(_PS_MODULE_DIR_.$this->name."/maintenance/emails.txt", 'r')) {
				$mes = "Cant open storage file";
			} else {	
				$filecontents = file_get_contents(_PS_MODULE_DIR_.$this->name."/maintenance/emails.txt");
				$emails = explode(";", $filecontents);
				foreach ($emails as $email) {
					if (Validate::isEmail($email))
						$filtered[] = $email;
				}
				$readyEmails = array_unique($filtered);
				fclose($fileHolder);
			}

			if (Configuration::get('PS_LOGO_MAIL') !== false && file_exists(_PS_IMG_DIR_.Configuration::get('PS_LOGO_MAIL', null, null, $id_shop)))
				$logo = _PS_IMG_DIR_.Configuration::get('PS_LOGO_MAIL', null, null, $id_shop);
			else {
				if (file_exists(_PS_IMG_DIR_.Configuration::get('PS_LOGO', null, null, $id_shop)))
					$logo = _PS_IMG_DIR_.Configuration::get('PS_LOGO', null, null, $id_shop);
				else
					$vars['{shop_logo}'] = '';
			}
			ShopUrl::cacheMainDomainForShop((int)$id_shop);
			/* don't attach the logo as */

			if (isset($logo))
				$vars['{shop_logo}'] = ImageManager::getMimeTypeByExtension($logo);
			$vars['{email_menu_item}'] = 'color:#ffffff; font-size:15px; line-height:46px; mso-line-height-rule:exactly; font-family: \'Times new roman\'; text-transform:uppercase; text-decoration:none;';
			$vars["{shop_name}"] = Configuration::get('PS_SHOP_NAME');
			$vars["{shop_url}"] = Context::getContext()->link->getPageLink('index', true, Context::getContext()->language->id);
			Mail::Send(
				(int)$id_lang,
				'opening',
				Mail::l('Shop Opening', (int)$id_lang),
				$vars,
				$readyEmails,
				null,
				null,
				null,
				null,
				null,
				_PS_MODULE_DIR_.$this->name."/mails/",
				false,
				(int)$id_shop
			);
			
		}
		return $output.$this->displayForm($msg);
		
	}
	private function InstallToComingSoon() {
		$mod_id = Module::getModuleIdByName($this->name);
		$shop_id = (int)$this->context->shop->id;
		$hook_id = Hook::getIdByName("comingsoon");
		$result = Db::getInstance()->insert('hook_module', array('id_module' => $mod_id, 'id_shop' => $shop_id, 'id_hook' => $hook_id, 'position' => "0"));
		if (!$result) {
			$msg = '<div class="conf error">'.$this->l('Installiation error').'</div>';						
		} else {
			$msg = '<div class="conf confirm">'.$this->l('Hook successfully created').'</div>';
		}
		return $msg;
	}

	private function emailFilesUpdate() {

		$err = false;
		$msg = "";
		$file_headers = @get_headers($source);

		if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
			$msg .= "<div class=\"conf error\">There is no file to update!</div>"; 
		} else {		
			$file = _PS_MODULE_DIR_.$this->name.'/mail.zip';			
			if (!@copy($this->mailArchive, $file)) {
				$msg .= "<div class=\"conf error\">Can't download the file</div>"; 
			} else {
				$err = true;
				if (!Tools::ZipTest($file)) {
					$msg .= "<div class=\"conf error\">Zip file seems to be broken</div>";
				} else {
					$err = true;
					$zip = new ZipArchive;
					$res = $zip->open($file);
					if ($res === TRUE) {
					  $zip->extractTo(_PS_OVERRIDE_DIR_.'/classes/');
					  $zip->close();
					  unlink($file);
					  $msg .= $this->clearCache(_PS_CACHE_DIR_.'class_index.php');
					} else {
					  $msg .= "<div class=\"conf error\">Unable to unzip Mail.php file to \"override\" folder</div>";
					}
				}
			}				

			if ($err == true)
				$msg .= "<div class=\"conf confirm\">Mail.php updated</div>";	
		}

		return $msg;

	}

	private function addImage($image, $name, $sid, $lid, $tab_num)
		{	
			$errors = "";
			
			if (isset($image[$name]) && isset($image[$name]['tmp_name']) && !empty($image[$name]['tmp_name']))
			{

				if ($error = ImageManager::validateUpload($image[$name], Tools::convertBytes(ini_get('upload_max_filesize')))) $errors = $error;

					if ($errors == "Image format not recognized, allowed formats are: .gif, .jpg, .png") {

						$errors = "Images extension wrong!";				

					} elseif ($dot_pos = strrpos($image[$name]['name'], '.')) {

						$imgname = $name;
						$ext = substr($image[$name]['name'], $dot_pos + 1);
						$newname = $name.'-'.(int)$this->context->shop->id;

						if (!move_uploaded_file($image[$name]['tmp_name'], _PS_MODULE_DIR_.$this->name.'/images/upload/'.$newname.'.'.$ext))
							$result["error"] .= $this->l('Error move uploaded file');
						else
							$imgname = $newname;

						$sql = array();
						$sql[] = 'UPDATE `'._DB_PREFIX_.'pk_theme_settings` SET value = "'.$imgname.'.'.$ext.'" WHERE name = "'.$name.'" AND id_shop = '.$sid.';';
						$this->runSql($sql);					

					}				

			} else {
				$errors = "No image to upload";
			}	

			$this->savePosition($tab_num);

			if ($errors) { 
				$errors = '<div class="conf error">'.$errors.'</div>'; 
			} else $errors = "";			
			$result["error"] = $errors;	

			return $result;

		}
	private function deleteImg($img, $name, $tab_num, $sid, $lid) {

		$string = file_get_contents($this->config);
		$json_arr = json_decode($string, true);
		$err = "";
		
		// Delete the image file
		if (file_exists(_PS_MODULE_DIR_.$this->name.'/images/upload/'.$img)) {
		
			unlink(_PS_MODULE_DIR_.$this->name.'/images/upload/'.$img);

			$sql = array();
			$sql[] = 'UPDATE `'._DB_PREFIX_.'pk_theme_settings` SET value = "'.$json_arr[$name].'" WHERE name = "'.$name.'" AND id_shop = '.$sid.';';
			$sql[] = 'UPDATE `'._DB_PREFIX_.'pk_theme_settings` SET value = "'.$tab_num.'" WHERE name = "tab_number" AND id_shop = '.$sid.';';
			$this->runSql($sql);
		} else {
			$err = "No image to delete";
		}
		return $err;

	}

	private function fontNameAdaptation($name) {
		return str_replace( ' ', '+', $name ); 
	}

	public function cssWriter() {
		
		if (!$fileHolder = @fopen($this->settingsFile, 'w')) {
			$this->errors .= "Can't open settings file!";
		} else {				
			if (fwrite($fileHolder, $this->cssGenerator()) === FALSE) {
				$this->errors .= "Can't write settings!";
			}
			fclose($fileHolder);
		}

	}

	public function cssGenerator() {			

		$id_shop = (int)$this->context->shop->id;
		$s = $this->getOptions();					
		$pattern = "";
		$bg = "background-color: ".$s['back_color'];		
		if ($s["back_image"] != "") {
			$pattern = "
			background-image: url(../images/upload/".$s["back_image"].") !important; 
			background-repeat:".$s['back_repeat']." !important; 
			background-position:".$s['back_position']." top !important;";
		}

		$css = "";
		/*$css .= $this->selectors["first_color"]." {
				color:".$s['first_color']."}\n";
		$css .= $this->selectors["first_color_back"]." {
				background-color:".$s['first_color']."}\n";
		$css .= $this->selectors["second_color"]." {
				color:".$s['second_color']."}\n";
		$css .= $this->selectors["second_color_back"]." {
				background-color:".$s['second_color']."}\n";
		$css .= $this->selectors["third_color"]." {
				color:".$s['third_color']."}\n";
		$css .= $this->selectors["third_color_back"]." {
				background-color:".$s['third_color']."}\n";*/
		$css .= $this->selectors["logo_container"].", ".$this->selectors["logo_container_admin"]. "{
				left:".$s['logo_left']."px; top:".$s['logo_top']."px; }\n";
		$css .= $this->selectors["logo"].", ".$this->selectors["logo_admin"] ." {
				font: normal ".$s['logo_size']."px/".$s['logo_lh']."px \"".$s['logo_font']."\";				
				color:".$s['logo_color']."\n}\n";
		$css .= $this->selectors["slogan"].", ".$this->selectors["slogan_admin"] ." {
				font-size:".$s['slogan_size']."px; 
				font-family:\"".$s['slogan_font']."\"; 
				color:".$s['slogan_color']."}\n";
		$css .= $this->selectors["buttons"]." {
				background-color: ".$s['buttons_color']."; 
				color: ".$s['buttons_text_color']."; 
				font-family: \"".$s['buttons_font']."\"}\n";
		$css .= $this->selectors["buttons_hover"]." {
				background-color: ".$s['buttons_hover_color']."; 
				color: ".$s['buttons_hover_text_color']."}\n";
		$css .= $this->selectors["headings"].", ".$this->selectors["headings_admin"]." {
				font-family: \"".$s['heading_font']."\"; 
				color: ".$s['headings_color']."}\n";
		$css .= $this->selectors["subheadings"].", ".$this->selectors["subheadings_admin"]." {
				font-family: \"".$s['subheading_font']."\"; 
				color: ".$s['subheadings_color']."}\n";
		$css .= $this->selectors["text"].", ".$this->selectors["text_admin"]." {
				font-family: \"".$s['text_font']."\"; 
				color: ".$s['text_color']."}\n";
		$css .= $this->selectors["links"].", ".$this->selectors["links_admin"]." {
					color: ".$s['links_color']."}\n"
				.$this->selectors["links_admin"].":hover{
					color: ".$s['hover_links_color']."}\n";
		/*		##### 		temporary styles 		####	*/
		$css .= $this->selectors["bg"]." {".$bg."}\n";
		$css .= $this->selectors["additionalBg"]." {".$pattern."}\n";

		$css .= $s['custom_css'];
		return $css;
	}

	public function maintenanceDate($s)	{  // get sizes

		$ready_date = "";		
		$el = array('min', 'hour', 'day', 'month', 'year');
		$monthes = array("0"=>"January","1"=>"February","2"=>"March","3"=>"April","4"=>"May","5"=>"June","6"=>"July","7"=>"August","8"=>"September","9"=>"October","10"=>"November","11"=>"December");

		foreach ($el as $key => $value) {
			$from = 0;
			$ready_date .= '<select name="ready_'.$value.'">';
			switch($value){
				case 'min':	
					$until = 59;
					$ready_date .= "<option disabled='disabled'>".$value."</option>";
					break;
				case 'hour':
					$until = 23;
					$ready_date .= "<option disabled='disabled'>".$value."</option>";
					break;
				case 'day':	
					$until = 31;
					$ready_date .= "<option disabled='disabled'>".$value."</option>";
					break;
				case 'month':
					$until = 11;
					$ready_date .= "<option disabled='disabled'>".$value."</option>";
					break;
				case 'year':
					$from = date("Y");$until = (date("Y")+1);
					$ready_date .= "<option disabled='disabled'>".$value."</option>";
					break;
				default:
					break;
			}						
			for ($time=$from; $time <= $until; $time++) {				
				if ($value == "month") $tm = $monthes[$time]; else $tm = $time;
				$ready_date .= '<option '.(($time == $s["ready_".$value.""]) ? 'selected' : '').' value="'.$time.'">'.$tm.'</option>';
			}					
			$ready_date .= '</select>';
		}					

		return $ready_date;

	}

	public function optionList($fontType)	{  // create a <option> list for <select>

		$fontsOption = "";
		foreach ($this->fontslist() as $key => $font) { // get fonts list
			if ($fontType == $font) {
				$selected = "selected";
			} else {
				$selected = "";
			}
			$fontsOption .= '<option '.$selected.' value="'.$font.'">'.$font.'</option>';					   
		}
		return $fontsOption;

	}

	public function getOptions()	{  // get options from database
		if (!$sett = Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'pk_theme_settings` WHERE id_shop = '.(int)$this->context->shop->id.';')) return false;
		
		foreach ($sett as $key => $item) {			
			foreach ($item as $k => $value) {				
				if ($k == "name") $n = $value;
				if ($k == "value") $v = $value;
				if (isset($v) && isset($n)) $s[$n] = $v;				
			}
		}	
		return $s;
	}

	public function getSizes($list, $type)	{  // get sizes

		$sizes = "";
		for ($i = 10; $i < $this->maxSize; $i++) {
			if ($list[$type] == $i) {
				$selected = "selected";
			} else {
				$selected = "";
			}
			$sizes .= '<option '.$selected.' value="'.$i.'">'.$i.'px</option>';	
		}
		return $sizes;

	}

	public function displayForm($message)
	{
		$s = $this->getOptions();
		$imgPath = $_SERVER['DOCUMENT_ROOT'].$this->_path.'/images/upload/';					

		$sett_str = $styles = "";
		for ($i = 0; $i < $this->patternsQuantity; $i++) { // get patterns

			if ($s["pattern"] == $i) {
				$checked = "checked=\"maxchecked\"";
				$ptClass = "selected";
			} else {
				$checked = "";
				$ptClass = "";
			}
			if ($i == 0) {
				$title = "title=\"No pattern\"";
			} else {$title = "";}
			$sett_str .= "<label ".$title." for=\"back_".$i."\" class=\"cell back_".$i." ".$ptClass."\" onclick=\"$('#ptrns label').removeClass('selected');$(this).addClass('selected');\">							
			<input type=\"radio\" id=\"back_".$i."\" name=\"pattern\" value=\"".$i."\" class=\"var\" $checked /></label>";
		}

		$fonts = $this->optionList($s["logo_font"]);
		$sl_fonts = $this->optionList($s["slogan_font"]);
		$h_fonts = $this->optionList($s["heading_font"]);
		$sh_fonts = $this->optionList($s["subheading_font"]);		
		$text_fonts = $this->optionList($s["text_font"]);
		$buttons_fonts = $this->optionList($s["buttons_font"]);

		$logo_sizes = $this->getSizes($s, "logo_size");
		$logo_lh = $this->getSizes($s, "logo_lh");
		$slogan_sizes = $this->getSizes($s, "slogan_size");
		$protocol = Tools::getShopProtocol();
		
		$link_params = "rel=\"stylesheet\" type=\"text/css\" href=\"".$protocol."fonts.googleapis.com/css?family=";
		$fontFiles = "";

		/*	####	if not default font, include it ####	*/
		if (!(in_array($s["logo_font"], $this->defaultFonts))) {
			$fontFiles .= "<link id=\"logofont_link\" ".$link_params.$this->fontNameAdaptation($s["logo_font"])."\">";
		}
		if (!(in_array($s["slogan_font"], $this->defaultFonts))) {
			$fontFiles .= "<link id=\"sloganfont_link\" ".$link_params.$this->fontNameAdaptation($s["slogan_font"])."\">";
		}
		if (!(in_array($s["heading_font"], $this->defaultFonts))) {
			$fontFiles .= "<link id=\"headingsfont_style\" ".$link_params.$this->fontNameAdaptation($s["heading_font"])."\">";
		}
		if (!(in_array($s["subheading_font"], $this->defaultFonts))) {
			$fontFiles .= "<link id=\"subheadingsfont_style\" ".$link_params.$this->fontNameAdaptation($s["subheading_font"])."\">";
		}
		if (!(in_array($s["text_font"], $this->defaultFonts))) {
			$fontFiles .= "<link id=\"textfont_style\" ".$link_params.$this->fontNameAdaptation($s["text_font"])."\">";
		}	
		if (!(in_array($s["buttons_font"], $this->defaultFonts))) {
			$fontFiles .= "<link id=\"buttonsfont_style\" ".$link_params.$this->fontNameAdaptation($s["buttons_font"])."\">";
		}

		$ready_date = $this->maintenanceDate($s); // get maintenance date		

		$tabs = "";
		foreach ($this->tabs as $num => $name) {
			$tabs .= '<div class="tab '.(($s["tab_number"] == ($num+1)) ? 'selected' : '').'" id="tab_menu_'.($num+1).'"><div class="link">'.$name.'</div><div class="arrow"></div></div>';
		}

		$options = $this->getCMSOptions(1, 1, (int)$s["maintanance_cms_page"]);
		return $fontFiles.'
		<style type="text/css">
			#tab_content_4 .button:hover {background:'.$s["buttons_hover_color"].'; color:'.$s["buttons_hover_text_color"].'}
			#tab_content_4 .button {background:'.$s["buttons_color"].'; color:'.$s["buttons_text_color"].'; font-family:'.$s["buttons_font"].'}
			#heading-example {font-family:'.$s["heading_font"].'; color: '.$s["headings_color"].'}
			#text-example {font-family:'.$s["text_font"].'; color: '.$s["text_color"].'}
			#link-example a { color: '.$s["links_color"].'}
			#link-example a:hover { color: '.$s["hover_links_color"].'}
		</style>
		<form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" enctype="multipart/form-data" class="list_options" id="themesettings ts-prefix" method="post">
			<fieldset>
				<div class="tabscontainer">
					<div class="heading">
						<div class="module-title">
						<img src="'.$this->_path.'logo.png" width="16" height="16" alt="" title="" />'.$this->l('Theme Settings').'
						</div>
						<div class="buttons_section">
							'.$message.'							
							<input type="submit" name="submitThemeSettings" value="'.$this->l('Apply Settings').'" class="button" />
						</div>
					</div>
					<div class="tabs">						
						'.$tabs.'
			        </div>			        
					<div class="curvedContainer">					
						<div class="tabcontent" id="tab_content_1" '.(($s["tab_number"] == 1) ? 'style="display:block"' : '').'>
						<input type="radio" class="hide" name="tab_number" id="tab_1" value="1" '.(($s["tab_number"] == 1) ? 'checked="checked"' : '').' />
						<div class="va-content">
							<!--<div class="margin form">
								<h4>'.$this->l('Settings Panel on the front page').'</h4>								
								<p class="variant switch">
								    <input type="radio" name="sett_panel" id="settpanel_on" value="1" '.(($s["sett_panel"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="sett_panel" id="settpanel_off" value="0" '.(($s["sett_panel"] == 0) ? 'checked ' : '').'/>
								    <label for="settpanel_on" class="cb-enable'.(($s["sett_panel"] == 1) ? ' sel' : '').'"><span>'.$this->l('Show').'</span></label>
								    <label for="settpanel_off" class="cb-disable'.(($s["sett_panel"] == 0) ? ' sel' : '').'"><span>'.$this->l('Hide').'</span></label>
								</p>								
							</div>-->
							<div class="margin form" id="responsive">
								<h4>'.$this->l('Responsive Mode').'</h4>
								<div class="variant tt-wrapper">								
									<label class="illustrated '.(($s["responsive"] == 1) ? 'selected' : '').'" for="responsive_on" onclick="$(\'#responsive label\').removeClass(\'selected\');$(this).addClass(\'selected\');" id="responsive_on_lbl">
										<img src="'.$this->_path.'images/backoffice/responsive_on.jpg" alt="" />
										<span class="tt">'.$this->l('Enable Responsive').'</span>
									</label>
									<input class="invisible" type="radio" id="responsive_on" name="responsive" value="1" '.(($s["responsive"] == 1) ? 'checked="checked" ' : '').'/>
									<label class="illustrated '.(($s["responsive"] == 0) ? 'selected' : '').'" for="responsive_off" onclick="$(\'#responsive label\').removeClass(\'selected\');$(this).addClass(\'selected\');" id="responsive_off_lbl">
											<img src="'.$this->_path.'images/backoffice/responsive_off.jpg" alt="" />				
											<span class="tt">'.$this->l('Disable Responsive').'</span>
									</label>
									<input class="invisible" type="radio" name="responsive" id="responsive_off" value="0" '.(($s["responsive"] == 0) ? 'checked="checked" ' : '').'/>
									<div class="clear"></div>
								</div>
							</div>
							<!--<div class="margin form" id="page-view">
								<h4>'.$this->l('Page width').'</h4>
								<div class="variant tt-wrapper">								
									<label class="illustrated '.(($s["widescreen"] == 1) ? 'selected' : '').'" for="widescreen_on" onclick="$(\'#page-view label\').removeClass(\'selected\');$(this).addClass(\'selected\');" id="widescreen_on_lbl">
										<img src="'.$this->_path.'/images/backoffice/widescreen_on.gif" alt="" />
										<span class="tt">'.$this->l('Full width').'</span>
									</label>
									<input class="invisible" type="radio" id="widescreen_on" name="widescreen" value="1" '.(($s["widescreen"] == 1) ? 'checked="checked" ' : '').'/>
									<label class="illustrated '.(($s["widescreen"] == 0) ? 'selected' : '').'" for="widescreen_off" onclick="$(\'#page-view label\').removeClass(\'selected\');$(this).addClass(\'selected\');" id="widescreen_off_lbl">
											<img src="'.$this->_path.'/images/backoffice/widescreen_off.gif" alt="" />
											<span class="tt">'.$this->l('Fixed width').'</span>
									</label>
									<input class="invisible" type="radio" name="widescreen" id="widescreen_off" value="0" '.(($s["widescreen"] == 0) ? 'checked="checked" ' : '').'/>
									<div class="clear"></div>
								</div>
							</div>		-->												
							<div class="margin form" id="page-layout">
								<h4>'.$this->l('Page Layout').'</h4>
								<div class="variant tt-wrapper">
									<label class="illustrated_medium '.(($s["column"] == "left") ? 'selected' : '').'" for="left_col" onclick="$(\'#page-layout label\').removeClass(\'selected\');$(this).addClass(\'selected\');">
										<img src="'.$this->_path.'/images/backoffice/col_left.gif" alt="" />
										<input class="invisible" type="radio" id="left_col" name="column" value="left" '.(($s["column"] == "left") ? 'checked="checked" ' : '').'/>
										<span class="tt">'.$this->l('Left Column + Center Column').'</span>
									</label>
									<label class="illustrated_medium '.(($s["column"] == "right") ? 'selected' : '').'" for="right_col" onclick="$(\'#page-layout label\').removeClass(\'selected\');$(this).addClass(\'selected\');">
										<img src="'.$this->_path.'/images/backoffice/col_right.gif" alt="" />
										<input class="invisible" type="radio" name="column" id="right_col" value="right" '.(($s["column"] == "right") ? 'checked="checked" ' : '').'/>
										<span class="tt">'.$this->l('Center Column + Right Column').'</span>
									</label>
									<!--<label class="illustrated_medium nomargin '.(($s["column"] == "nocol") ? 'selected' : '').'" for="nocol" onclick="$(\'#page-layout label\').removeClass(\'selected\');$(this).addClass(\'selected\');">
										<img src="'.$this->_path.'/images/backoffice/col_no.gif" alt="" />
										<input class="invisible" type="radio" name="column" id="nocol" value="nocol" '.(($s["column"] == "nocol") ? 'checked="checked" ' : '').'/>
										<span class="tt">'.$this->l('No columns').'</span>
									</label>-->
									<div class="clear"></div>
								</div>															
							</div>
							<div class="margin form">
								<h4>'.$this->l('"Sticky" menu').'</h4>
								<p class="variant switch">
								    <input type="radio" name="sticky_menu" id="scroll_sticky_menu_on" value="1" '.(($s["sticky_menu"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="sticky_menu" id="scroll_sticky_menu_off" value="0" '.(($s["sticky_menu"] == 0) ? 'checked ' : '').'/>
								    <label for="scroll_sticky_menu_on" class="cb-enable'.(($s["sticky_menu"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="scroll_sticky_menu_off" class="cb-disable'.(($s["sticky_menu"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								</p>							
							</div>
							<div class="margin form">
								<h4>'.$this->l('"Scroll to Top" Button').'</h4>
								<p class="variant switch">
								    <input type="radio" name="toTop" id="scroll_totop_on" value="1" '.(($s["toTop"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="toTop" id="scroll_totop_off" value="0" '.(($s["toTop"] == 0) ? 'checked ' : '').'/>
								    <label for="scroll_totop_on" class="cb-enable'.(($s["toTop"] == 1) ? ' sel' : '').'"><span>'.$this->l('Show').'</span></label>
								    <label for="scroll_totop_off" class="cb-disable'.(($s["toTop"] == 0) ? ' sel' : '').'"><span>'.$this->l('Hide').'</span></label>
								</p>							
							</div>
							<div class="margin form">
								<h4>'.$this->l('Stylized Tooltips').'</h4>
								<p class="variant switch">
								    <input type="radio" name="tooltips" id="tooltips_on" value="1" '.(($s["tooltips"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="tooltips" id="tooltips_off" value="0" '.(($s["tooltips"] == 0) ? 'checked ' : '').'/>
								    <label for="tooltips_on" class="cb-enable'.(($s["tooltips"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="tooltips_off" class="cb-disable'.(($s["tooltips"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								</p>								
							</div>
							<div class="margin form" id="coordinates">
								<h4>'.$this->l('Map on the Contact Page').'</h4>
								<p class="variant switch">
								    <input type="radio" name="show_map" id="show_map_on" value="1" '.(($s["show_map"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="show_map" id="show_map_off" value="0" '.(($s["show_map"] == 0) ? 'checked ' : '').'/>
								    <label for="show_map_on" class="cb-enable'.(($s["show_map"] == 1) ? ' sel' : '').'"><span>'.$this->l('Show').'</span></label>
								    <label for="show_map_off" class="cb-disable'.(($s["show_map"] == 0) ? ' sel' : '').'"><span>'.$this->l('Hide').'</span></label>
								</p>
								<div class="variant" id="coordinates-info">
									<label class="t textinput" for="location">'.$this->l('Your Location').':</label>
									<input type="text" size="30" name="location" id="location" value="'.$s["location"].'" /><br/>
									&nbsp;&nbsp;'.$this->l('Latitude').': <span class="lat">'.$s["location_lat"].'</span>;&nbsp;&nbsp;'.$this->l('Longtitude').': <span class="lon">'.$s["location_lng"].'</span>
									<input type="hidden" name="location_lng" id="location_lng" value="'.$s["location_lng"].'"/>
									<input type="hidden" name="location_lat" id="location_lat" value="'.$s["location_lat"].'"/>
									<br/>
								</div>								
							</div>
							<div class="margin form">
								<h4>'.$this->l('Text in the footer section').'</h4>
								<textarea id="footer_text" name="footer_text" cols="26" rows="3">'.$s["footer_text"].'</textarea>
								<p class="preference_description">* '.$this->l('You can use html tags in this field').'</p>
							</div>	
							<div class="margin form">
								<h4>'.$this->l('Alysum ThemeSettings Updater').'</h4>											
								<input class="button" type="submit" name="aupdate" value="'.$this->l('Update').'" />
								<p class="preference_description">'.$this->l('This function can be usefull after update Alysum themesettings module. It will activate new features.').'</p><br/>							
							</div>
						</div>
					</div>
					<div class="tabcontent" id="tab_content_2" '.(($s["tab_number"] == 2) ? 'style="display:block"' : '').'>
					<input type="radio" class="hide" name="tab_number" id="tab_2" value="2" '.(($s["tab_number"] == 2) ? 'checked="checked"' : '').' />
						<div class="va-content">
							<div class="margin form">
								<h4>'.$this->l('Background Color').'</h4>								
								<div class="variant">
									<input type="color" name="back_color" id="back_color" class="colorpicker" data-hex="true" value="'.$s["back_color"].'" />									
									<label class="t textinput" for="back_color"></label><br />
								</div>
								<div class="option-separator"></div>
								<h4>'.$this->l('Background Image').'</h4>
								<div class="variant">
									<div class="back_image_container">
										'.(($s["back_image"] != "") ? '<img class="back_image" src="'.$this->_path.'images/upload/'.$s["back_image"].'">' : 'No Image').'
									</div>									
									<input id="back_image" type="file" name="back_image">
									<input id="bimage" type="submit" class="button" name="back_image_upload">
									<input class="button'.(($s["back_image"] == "") ? ' hide' : '').'" type="submit" name="submitDeleteImgConf" value="'.$this->l('Delete image').'" />
								</div>								
								<div class="'.(($s["back_image"] == "") ? 'hide' : '').'">
								<div class="option-separator"></div>
									<h4>'.$this->l('Background Position').'</h4>
									<div class="labels-wrapper variant tt-wrapper" id="backpos">
										<label class="illustrated_medium '.(($s["back_position"] == "left") ? 'selected' : '').'" for="back_position_left" onclick="$(\'#backpos label\').removeClass(\'selected\');$(this).addClass(\'selected\');">
											<img src="'.$this->_path.'/images/backoffice/back_left.gif" alt="" />
											<input class="invisible" type="radio" name="back_position" id="back_position_left" value="left" '.(($s["back_position"] == "left") ? 'checked="checked" ' : '').'/>
											<span class="tt">'.$this->l('Left Top').'</span>
										</label>
										<label class="illustrated_medium '.(($s["back_position"] == "center") ? 'selected' : '').'" for="back_position_center" onclick="$(\'#backpos label\').removeClass(\'selected\');$(this).addClass(\'selected\');">
											<img src="'.$this->_path.'/images/backoffice/back_center.gif" alt="" />
											<input class="invisible" type="radio" name="back_position" id="back_position_center" value="center" '.(($s["back_position"] == "center") ? 'checked="checked" ' : '').'/>
											<span class="tt">'.$this->l('Center Top').'</span>
										</label>
										<label class="illustrated_medium '.(($s["back_position"] == "right") ? 'selected' : '').'" for="back_position_right" onclick="$(\'#backpos label\').removeClass(\'selected\');$(this).addClass(\'selected\');">
											<img src="'.$this->_path.'/images/backoffice/back_right.gif" alt="" />
											<input class="invisible" type="radio" name="back_position" id="back_position_right" value="right" '.(($s["back_position"] == "right") ? 'checked="checked" ' : '').'/>
											<span class="tt">'.$this->l('Right Top').'</span>
										</label>
										<div class="clear"></div>
									</div>
									<h4>'.$this->l('Background Repeat').'</h4>
									<div class="labels-wrapper variant tt-wrapper" id="backrepeat">
										<label class="illustrated_small '.(($s["back_repeat"] == "no-repeat") ? 'selected' : '').'" for="back_rpt_no" onclick="$(\'#backrepeat label\').removeClass(\'selected\');$(this).addClass(\'selected\');">
											<img src="'.$this->_path.'/images/backoffice/rpt-no.gif" alt="" />
											<input class="invisible" type="radio" name="back_repeat" id="back_rpt_no" value="no-repeat" '.(($s["back_repeat"] == "no-repeat") ? 'checked="checked" ' : '').'/>
											<span class="tt">'.$this->l('No Repeat').'</span>
										</label>
										<label class="illustrated_small '.(($s["back_repeat"] == "repeat-x") ? 'selected' : '').'" for="back_rpt_x" onclick="$(\'#backrepeat label\').removeClass(\'selected\');$(this).addClass(\'selected\');">
											<img src="'.$this->_path.'/images/backoffice/rpt_x.gif" alt="" />
											<input class="invisible" type="radio" name="back_repeat" id="back_rpt_x" value="repeat-x" '.(($s["back_repeat"] == "repeat-x") ? 'checked="checked" ' : '').'/>
											<span class="tt">'.$this->l('Repeat Horizontally').'</span>
										</label>
										<label class="illustrated_small '.(($s["back_repeat"] == "repeat-y") ? 'selected' : '').'" for="back_rpt_y" onclick="$(\'#backrepeat label\').removeClass(\'selected\');$(this).addClass(\'selected\');">
											<img src="'.$this->_path.'/images/backoffice/rpt_y.gif" alt="" />
											<input class="invisible" type="radio" name="back_repeat" id="back_rpt_y" value="repeat-y" '.(($s["back_repeat"] == "repeat-y") ? 'checked="checked" ' : '').'/>
											<span class="tt">'.$this->l('Repeat Vertically').'</span>
										</label>
										<label class="illustrated_small '.(($s["back_repeat"] == "repeat") ? 'selected' : '').'" for="back_rpt" onclick="$(\'#backrepeat label\').removeClass(\'selected\');$(this).addClass(\'selected\');">
											<img src="'.$this->_path.'/images/backoffice/rpt.gif" alt="" />
											<input class="invisible" type="radio" name="back_repeat" id="back_rpt" value="repeat" '.(($s["back_repeat"] == "repeat") ? 'checked="checked" ' : '').'/>
											<span class="tt">'.$this->l('Repeat All').'</span>
										</label>									
										<div class="clear"></div>
									</div>
									<div class="info">'.$this->l('If you want to use predefined pattern, please remove background image').'</div>
								</div>
								<br/>
								<div class="'.(($s["back_image"] != "") ? 'hide' : '').'">
									<div class="option-separator"></div>
									<h4>'.$this->l('Patterns').'</h4>
									<div class="sect labels-wrapper" id="ptrns">
										'.( $sett_str ).'
										<div class="clear"></div>
									</div>	
								</div>
							</div>
						</div>
					</div>
					<div class="tabcontent" id="tab_content_3" '.(($s["tab_number"] == 3) ? 'style="display:block"' : '').'>
						<input type="radio" class="hide" name="tab_number" id="tab_3" value="3" '.(($s["tab_number"] == 3) ? 'checked="checked"' : '').' />
						<div class="va-content">
							<div class="margin form">
								<h4>'.$this->l('Logo type').'</h4>		
								<p class="variant switch">
								    <input type="radio" name="logo_type" id="logo_type_image" value="0" '.(($s["logo_type"] == 0) ? 'checked ' : '').'/>
								    <input type="radio" name="logo_type" id="logo_type_text" value="1" '.(($s["logo_type"] == 1) ? 'checked ' : '').'/>
								    <label for="logo_type_image" class="cb-enable'.(($s["logo_type"] == 0) ? ' sel' : '').'"><span>'.$this->l('Image').'</span></label>
								    <label for="logo_type_text" class="cb-disable'.(($s["logo_type"] == 1) ? ' sel' : '').'"><span>'.$this->l('Text').'</span></label>
								</p>
								<div class="option-separator"></div>
								<div id="logotype-text" '.(($s["logo_type"] == 0) ? 'class="hide" ' : '').'>
									<h4>'.$this->l('Logo options').'</h4>
									<div class="variant">
										<input type="text" size="20" name="logo_text" id="logo_text" value="'.$s["logo_text"].'" />
										<label class="t textinput" for="logo_text">'.$this->l('Shop Name').'</label>
									</div>
									<div class="variant">
										<input type="color" name="logo_color" id="logo_color" class="colorpicker" data-hex="true" value="'.$s["logo_color"].'" />
									</div>
									<label class="t" for="logo_font">'.$this->l('Font').':</label>
									<select name="logo_font" id="logofont">'.( $fonts ).'</select>
									<label class="t" for="logo_size">'.$this->l('Size').':</label>
									<select name="logo_size" id="logosize">'.( $logo_sizes ).'</select>			
									<label class="t" for="logo_lh">'.$this->l('Line-height').':</label>
									<select name="logo_lh" id="logolh">'.( $logo_lh ).'</select>			
									<div class="option-separator"></div>									
									<h4>'.$this->l('Slogan options').'</h4>
									<div class="variant">
										<input type="text" size="20" name="slogan" id="slogan" value="'.$s["slogan"].'" />
										<label class="t textinput" for="slogan">'.$this->l('Shop Slogan').'</label>
										<br/>
									</div>
									<div class="variant">
										<input type="color" name="slogan_color" id="slogan_color" class="colorpicker" data-hex="true" value="'.$s["slogan_color"].'" />
									</div>
									<label class="t" for="slogan_font">'.$this->l('Font').':</label>
									<select name="slogan_font" id="sloganfont">'.( $sl_fonts ).'</select>
									<label class="t" for="slogan_size">'.$this->l('Size').':</label>
									<select name="slogan_size" id="slogansize">'.( $slogan_sizes ).'</select>
									<div class="option-separator"></div>	
									<h4>'.$this->l('Logo position').'</h4>
									<div class="position_buttons">
										<div class="logo_top"></div>
										<div class="logo_left"></div>
										<div class="logo_right"></div>
										<div class="logo_bottom"></div>
										<input type="hidden" name="logo_top" id="logo_top" value="'.$s["logo_top"].'">
										<input type="hidden" name="logo_left" id="logo_left" value="'.$s["logo_left"].'">
										<span>logo</span>
									</div>	
									<div class="option-separator"></div>	
									<div class="font-sample">
										<div class="font-wrapper">
											<div class="logo-container" style="top:'.$s["logo_top"].'px; left:'.$s["logo_left"].'px;">
												<span id="logofont_example" style="font-size:'.$s["logo_size"].'px; line-height:'.$s["logo_lh"].'px; font-family:'.$s["logo_font"].'; color:'.$s["logo_color"].'">'.$s["logo_text"].'</span>
												<span id="sloganfont_example" style="font-size:'.$s["slogan_size"].'px; line-height:18px; font-family:'.$s["slogan_font"].'; color:'.$s["slogan_color"].'">'.$s["slogan"].'</span>
											</div>
										</div>
									</div>
								</div>								
								<div id="logotype-image" '.(($s["logo_type"] == 1) ? 'class="hide" ' : '').'>	
									Upoad your logo <a href="index.php?controller=AdminThemes&token='.Tools::getAdminTokenLite('AdminThemes').'#conf_id_PS_LOGO" target="_blank">here:</a>					
								</div>	
							</div>
						</div>
					</div>
					<div class="tabcontent" id="tab_content_4" '.(($s["tab_number"] == 4) ? 'style="display:block"' : '').'>
					<input type="radio" class="hide" name="tab_number" id="tab_4" value="4" '.(($s["tab_number"] == 4) ? 'checked="checked"' : '').' />
						<div class="va-content">
							<div class="margin form">
								<h4>'.$this->l('Buttons Colors').'</h4>
								<div class="show_area" id="page">										
									<a class="button">'.$this->l('Example').'</a>
								</div>								
								<div class="sect funct_area">
									<div class="variant">										
										<input type="color" name="buttons_color" class="colorpicker" data-hex="true" id="buttons_color" value="'.$s["buttons_color"].'" />
										<label class="t textinput" for="buttons_color">'.$this->l('Normal').'</label>
									</div>
									<div class="variant">										
										<input type="color" name="buttons_text_color" class="colorpicker" data-hex="true" id="buttons_text_color" value="'.$s["buttons_text_color"].'" />
										<label class="t textinput" for="buttons_text_color">'.$this->l('Normal text').'</label>
									</div>
									<div class="variant">										
									<input type="color" name="buttons_hover_color" class="colorpicker" data-hex="true" id="buttons_hover_color" value="'.$s["buttons_hover_color"].'" />
									<label class="t textinput" for="buttons_hover_color">'.$this->l('Hover').'</label>
									</div>
									<div class="variant">										
										<input type="color" name="buttons_hover_text_color" class="colorpicker" data-hex="true" id="buttons_hover_text_color" value="'.$s["buttons_hover_text_color"].'" />
										<label class="t textinput" for="buttons_hover_text_color">'.$this->l('Hover text').'</label><br>
									</div>
								</div>	
								<div class="option-separator"></div>
								<h4>'.$this->l('Buttons Font').'</h4>
								<select name="buttons_font" id="buttons_font">'.( $buttons_fonts ).'</select>
							</div>
						</div>
					</div>
					<div class="tabcontent" id="tab_content_5" '.(($s["tab_number"] == 5) ? 'style="display:block"' : '').'>
					<input type="radio" class="hide" name="tab_number" id="tab_5" value="5" '.(($s["tab_number"] == 5) ? 'checked="checked"' : '').' />
						<div class="va-content">
							<div class="margin form">
								<h4>'.$this->l('Headings').'</h4>
								<div class="variant">
									<div class="show_area" id="heading-example"><h5>Heading example</h5></div>
									<div class="funct_area">		
										<label class="t" for="heading_font">Font name:</label>	
										<select name="heading_font" id="heading_font">'.( $h_fonts ).'</select>						
										<input type="color" name="headings_color" class="colorpicker" data-hex="true" id="headings_color" value="'.$s["headings_color"].'" />
									</div>
								</div>
								<div class="option-separator"></div>
								<!--<h4>'.$this->l('SubHeadings').'</h4>
								<div class="variant">
									<div class="show_area" id="subheading-example"><h6>Subheading example</h6></div>
									<div class="funct_area">		
										<label class="t" for="subheading_font">Font name:</label>	
										<select name="subheading_font" id="subheading_font">'.( $sh_fonts ).'</select>
										<input type="color" name="subheadings_color" class="colorpicker" data-hex="true" id="subheadings_color" value="'.$s["subheadings_color"].'" />
									</div>
								</div>
								<div class="option-separator"></div>-->
								<h4>'.$this->l('Text').'</h4>
								<div class="variant">
									<div class="show_area" id="text-example">Lorem ipsum dolor sit amet</div>
									<div class="funct_area">		
										<label class="t" for="text_font">'.$this->l('Font name').':</label>
										<select name="text_font" id="text_font">'.( $text_fonts ).'</select>						
										<input type="color" name="text_color" class="colorpicker" data-hex="true" id="text_color" value="'.$s["text_color"].'" />
									</div>
								</div>
								<div class="option-separator"></div>
								<h4>'.$this->l('Links').'</h4>
								<div class="variant">
									<div class="show_area" id="link-example"><a>'.$this->l('Just a link').'</a></div>
									<div class="funct_area">
										<div class="color_holder">											
											<div>'.$this->l('Normal').'</div>
											<input type="color" name="links_color" class="colorpicker" data-hex="true" id="links_color" value="'.$s["links_color"].'" />
										</div>
										<div class="color_holder">											
											<div>'.$this->l('Hover').'</div>
											<input type="color" name="hover_links_color" class="colorpicker" data-hex="true" id="hover_links_color" value="'.$s["hover_links_color"].'" />
										</div>
									</div>
								</div>
								<!--<div class="option-separator"></div>
								<h4>'.$this->l('Theme Colors').':</h4>
								<div class="variant">
									<div class="funct_area">
										<div class="color_holder">											
											<div>'.$this->l('Color 1').'</div>
											<input type="color" name="first_color" class="colorpicker" data-hex="true" id="first_color" value="'.$s["first_color"].'" />
										</div>
										<div class="color_holder">											
											<div>'.$this->l('Color 2').'</div>
											<input type="color" name="second_color" class="colorpicker" data-hex="true" id="second_color" value="'.$s["second_color"].'" />
										</div>
										<div class="color_holder">											
											<div>'.$this->l('Color 3').'</div>
											<input type="color" name="third_color" class="colorpicker" data-hex="true" id="third_color" value="'.$s["third_color"].'" />
										</div>
									</div>
								</div>-->
								<div class="clear"></div>
							</div>
						</div>
					</div>					
					<div class="tabcontent'.((($s["column"] != "nocol") && (($s["homepage_column"] == 1) ? ' sel' : '')) ? ' columns' : '').(($s["column"] == "right") ? ' right_column' : '').(($s["column"] == "left") ? ' left_column' : '').'" id="tab_content_6" '.(($s["tab_number"] == 6) ? 'style="display:block"' : '').'>
					<input type="radio" class="hide" name="tab_number" id="tab_6" value="6" '.(($s["tab_number"] == 6) ? 'checked="checked"' : '').' />
						<div class="va-content site-structure">
							<div class="margin form homepage_column'.(($s["column"] == "nocol") ? ' hide' : '').'">
								<h4>'.$this->l('Homepage Column').'</h4>
								<p class="variant switch">
								    <input type="radio" name="homepage_column" id="homepage_column_on" value="1" '.(($s["homepage_column"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="homepage_column" id="homepage_column_off" value="0" '.(($s["homepage_column"] == 0) ? 'checked ' : '').'/>
								    <label for="homepage_column_on" class="cb-enable'.(($s["homepage_column"] == 1) ? ' sel' : '').'"><span>'.$this->l('Show').'</span></label>
								    <label for="homepage_column_off" class="cb-disable'.(($s["homepage_column"] == 0) ? ' sel' : '').'"><span>'.$this->l('Hide').'</span></label>
								</p>								
							</div>
							<div class="section-header">
							</div>
							<div class="section-promo module-wrapper'.(($s["promo"] == 0) ? ' hidden-module' : '').'">
								<div class="module-bg">								
								<p class="variant switch">
								    <input type="radio" name="promo" id="promoon" value="1" '.(($s["promo"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="promo" id="promooff" value="0" '.(($s["promo"] == 0) ? 'checked ' : '').'/>
								    <label for="promoon" class="cb-enable'.(($s["promo"] == 1) ? ' sel' : '').'"><span>'.$this->l('Show').'</span></label>
								    <label for="promooff" class="cb-disable'.(($s["promo"] == 0) ? ' sel' : '').'"><span>'.$this->l('Hide').'</span></label>
								    <a href="index.php?controller=adminmodules&configure=promominicslider&token='.Tools::getAdminTokenLite("AdminModules").'" title="Module configure" target="_blank" class="module-configure">
								    </a>
								    <span class="module-name">'.$this->l('PromoMinic Slider').'</span>
								</p>
								</div>
							</div>
							<div class="section-sequence module-wrapper'.(($s["sequence"] == 0) ? ' hidden-module' : '').'">
								<div class="module-bg">								
								<p class="variant switch">
								    <input type="radio" name="sequence" id="sequenceon" value="1" '.(($s["sequence"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="sequence" id="sequenceoff" value="0" '.(($s["sequence"] == 0) ? 'checked ' : '').'/>
								    <label for="sequenceon" class="cb-enable'.(($s["sequence"] == 1) ? ' sel' : '').'"><span>'.$this->l('Show').'</span></label>
								    <label for="sequenceoff" class="cb-disable'.(($s["sequence"] == 0) ? ' sel' : '').'"><span>'.$this->l('Hide').'</span></label>
								    <a href="index.php?controller=adminmodules&configure=pk_sequenceslider&token='.Tools::getAdminTokenLite("AdminModules").'" title="Module configure" target="_blank" class="module-configure">
								    </a>
								    <span class="module-name">'.$this->l('Sequence Slider').'</span>
								</p>
								</div>
							</div>
							<div class="section-slider-01 module-wrapper'.(($s["slider01"] == 0) ? ' hidden-module' : '').'">
								<div class="module-bg">								
								<p class="variant switch">
								    <input type="radio" name="slider01" id="slider01on" value="1" '.(($s["slider01"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="slider01" id="slider01off" value="0" '.(($s["slider01"] == 0) ? 'checked ' : '').'/>
								    <label for="slider01on" class="cb-enable'.(($s["slider01"] == 1) ? ' sel' : '').'"><span>'.$this->l('Show').'</span></label>
								    <label for="slider01off" class="cb-disable'.(($s["slider01"] == 0) ? ' sel' : '').'"><span>'.$this->l('Hide').'</span></label>
								    <a href="index.php?controller=adminmodules&configure=pk_awShowcaseSlider&token='.Tools::getAdminTokenLite("AdminModules").'" title="Module configure" target="_blank" class="module-configure">
								    </a>
								    <span class="module-name">'.$this->l('awShowcase Slider').'</span>
								</p>
								</div>
							</div>							
							<div class="content-section">
								<div class="center_column">									
									<div class="section-product_carousel module-wrapper'.(($s["product_carousel"] == 0) ? ' hidden-module' : '').'">
										<div class="module-bg">								
										<p class="variant switch">
										    <input type="radio" name="product_carousel" id="product_carouselon" value="1" '.(($s["product_carousel"] == 1) ? 'checked ' : '').'/>
										    <input type="radio" name="product_carousel" id="product_carouseloff" value="0" '.(($s["product_carousel"] == 0) ? 'checked ' : '').'/>
										    <label for="product_carouselon" class="cb-enable'.(($s["product_carousel"] == 1) ? ' sel' : '').'"><span>'.$this->l('Show').'</span></label>
										    <label for="product_carouseloff" class="cb-disable'.(($s["product_carousel"] == 0) ? ' sel' : '').'"><span>'.$this->l('Hide').'</span></label>
										    <a href="index.php?controller=adminmodules&configure=pk_productsCarousel&token='.Tools::getAdminTokenLite("AdminModules").'" title="Module configure" target="_blank" class="module-configure">
										    </a>
										    <span class="module-name">'.$this->l('Products Carousel').'</span>
										</p>
										</div>
									</div>									
									<div class="section-isotope module-wrapper'.(($s["isotope"] == 0) ? ' hidden-module' : '').'">
										<div class="module-bg">								
										<p class="variant switch">
										    <input type="radio" name="isotope" id="isotopeon" value="1" '.(($s["isotope"] == 1) ? 'checked ' : '').'/>
										    <input type="radio" name="isotope" id="isotopeoff" value="0" '.(($s["isotope"] == 0) ? 'checked ' : '').'/>
										    <label for="isotopeon" class="cb-enable'.(($s["isotope"] == 1) ? ' sel' : '').'"><span>'.$this->l('Show').'</span></label>
										    <label for="isotopeoff" class="cb-disable'.(($s["isotope"] == 0) ? ' sel' : '').'"><span>'.$this->l('Hide').'</span></label>
										    <a href="index.php?controller=adminmodules&configure=pk_isotopeSort&token='.Tools::getAdminTokenLite("AdminModules").'" title="Module configure" target="_blank" class="module-configure">
										    </a>
										    <span class="module-name">'.$this->l('Isotope Sort').'</span>
										</p>
										</div>
									</div>
									<div class="section-newsletter module-wrapper'.(($s["newsletter"] == 0) ? ' hidden-module' : '').'">
										<div class="module-bg">
										<p class="variant switch">
										    <input type="radio" name="newsletter" id="newsletteron" value="1" '.(($s["newsletter"] == 1) ? 'checked ' : '').'/>
										    <input type="radio" name="newsletter" id="newsletteroff" value="0" '.(($s["newsletter"] == 0) ? 'checked ' : '').'/>
										    <label for="newsletteron" class="cb-enable'.(($s["newsletter"] == 1) ? ' sel' : '').'"><span>'.$this->l('Show').'</span></label>
										    <label for="newsletteroff" class="cb-disable'.(($s["newsletter"] == 0) ? ' sel' : '').'"><span>'.$this->l('Hide').'</span></label>
										    <a href="index.php?controller=adminmodules&configure=pk_bannercarousel&token='.Tools::getAdminTokenLite("AdminModules").'" title="Module configure" target="_blank" class="module-configure">
										    </a>
										    <span class="module-name">'.$this->l('Newsletter').'</span>
										</p>
										</div>
									</div>
									<div class="section-blog module-wrapper'.(($s["blog"] == 0) ? ' hidden-module' : '').'">
										<div class="module-bg">								
										<p class="variant switch">
										    <input type="radio" name="blog" id="blogon" value="1" '.(($s["blog"] == 1) ? 'checked ' : '').'/>
										    <input type="radio" name="blog" id="blogoff" value="0" '.(($s["blog"] == 0) ? 'checked ' : '').'/>
										    <label for="blogon" class="cb-enable'.(($s["blog"] == 1) ? ' sel' : '').'"><span>'.$this->l('Show').'</span></label>
										    <label for="blogoff" class="cb-disable'.(($s["blog"] == 0) ? ' sel' : '').'"><span>'.$this->l('Hide').'</span></label>
										    <a href="index.php?controller=adminmodules&configure=pk_blockblog&token='.Tools::getAdminTokenLite("AdminModules").'" title="Module configure" target="_blank" class="module-configure">
										    </a>
										    <span class="module-name">'.$this->l('Blog').'</span>
										</p>
										</div>
									</div>
									<div class="section-manufacturers_carousel module-wrapper'.(($s["manufacturers_carousel"] == 0) ? ' hidden-module' : '').'">
										<div class="module-bg">								
										<p class="variant switch">
										    <input type="radio" name="manufacturers_carousel" id="manufacturers_carouselon" value="1" '.(($s["manufacturers_carousel"] == 1) ? 'checked ' : '').'/>
										    <input type="radio" name="manufacturers_carousel" id="manufacturers_carouseloff" value="0" '.(($s["manufacturers_carousel"] == 0) ? 'checked ' : '').'/>
										    <label for="manufacturers_carouselon" class="cb-enable'.(($s["manufacturers_carousel"] == 1) ? ' sel' : '').'"><span>'.$this->l('Show').'</span></label>
										    <label for="manufacturers_carouseloff" class="cb-disable'.(($s["manufacturers_carousel"] == 0) ? ' sel' : '').'"><span>'.$this->l('Hide').'</span></label>
										    <a href="index.php?controller=adminmodules&configure=pk_manufacturersCarousel&token='.Tools::getAdminTokenLite("AdminModules").'" title="Module configure" target="_blank" class="module-configure">
										    </a>
										    <span class="module-name">'.$this->l('Manufacturers Carousel').'</span>
										</p>
										</div>
									</div>
								</div>
								<div class="site-column">'.((($s["column"] == "left") || ($s["column"] == "right")) ? 'Column' : '').'</div>
							</div>
							<div class="section-footer module-wrapper'.(($s["footer"] == 0) ? ' hidden-module' : '').'">
								<div class="module-bg">								
								<p class="variant switch">
								    <input type="radio" name="footer" id="footeron" value="1" '.(($s["footer"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="footer" id="footeroff" value="0" '.(($s["footer"] == 0) ? 'checked ' : '').'/>
								    <label for="footeron" class="cb-enable'.(($s["footer"] == 1) ? ' sel' : '').'"><span>'.$this->l('Show').'</span></label>
								    <label for="footeroff" class="cb-disable'.(($s["footer"] == 0) ? ' sel' : '').'"><span>'.$this->l('Hide').'</span></label>
								    <span class="module-name">'.$this->l('Footer Section').'</span>
								</p>
								</div>
							</div>
							<div class="section-footer_bottom module-wrapper'.(($s["footer_bottom"] == 0) ? ' hidden-module' : '').'">
								<div class="module-bg">								
								<p class="variant switch">
								    <input type="radio" name="footer_bottom" id="footer_bottomon" value="1" '.(($s["footer_bottom"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="footer_bottom" id="footer_bottomoff" value="0" '.(($s["footer_bottom"] == 0) ? 'checked ' : '').'/>
								    <label for="footer_bottomon" class="cb-enable'.(($s["footer_bottom"] == 1) ? ' sel' : '').'"><span>'.$this->l('Show').'</span></label>
								    <label for="footer_bottomoff" class="cb-disable'.(($s["footer_bottom"] == 0) ? ' sel' : '').'"><span>'.$this->l('Hide').'</span></label>
								    <span class="module-name">'.$this->l('Footer Bottom').'</span>
								</p>
								</div>
							</div>
						</div>
					</div>
					<div class="tabcontent" id="tab_content_7" '.(($s["tab_number"] == 7) ? 'style="display:block"' : '').'>		
					<input type="radio" class="hide" name="tab_number" id="tab_7" value="7" '.(($s["tab_number"] == 7) ? 'checked="checked"' : '').' />
						<div class="va-content tt-wrapper">
							<div class="margin form" id="product-listing">
								<h4>'.$this->l('Listing View').'</h4>								
								<label class="illustrated '.(($s["view"] == 1) ? 'selected' : '').'" for="view_list" onclick="$(\'#product-listing label\').removeClass(\'selected\');$(this).addClass(\'selected\');" id="view_list_lbl">
									<img src="'.$this->_path.'/images/backoffice/view_list.gif" alt="" />
									<span class="tt">'.$this->l('List View').'</span>
								</label>
								<input class="invisible" type="radio" id="view_list" name="view" value="1" '.(($s["view"] == 1) ? 'checked="checked" ' : '').'/>
								<label class="illustrated '.(($s["view"] == 0) ? 'selected' : '').'" for="view_grid" onclick="$(\'#product-listing label\').removeClass(\'selected\');$(this).addClass(\'selected\');" id="view_grid_lbl">
										<img src="'.$this->_path.'/images/backoffice/view_grid.gif" alt="" />
										<span class="tt">'.$this->l('Grid View').'</span>
								</label>
								<input class="invisible" type="radio" name="view" id="view_grid" value="0" '.(($s["view"] == 0) ? 'checked="checked" ' : '').'/>
								<div class="clear"></div>
								<p class="preference_description">'.$this->l('Show the product listing as a grid or a list view').'</p>
							</div>
							<!--<div class="margin form '.(($s["view"] == 1) ? 'hide' : '').'" id="images">
							<h4>'.$this->l('Product Image Size').'</h4>								
								<label class="illustrated '.(($s["productImageSize"] == 1) ? 'selected' : '').'" for="images_big" onclick="$(\'#images label\').removeClass(\'selected\');$(this).addClass(\'selected\');">
									<img src="'.$this->_path.'/images/backoffice/image_big.gif" alt="" />
									<span class="tt">'.$this->l('Big Product Images / 3 per row').'</span>
								</label>
								<input class="invisible" type="radio" id="images_big" name="productImageSize" value="1" '.(($s["productImageSize"] == 1) ? 'checked="checked" ' : '').'/>
								<label class="illustrated '.(($s["productImageSize"] == 0) ? 'selected' : '').'" for="images_small" onclick="$(\'#images label\').removeClass(\'selected\');$(this).addClass(\'selected\');">
										<img src="'.$this->_path.'/images/backoffice/image_small.gif" alt="" />
										<span class="tt">'.$this->l('Small Product Images / 4 per row').'</span>
								</label>
								<input class="invisible" type="radio" name="productImageSize" id="images_small" value="0" '.(($s["productImageSize"] == 0) ? 'checked="checked" ' : '').'/>
								<div class="clear"></div>
								<p class="preference_description">'.$this->l('Choose between 3 or 4 products per row').'</p>
							</div>-->
							<div class="margin form">
								<h4>'.$this->l('Category title and description on the category page').'</h4>
								<p class="variant switch">
								    <input type="radio" name="cat_title" id="cat_title_on" value="1" '.(($s["cat_title"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="cat_title" id="cat_title_off" value="0" '.(($s["cat_title"] == 0) ? 'checked ' : '').'/>
								    <label for="cat_title_on" class="cb-enable'.(($s["cat_title"] == 1) ? ' sel' : '').'"><span>'.$this->l('Show').'</span></label>
								    <label for="cat_title_off" class="cb-disable'.(($s["cat_title"] == 0) ? ' sel' : '').'"><span>'.$this->l('Hide').'</span></label>
								</p>
							</div>							
						</div>
						<div class="va-content tt-wrapper">
							<div class="margin form" id="subcategories">
								<h4>'.$this->l('Show subcategories').'</h4>								
								<p class="variant switch">
								    <input type="radio" name="subcategories" id="show_subcat" value="1" '.(($s["subcategories"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="subcategories" id="hide_subcat" value="0" '.(($s["subcategories"] == 0) ? 'checked ' : '').'/>
								    <label for="show_subcat" class="cb-enable'.(($s["subcategories"] == 1) ? ' sel' : '').'"><span>'.$this->l('Show').'</span></label>
								    <label for="hide_subcat" class="cb-disable'.(($s["subcategories"] == 0) ? ' sel' : '').'"><span>'.$this->l('Hide').'</span></label>
								</p>
							</div>
							<div class="margin form" id="lc_buttons">
								<h4>'.$this->l('Show layout control buttons (list/grid, big/small)').'</h4>
								<p class="variant switch">
								    <input type="radio" name="lc_buttons" id="show_lc_buttons" value="1" '.(($s["lc_buttons"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="lc_buttons" id="hide_lc_buttons" value="0" '.(($s["lc_buttons"] == 0) ? 'checked ' : '').'/>
								    <label for="show_lc_buttons" class="cb-enable'.(($s["lc_buttons"] == 1) ? ' sel' : '').'"><span>'.$this->l('Show').'</span></label>
								    <label for="hide_lc_buttons" class="cb-disable'.(($s["lc_buttons"] == 0) ? ' sel' : '').'"><span>'.$this->l('Hide').'</span></label>
								</p>
							</div>
							<div class="margin form" id="availability_label">
								<h4>'.$this->l('"Availability" label').'</h4>								
								<p class="variant switch">
								    <input type="radio" name="availability_label" id="availability_label_on" value="1" '.(($s["availability_label"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="availability_label" id="availability_label_off" value="0" '.(($s["availability_label"] == 0) ? 'checked ' : '').'/>
								    <label for="availability_label_on" class="cb-enable'.(($s["availability_label"] == 1) ? ' sel' : '').'"><span>'.$this->l('Show').'</span></label>
								    <label for="availability_label_off" class="cb-disable'.(($s["availability_label"] == 0) ? ' sel' : '').'"><span>'.$this->l('Hide').'</span></label>
								</p>
							</div>
							<div class="margin form" id="reduced_label">
								<h4>'.$this->l('"Reduced Price" label').'</h4>								
								<p class="variant switch">
								    <input type="radio" name="reduced_label" id="reduced_label_on" value="1" '.(($s["reduced_label"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="reduced_label" id="reduced_label_off" value="0" '.(($s["reduced_label"] == 0) ? 'checked ' : '').'/>
								    <label for="reduced_label_on" class="cb-enable'.(($s["reduced_label"] == 1) ? ' sel' : '').'"><span>'.$this->l('Show').'</span></label>
								    <label for="reduced_label_off" class="cb-disable'.(($s["reduced_label"] == 0) ? ' sel' : '').'"><span>'.$this->l('Hide').'</span></label>
								</p>
							</div>
						</div>
					</div>		
					<div class="tabcontent" id="tab_content_8" '.(($s["tab_number"] == 8) ? 'style="display:block"' : '').'>		
					<input type="radio" class="hide" name="tab_number" id="tab_8" value="8" '.(($s["tab_number"] == 8) ? 'checked="checked"' : '').' />
						<!--<div class="va-content tt-wrapper">
							<div class="margin form" id="product-appearance">
							<h4>'.$this->l('Product Image Appearance').'</h4>								
								<label class="illustrated '.(($s["product_page"] == 1) ? 'selected' : '').'" for="view_serialscroll" onclick="$(\'#product-appearance label\').removeClass(\'selected\');$(this).addClass(\'selected\');" id="view_serialscroll_lbl">
									<img src="'.$this->_path.'/images/backoffice/view_serialscroll.gif" alt="" />
									<span class="tt">'.$this->l('SerialScroll carousel').'</span>
								</label>
								<input class="invisible" type="radio" id="view_serialscroll" name="product_page" value="1" '.(($s["product_page"] == 1) ? 'checked="checked" ' : '').'/>
								<label class="illustrated '.(($s["product_page"] == 0) ? 'selected' : '').'" for="view_feat_carousel" onclick="$(\'#product-appearance label\').removeClass(\'selected\');$(this).addClass(\'selected\');" id="view_grid_lbl">
										<img src="'.$this->_path.'/images/backoffice/view_feat_carousel.gif" alt="" />
										<span class="tt">'.$this->l('Feature Carousel').'</span>
								</label>
								<input class="invisible" type="radio" name="product_page" id="view_feat_carousel" value="0" '.(($s["product_page"] == 0) ? 'checked="checked" ' : '').'/>
								<div class="clear"></div>
								<p class="preference_description">'.$this->l('Show product images as "Feature Carousel" or "serialScroll carousel"').'</p>
							</div>
						</div>-->
						<div class="margin form" id="product_video">
						<h4>'.$this->l('Product Video').'</h4>
							<div class="va-content tt-wrapper">
							<p class="variant switch">
							    <input type="radio" name="product_video" id="show_product_video" value="1" '.(($s["product_video"] == 1) ? 'checked ' : '').'/>
							    <input type="radio" name="product_video" id="hide_product_video" value="0" '.(($s["product_video"] == 0) ? 'checked ' : '').'/>
							    <label for="show_product_video" class="cb-enable'.(($s["product_video"] == 1) ? ' sel' : '').'"><span>'.$this->l('Show').'</span></label>
							    <label for="hide_product_video" class="cb-disable'.(($s["product_video"] == 0) ? ' sel' : '').'"><span>'.$this->l('Hide').'</span></label>
							</p>
							<p class="preference_description">'.$this->l('Show product video at the product page').'</p>
							</div>
						</div>
						<div class="margin form" id="custom_tab">
						<h4>'.$this->l('Custom Tab').'</h4>
							<div class="va-content tt-wrapper">
							<p class="variant switch">
							    <input type="radio" name="custom_tab" id="show_custom_tab" value="1" '.(($s["custom_tab"] == 1) ? 'checked ' : '').'/>
							    <input type="radio" name="custom_tab" id="hide_custom_tab" value="0" '.(($s["custom_tab"] == 0) ? 'checked ' : '').'/>
							    <label for="show_custom_tab" class="cb-enable'.(($s["custom_tab"] == 1) ? ' sel' : '').'"><span>'.$this->l('Show').'</span></label>
							    <label for="hide_custom_tab" class="cb-disable'.(($s["custom_tab"] == 0) ? ' sel' : '').'"><span>'.$this->l('Hide').'</span></label>
							</p>
							<p>'.$this->l('Display tab with custom information. You can manage content of this tab in the product section of your back office.').'</p>
							</div>
						</div>
						<div class="margin form" id="product_rating">
						<h4>'.$this->l('Product Rating (stars)').'</h4>
							<div class="va-content tt-wrapper">
							<p class="variant switch">
							    <input type="radio" name="product_rating" id="show_product_rating" value="1" '.(($s["product_rating"] == 1) ? 'checked ' : '').'/>
							    <input type="radio" name="product_rating" id="hide_product_rating" value="0" '.(($s["product_rating"] == 0) ? 'checked ' : '').'/>
							    <label for="show_product_rating" class="cb-enable'.(($s["product_rating"] == 1) ? ' sel' : '').'"><span>'.$this->l('Show').'</span></label>
							    <label for="hide_product_rating" class="cb-disable'.(($s["product_rating"] == 0) ? ' sel' : '').'"><span>'.$this->l('Hide').'</span></label>
							</p>
							</div>
						</div>
						<div class="margin form" id="share_buttons">
						<h4>'.$this->l('Share buttons').'</h4>
							<div class="va-content tt-wrapper">
							<p class="variant switch">
							    <input type="radio" name="share_buttons" id="show_share_buttons" value="1" '.(($s["share_buttons"] == 1) ? 'checked ' : '').'/>
							    <input type="radio" name="share_buttons" id="hide_share_buttons" value="0" '.(($s["share_buttons"] == 0) ? 'checked ' : '').'/>
							    <label for="show_share_buttons" class="cb-enable'.(($s["share_buttons"] == 1) ? ' sel' : '').'"><span>'.$this->l('Show').'</span></label>
							    <label for="hide_share_buttons" class="cb-disable'.(($s["share_buttons"] == 0) ? ' sel' : '').'"><span>'.$this->l('Hide').'</span></label>
							</p>
							</div>
						</div>
						<div class="margin form" id="short_desc">
						<h4>'.$this->l('Short Product Description').'</h4>
							<div class="va-content tt-wrapper">
							<p class="variant switch">
							    <input type="radio" name="short_desc" id="show_short_desc" value="1" '.(($s["short_desc"] == 1) ? 'checked ' : '').'/>
							    <input type="radio" name="short_desc" id="hide_short_desc" value="0" '.(($s["short_desc"] == 0) ? 'checked ' : '').'/>
							    <label for="show_short_desc" class="cb-enable'.(($s["short_desc"] == 1) ? ' sel' : '').'"><span>'.$this->l('Show').'</span></label>
							    <label for="hide_short_desc" class="cb-disable'.(($s["short_desc"] == 0) ? ' sel' : '').'"><span>'.$this->l('Hide').'</span></label>
							</p>
							</div>
						</div>	
						<div class="margin form" id="product_colorpicker">
						<h4>'.$this->l('ColorPicker Type').'</h4>
							<div class="va-content tt-wrapper">
							<p class="variant switch">
							    <input type="radio" name="product_colorpicker" id="show_product_colorpicker" value="1" '.(($s["product_colorpicker"] == 1) ? 'checked ' : '').'/>
							    <input type="radio" name="product_colorpicker" id="hide_product_colorpicker" value="0" '.(($s["product_colorpicker"] == 0) ? 'checked ' : '').'/>
							    <label for="show_product_colorpicker" class="cb-enable'.(($s["product_colorpicker"] == 1) ? ' sel' : '').'"><span>'.$this->l('Dropdown').'</span></label>
							    <label for="hide_product_colorpicker" class="cb-disable'.(($s["product_colorpicker"] == 0) ? ' sel' : '').'"><span>'.$this->l('List').'</span></label>
							</p>
							</div>
						</div>
						<!--<div class="margin form" id="maximize_image">
						<h4>'.$this->l('Maximize Product Image').'</h4>
							<div class="va-content tt-wrapper">
							<p class="variant switch">
							    <input type="radio" name="maximize_image" id="show_maximize_image" value="1" '.(($s["maximize_image"] == 1) ? 'checked ' : '').'/>
							    <input type="radio" name="maximize_image" id="hide_maximize_image" value="0" '.(($s["maximize_image"] == 0) ? 'checked ' : '').'/>
							    <label for="show_maximize_image" class="cb-enable'.(($s["maximize_image"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
							    <label for="hide_maximize_image" class="cb-disable'.(($s["maximize_image"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
							</p>
							<p class="preference_description">'.$this->l('This function add a zoom button above main product image.').'</p>
							</div>
						</div>	-->				
					</div>
					<div class="tabcontent" id="tab_content_9" '.(($s["tab_number"] == 9) ? 'style="display:block"' : '').'>		
					<input type="radio" class="hide" name="tab_number" id="tab_9" value="9" '.(($s["tab_number"] == 9) ? 'checked="checked"' : '').' />
						<div class="va-content tt-wrapper">
							<div class="margin form">
								<h4>'.$this->l('Add your CSS styles').'</h4>
								<textarea id="customcss" name="custom_css" cols="26" rows="3">'.$s["custom_css"].'</textarea><br/><br/>
								<div'.((Configuration::get("PS_CSS_THEME_CACHE") == 0) ? ' class="hide"' : '').'>
								<input class="button" type="submit" name="clear_css_cache" value="'.$this->l('Clear CSS Cache').'" />
								<p class="preference_description">'.$this->l('Clear CSS cache if your custom CSS code doesnt work').'</p>						
								</div>
							</div>							
						</div>
					</div>
					<div class="tabcontent" id="tab_content_10" '.(($s["tab_number"] == 10) ? 'style="display:block"' : '').'>
					<input type="radio" class="hide" name="tab_number" id="tab_10" value="10" '.(($s["tab_number"] == 10) ? 'checked="checked"' : '').' />
						<div class="va-content">
							<div class="margin form emails">
								<h4>'.$this->l('Facebook').'</h4>
								<p class="variant switch">
								    <input type="radio" name="email_fb" id="email_fb_en" value="1" '.(($s["email_fb"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="email_fb" id="email_fb_dis" value="0" '.(($s["email_fb"] == 0) ? 'checked ' : '').'/>
								    <label for="email_fb_en" class="swt_on cb-enable'.(($s["email_fb"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="email_fb_dis" class="swt_off cb-disable'.(($s["email_fb"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								</p>
								<h4>'.$this->l('Twitter').'</h4>
								<p class="variant switch">
								    <input type="radio" name="email_tw" id="email_tw_en" value="1" '.(($s["email_tw"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="email_tw" id="email_tw_dis" value="0" '.(($s["email_tw"] == 0) ? 'checked ' : '').'/>
								    <label for="email_tw_en" class="swt_on cb-enable'.(($s["email_tw"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="email_tw_dis" class="swt_off cb-disable'.(($s["email_tw"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								</p>
								<h4>'.$this->l('Youtube').'</h4>
								<p class="variant switch">
								    <input type="radio" name="email_yt" id="email_yt_en" value="1" '.(($s["email_yt"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="email_yt" id="email_yt_dis" value="0" '.(($s["email_yt"] == 0) ? 'checked ' : '').'/>
								    <label for="email_yt_en" class="swt_on cb-enable'.(($s["email_yt"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="email_yt_dis" class="swt_off cb-disable'.(($s["email_yt"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								</p>
								<h4>'.$this->l('Google+').'</h4>
								<p class="variant switch">
								    <input type="radio" name="email_gp" id="email_gp_en" value="1" '.(($s["email_gp"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="email_gp" id="email_gp_dis" value="0" '.(($s["email_gp"] == 0) ? 'checked ' : '').'/>
								    <label for="email_gp_en" class="swt_on cb-enable'.(($s["email_gp"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="email_gp_dis" class="swt_off cb-disable'.(($s["email_gp"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								</p>
								<p class="preference_description">'.$this->l('Accounts of social networks above you can change in the "Social Accounts" section.').'</p><br/><br/>
								<h4>'.$this->l('Emails').'</h4>
								<p class="variant switch he80">
								    <input type="radio" name="email_em" id="email_em_en" value="1" '.(($s["email_em"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="email_em" id="email_em_dis" value="0" '.(($s["email_em"] == 0) ? 'checked ' : '').'/>
								    <label for="email_em_en" class="swt_on cb-enable'.(($s["email_em"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="email_em_dis" class="swt_off cb-disable'.(($s["email_em"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								    <span class="swt_container'.(($s["email_em"] == 0) ? ' hide' : '').'">
								    <input type="text" size="20" name="email_em_acc" id="email_em_acc" value="'.$s["email_em_acc"].'" />
										<label class="t textinput" for="email_em_acc">'.$this->l('Your first email').'</label><br/>
									<input type="text" size="20" name="email_em_acc2" id="email_em_acc2" value="'.$s["email_em_acc2"].'" />
									<label class="t textinput" for="email_em_acc2">'.$this->l('Your second email').'</label>
									</span>
								</p>
								<h4>'.$this->l('Skype').'</h4>
								<p class="variant switch he80">
								    <input type="radio" name="email_sk" id="email_sk_en" value="1" '.(($s["email_sk"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="email_sk" id="email_sk_dis" value="0" '.(($s["email_sk"] == 0) ? 'checked ' : '').'/>
								    <label for="email_sk_en" class="swt_on cb-enable'.(($s["email_sk"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="email_sk_dis" class="swt_off cb-disable'.(($s["email_sk"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								    <span class="swt_container'.(($s["email_sk"] == 0) ? ' hide' : '').'">
								    <input type="text" size="20" name="email_sk_acc" id="email_sk_acc" value="'.$s["email_sk_acc"].'" />
										<label class="t textinput" for="email_sk_acc">'.$this->l('Your first skype').'</label>
									<input type="text" size="20" name="email_sk_acc2" id="email_sk_acc2" value="'.$s["email_sk_acc2"].'" />
									<label class="t textinput" for="email_sk_acc2">'.$this->l('Your second skype').'</label>
									</span>
								</p>
								<h4>'.$this->l('Mobile Phones').'</h4>
								<p class="variant switch he80">
								    <input type="radio" name="email_ph" id="email_ph_en" value="1" '.(($s["email_ph"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="email_ph" id="email_ph_dis" value="0" '.(($s["email_ph"] == 0) ? 'checked ' : '').'/>
								    <label for="email_ph_en" class="swt_on cb-enable'.(($s["email_ph"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="email_ph_dis" class="swt_off cb-disable'.(($s["email_ph"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								    <span class="swt_container'.(($s["email_ph"] == 0) ? ' hide' : '').'">
								    <input type="text" size="20" name="email_ph_acc" id="email_ph_acc" value="'.$s["email_ph_acc"].'" />
										<label class="t textinput" for="email_ph_acc">'.$this->l('Your first phone').'</label>
									<input type="text" size="20" name="email_ph_acc2" id="email_ph_acc2" value="'.$s["email_ph_acc2"].'" />
									<label class="t textinput" for="email_ph_acc2">'.$this->l('Your second phone').'</label>
									</span>
								</p>
								<h4>'.$this->l('Other Phones').'</h4>
								<p class="variant switch he80">
								    <input type="radio" name="email_oph" id="email_oph_en" value="1" '.(($s["email_oph"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="email_oph" id="email_oph_dis" value="0" '.(($s["email_oph"] == 0) ? 'checked ' : '').'/>
								    <label for="email_oph_en" class="swt_on cb-enable'.(($s["email_oph"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="email_oph_dis" class="swt_off cb-disable'.(($s["email_oph"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								    <span class="swt_container'.(($s["email_oph"] == 0) ? ' hide' : '').'">
								    <input type="text" size="20" name="email_oph_acc" id="email_oph_acc" value="'.$s["email_oph_acc"].'" />
										<label class="t textinput" for="email_oph_acc">'.$this->l('Your first phone').'</label>
									<input type="text" size="20" name="email_oph_acc2" id="email_oph_acc2" value="'.$s["email_oph_acc2"].'" />
									<label class="t textinput" for="email_oph_acc2">'.$this->l('Your second phone').'</label>
									</span>
								</p>
							</div>
							<div class="margin form">
								<h4>'.$this->l('Advertising Image').'</h4>
								<div class="variant switch">
									<input type="radio" name="email_adv" id="email_adv_yes" value="1" '.(($s["email_adv"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="email_adv" id="email_adv_no" value="0" '.(($s["email_adv"] == 0) ? 'checked ' : '').'/>
								    <label for="email_adv_yes" class="swt_on cb-enable'.(($s["email_adv"] == 1) ? ' sel' : '').'"><span>'.$this->l('Show').'</span></label>
								    <label for="email_adv_no" class="swt_off cb-disable'.(($s["email_adv"] == 0) ? ' sel' : '').'"><span>'.$this->l('Hide').'</span></label>
								    <div class="swt_container'.(($s["email_adv"] == 0) ? ' hide' : '').'">
										<div class="back_image_container">
											'.(($s["email_image"] != "") ? '<img class="back_image" src="'.$this->_path.'images/upload/'.$s["email_image"].'">' : 'No Image').'
										</div>									
										<input id="email_image" type="file" name="email_image">
										<input id="eimage" type="submit" class="button" name="email_image_upload">
										<input class="button'.(($s["email_image"] == "") ? ' hide' : '').'" type="submit" name="submitDeleteEmailImg" value="'.$this->l('Delete image').'" />
										<br/><br/>
										<div style="clear:both">										
										<label for="email_adv_link" style="width:100px; text-align:right">'.$this->l('Advertising Link').':</label>
										<input type="text" size="40" name="email_adv_link" id="email_adv_link" value="'.$s["email_adv_link"].'" />
										</div>
									</div>
							    </div>							    
							</div>							
							<div class="margin form">
								<h4>'.$this->l('Fix problems').'</h4>
								<p>'.$this->l('Use this function only if you have some problems with email templates (broken images, no confirmation emails, etc...)').'</p>
								<input class="button" type="submit" name="fixEmails" value="'.$this->l('Fix it').'" />
							</div>
						</div>
					</div>	
					<div class="tabcontent" id="tab_content_11" '.(($s["tab_number"] == 11) ? 'style="display:block"' : '').'>		
						<input type="radio" class="hide" name="tab_number" id="tab_11" value="11" '.(($s["tab_number"] == 11) ? 'checked="checked"' : '').' />
						<div class="margin">
							<div class="margin form">
							<h4>'.$this->l('Show icons in footer').'</h4>
								<p class="variant switch">
								    <input type="radio" name="socials_in_footer" id="socials_in_footer_en" value="1" '.(($s["socials_in_footer"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="socials_in_footer" id="socials_in_footer_dis" value="0" '.(($s["socials_in_footer"] == 0) ? 'checked ' : '').'/>
								    <label for="socials_in_footer_en" class="swt_on cb-enable'.(($s["socials_in_footer"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="socials_in_footer_dis" class="swt_off cb-disable'.(($s["socials_in_footer"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								</p>
							</div>
							<div class="margin form">
								<h4 class="facebook"><i></i>'.$this->l('Facebook').'</h4>
								<p class="variant switch">
								    <input type="radio" name="soc_fb" id="soc_fb_en" value="1" '.(($s["soc_fb"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="soc_fb" id="soc_fb_dis" value="0" '.(($s["soc_fb"] == 0) ? 'checked ' : '').'/>
								    <label for="soc_fb_en" class="swt_on cb-enable'.(($s["soc_fb"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="soc_fb_dis" class="swt_off cb-disable'.(($s["soc_fb"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								    <span class="swt_container'.(($s["soc_fb"] == 0) ? ' hide' : '').'">
								    <input type="text" size="20" name="email_fb_acc" id="email_fb_acc" value="'.$s["email_fb_acc"].'" />
										<label class="t textinput" for="email_fb_acc">'.$this->l('The link to your facebook page').'</label>
									</span>
								</p>
							</div>
							<div class="margin form">
								<h4 class="twitter"><i></i>'.$this->l('Twitter').'</h4>
								<p class="variant switch">
								    <input type="radio" name="soc_tw" id="soc_tw_en" value="1" '.(($s["soc_tw"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="soc_tw" id="soc_tw_dis" value="0" '.(($s["soc_tw"] == 0) ? 'checked ' : '').'/>
								    <label for="soc_tw_en" class="swt_on cb-enable'.(($s["soc_tw"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="soc_tw_dis" class="swt_off cb-disable'.(($s["soc_tw"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								    <span class="swt_container'.(($s["soc_tw"] == 0) ? ' hide' : '').'">
								    <input type="text" size="20" name="email_tw_acc" id="email_tw_acc" value="'.$s["email_tw_acc"].'" />
										<label class="t textinput" for="email_tw_acc">'.$this->l('The link to your twitter page').'</label>
									</span>
								</p>
							</div>
							<div class="margin form">
								<h4 class="youtube"><i></i>'.$this->l('Youtube').'</h4>
								<p class="variant switch">
								    <input type="radio" name="soc_yt" id="soc_yt_en" value="1" '.(($s["soc_yt"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="soc_yt" id="soc_yt_dis" value="0" '.(($s["soc_yt"] == 0) ? 'checked ' : '').'/>
								    <label for="soc_yt_en" class="swt_on cb-enable'.(($s["soc_yt"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="soc_yt_dis" class="swt_off cb-disable'.(($s["soc_yt"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								    <span class="swt_container'.(($s["soc_yt"] == 0) ? ' hide' : '').'">
								    <input type="text" size="20" name="email_yt_acc" id="email_yt_acc" value="'.$s["email_yt_acc"].'" />
										<label class="t textinput" for="email_yt_acc">'.$this->l('The link to your youtube page').'</label>
									</span>
								</p>
							</div>
							<div class="margin form">
								<h4 class="gplus"><i></i>'.$this->l('Google+').'</h4>
								<p class="variant switch">
								    <input type="radio" name="soc_gp" id="soc_gp_en" value="1" '.(($s["soc_gp"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="soc_gp" id="soc_gp_dis" value="0" '.(($s["soc_gp"] == 0) ? 'checked ' : '').'/>
								    <label for="soc_gp_en" class="swt_on cb-enable'.(($s["soc_gp"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="soc_gp_dis" class="swt_off cb-disable'.(($s["soc_gp"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								    <span class="swt_container'.(($s["soc_gp"] == 0) ? ' hide' : '').'">
								    <input type="text" size="20" name="email_gp_acc" id="email_gp_acc" value="'.$s["email_gp_acc"].'" />
										<label class="t textinput" for="email_gp_acc">'.$this->l('The link to your google+ page').'</label>
									</span>
								</p>
							</div>
							<div class="margin form">
								<h4 class="pinterest"><i></i>'.$this->l('Pinterest').'</h4>
								<p class="variant switch">
								    <input type="radio" name="soc_pi" id="soc_pi_en" value="1" '.(($s["soc_pi"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="soc_pi" id="soc_pi_dis" value="0" '.(($s["soc_pi"] == 0) ? 'checked ' : '').'/>
								    <label for="soc_pi_en" class="swt_on cb-enable'.(($s["soc_pi"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="soc_pi_dis" class="swt_off cb-disable'.(($s["soc_pi"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								    <span class="swt_container'.(($s["soc_pi"] == 0) ? ' hide' : '').'">
								    <input type="text" size="20" name="email_pi_acc" id="email_pi_acc" value="'.$s["email_pi_acc"].'" />
										<label class="t textinput" for="email_pi_acc">'.$this->l('The link to your pinterest page').'</label>
									</span>
								</p>
							</div>
							<div class="margin form">
								<h4 class="instagram"><i></i>'.$this->l('Instagram').'</h4>
								<p class="variant switch">
								    <input type="radio" name="soc_ig" id="soc_ig_en" value="1" '.(($s["soc_ig"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="soc_ig" id="soc_ig_dis" value="0" '.(($s["soc_ig"] == 0) ? 'checked ' : '').'/>
								    <label for="soc_ig_en" class="swt_on cb-enable'.(($s["soc_ig"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="soc_ig_dis" class="swt_off cb-disable'.(($s["soc_ig"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								    <span class="swt_container'.(($s["soc_ig"] == 0) ? ' hide' : '').'">
								    <input type="text" size="20" name="email_ig_acc" id="email_ig_acc" value="'.$s["email_ig_acc"].'" />
										<label class="t textinput" for="email_ig_acc">'.$this->l('The link to your pinterest page').'</label>
									</span>
								</p>
							</div>
							<div class="margin form">
								<h4 class="linkedin"><i></i>'.$this->l('LinkedIn').'</h4>
								<p class="variant switch">
								    <input type="radio" name="soc_li" id="soc_li_en" value="1" '.(($s["soc_li"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="soc_li" id="soc_li_dis" value="0" '.(($s["soc_li"] == 0) ? 'checked ' : '').'/>
								    <label for="soc_li_en" class="swt_on cb-enable'.(($s["soc_li"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="soc_li_dis" class="swt_off cb-disable'.(($s["soc_li"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								    <span class="swt_container'.(($s["soc_li"] == 0) ? ' hide' : '').'">
								    <input type="text" size="20" name="email_li_acc" id="email_li_acc" value="'.$s["email_li_acc"].'" />
										<label class="t textinput" for="email_li_acc">'.$this->l('The link to your pinterest page').'</label>
									</span>
								</p>
							</div>
							<div class="margin form">
								<h4 class="flickr"><i></i>'.$this->l('Flickr').'</h4>
								<p class="variant switch">
								    <input type="radio" name="soc_fl" id="soc_fl_en" value="1" '.(($s["soc_fl"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="soc_fl" id="soc_fl_dis" value="0" '.(($s["soc_fl"] == 0) ? 'checked ' : '').'/>
								    <label for="soc_fl_en" class="swt_on cb-enable'.(($s["soc_fl"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="soc_fl_dis" class="swt_off cb-disable'.(($s["soc_fl"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								    <span class="swt_container'.(($s["soc_fl"] == 0) ? ' hide' : '').'">
								    <input type="text" size="20" name="email_fl_acc" id="email_fl_acc" value="'.$s["email_fl_acc"].'" />
										<label class="t textinput" for="email_fl_acc">'.$this->l('The link to your pinterest page').'</label>
									</span>
								</p>						    					
							</div>							
						</div>
					</div>
					<div class="tabcontent" id="tab_content_12" '.(($s["tab_number"] == 12) ? 'style="display:block"' : '').'>		
					<input type="radio" class="hide" name="tab_number" id="tab_12" value="12" '.(($s["tab_number"] == 12) ? 'checked="checked"' : '').' />
						<div class="va-content tt-wrapper">
							<div class="margin form">
								<h4>'.$this->l('Enable Shop').'</h4>	
								<p class="variant switch">
								<input type="radio" name="maintenance" id="maintenance_en" value="1" '.((Configuration::get('PS_SHOP_ENABLE') == 1) ? 'checked ' : '').'/>
							    <input type="radio" name="maintenance" id="maintenance_dis" value="0" '.((Configuration::get('PS_SHOP_ENABLE') == 0) ? 'checked ' : '').'/>
							    <label for="maintenance_en" class="swt_on cb-enable'.((Configuration::get('PS_SHOP_ENABLE') == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
							    <label for="maintenance_dis" class="swt_off cb-disable'.((Configuration::get('PS_SHOP_ENABLE') == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								</p>
								<p class="preference_description">'.$this->l('Activate or deactivate your shop (It is a good idea to deactivate your shop while you perform maintenance. Please note that the webservice will not be disabled).').'</p><br/>
								<h4>'.$this->l('The time when you will open your shop').'</h4>'.$ready_date.'
								<br/><br/>
								<h4>'.$this->l('Maintenance Message').'</h4>
								<textarea id="ready_text" name="ready_text" class="swt_container" style="height:80px; width:100%" cols="26" rows="3">'.$s["ready_text"].'</textarea>
								<br/><br/>
								<!--<h4>'.$this->l('Choose CMS page for About Us column').'</h4>												
								<select name="maintanance_cms_page">'.$options.'</select><br/><br/>-->
								<h4>'.$this->l('Notification').'</h4>											
								<input class="button" type="submit" name="sendnotification" value="'.$this->l('Send').'" />
								<p class="preference_description">'.$this->l('Send notification that your shop is open for all subscribed people.').'</p><br/>
								<!--<h4>'.$this->l('Troubleshooter').'</h4>
								<div>
								<input class="button" type="submit" name="fixhook" value="'.$this->l('Try to fix the issue').'" />
								<p class="preference_description">'.$this->l('This function recreating the hook (comingsoon). Should help if you have old maintenance page.').'</p>
								</div>-->
							</div>							
						</div>
					</div>
					<div class="tabcontent pay" id="tab_content_13" '.(($s["tab_number"] == 13) ? 'style="display:block"' : '').'>		
					<input type="radio" class="hide" name="tab_number" id="tab_13" value="13" '.(($s["tab_number"] == 13) ? 'checked="checked"' : '').' />
						<div class="va-content tt-wrapper">
							<div class="margin form">
								<h4>'.$this->l('Show payment icons in footer').'</h4>
								<p class="variant switch">
								    <input type="radio" name="pay_in_footer" id="pay_in_footer_en" value="1" '.(($s["pay_in_footer"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="pay_in_footer" id="pay_in_footer_dis" value="0" '.(($s["pay_in_footer"] == 0) ? 'checked ' : '').'/>
								    <label for="pay_in_footer_en" class="swt_on cb-enable'.(($s["pay_in_footer"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="pay_in_footer_dis" class="swt_off cb-disable'.(($s["pay_in_footer"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								</p>
							</div>
							<div class="margin form">
								<h4 class="am_exp"><i></i>'.$this->l('American Express').'</h4>
								<p class="variant switch">
								    <input type="radio" name="pay_am_exp" id="pay_am_exp_en" value="1" '.(($s["pay_am_exp"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="pay_am_exp" id="pay_am_exp_dis" value="0" '.(($s["pay_am_exp"] == 0) ? 'checked ' : '').'/>
								    <label for="pay_am_exp_en" class="swt_on cb-enable'.(($s["pay_am_exp"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="pay_am_exp_dis" class="swt_off cb-disable'.(($s["pay_am_exp"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								</p>
							</div>	
							<div class="margin form">
								<h4 class="cirrus"><i></i>'.$this->l('Cirrus').'</h4>
								<p class="variant switch">
								    <input type="radio" name="pay_cirrus" id="pay_cirrus_en" value="1" '.(($s["pay_cirrus"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="pay_cirrus" id="pay_cirrus_dis" value="0" '.(($s["pay_cirrus"] == 0) ? 'checked ' : '').'/>
								    <label for="pay_cirrus_en" class="swt_on cb-enable'.(($s["pay_cirrus"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="pay_cirrus_dis" class="swt_off cb-disable'.(($s["pay_cirrus"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								</p>
							</div>	
							<div class="margin form">
								<h4 class="mastercard"><i></i>'.$this->l('Mastercard').'</h4>
								<p class="variant switch">
								    <input type="radio" name="pay_mastercard" id="pay_mastercard_en" value="1" '.(($s["pay_mastercard"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="pay_mastercard" id="pay_mastercard_dis" value="0" '.(($s["pay_mastercard"] == 0) ? 'checked ' : '').'/>
								    <label for="pay_mastercard_en" class="swt_on cb-enable'.(($s["pay_mastercard"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="pay_mastercard_dis" class="swt_off cb-disable'.(($s["pay_mastercard"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								</p>
							</div>
							<div class="margin form">
								<h4 class="maestro"><i></i>'.$this->l('Maestro').'</h4>
								<p class="variant switch">
								    <input type="radio" name="pay_maestro" id="pay_maestro_en" value="1" '.(($s["pay_maestro"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="pay_maestro" id="pay_maestro_dis" value="0" '.(($s["pay_maestro"] == 0) ? 'checked ' : '').'/>
								    <label for="pay_maestro_en" class="swt_on cb-enable'.(($s["pay_maestro"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="pay_maestro_dis" class="swt_off cb-disable'.(($s["pay_maestro"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								</p>
							</div>	
							<div class="margin form">
								<h4 class="paypal"><i></i>'.$this->l('Paypal').'</h4>
								<p class="variant switch">
								    <input type="radio" name="pay_paypal" id="pay_paypal_en" value="1" '.(($s["pay_paypal"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="pay_paypal" id="pay_paypal_dis" value="0" '.(($s["pay_paypal"] == 0) ? 'checked ' : '').'/>
								    <label for="pay_paypal_en" class="swt_on cb-enable'.(($s["pay_paypal"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="pay_paypal_dis" class="swt_off cb-disable'.(($s["pay_paypal"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								</p>
							</div>
							<div class="margin form">
								<h4 class="discover"><i></i>'.$this->l('Discover').'</h4>
								<p class="variant switch">
								    <input type="radio" name="pay_discover" id="pay_discover_en" value="1" '.(($s["pay_discover"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="pay_discover" id="pay_discover_dis" value="0" '.(($s["pay_discover"] == 0) ? 'checked ' : '').'/>
								    <label for="pay_discover_en" class="swt_on cb-enable'.(($s["pay_discover"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="pay_discover_dis" class="swt_off cb-disable'.(($s["pay_discover"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								</p>
							</div>								
							<div class="margin form">
								<h4 class="direct"><i></i>'.$this->l('Direct').'</h4>
								<p class="variant switch">
								    <input type="radio" name="pay_direct" id="pay_direct_en" value="1" '.(($s["pay_direct"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="pay_direct" id="pay_direct_dis" value="0" '.(($s["pay_direct"] == 0) ? 'checked ' : '').'/>
								    <label for="pay_direct_en" class="swt_on cb-enable'.(($s["pay_direct"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="pay_direct_dis" class="swt_off cb-disable'.(($s["pay_direct"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								</p>
							</div>	
							<div class="margin form">
								<h4 class="solo"><i></i>'.$this->l('Solo').'</h4>
								<p class="variant switch">
								    <input type="radio" name="pay_solo" id="pay_solo_en" value="1" '.(($s["pay_solo"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="pay_solo" id="pay_solo_dis" value="0" '.(($s["pay_solo"] == 0) ? 'checked ' : '').'/>
								    <label for="pay_solo_en" class="swt_on cb-enable'.(($s["pay_solo"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="pay_solo_dis" class="swt_off cb-disable'.(($s["pay_solo"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								</p>
							</div>
							<div class="margin form">
								<h4 class="switch"><i></i>'.$this->l('Switch').'</h4>
								<p class="variant switch">
								    <input type="radio" name="pay_switch" id="pay_switch_en" value="1" '.(($s["pay_switch"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="pay_switch" id="pay_switch_dis" value="0" '.(($s["pay_switch"] == 0) ? 'checked ' : '').'/>
								    <label for="pay_switch_en" class="swt_on cb-enable'.(($s["pay_switch"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="pay_switch_dis" class="swt_off cb-disable'.(($s["pay_switch"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								</p>
							</div>
							<div class="margin form">
								<h4 class="visa"><i></i>'.$this->l('Visa').'</h4>
								<p class="variant switch">
								    <input type="radio" name="pay_visa" id="pay_visa_en" value="1" '.(($s["pay_visa"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="pay_visa" id="pay_visa_dis" value="0" '.(($s["pay_visa"] == 0) ? 'checked ' : '').'/>
								    <label for="pay_visa_en" class="swt_on cb-enable'.(($s["pay_visa"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="pay_visa_dis" class="swt_off cb-disable'.(($s["pay_visa"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								</p>
							</div>
							<div class="margin form">
								<h4 class="wu"><i></i>'.$this->l('Western Union').'</h4>
								<p class="variant switch">
								    <input type="radio" name="pay_wu" id="pay_wu_en" value="1" '.(($s["pay_wu"] == 1) ? 'checked ' : '').'/>
								    <input type="radio" name="pay_wu" id="pay_wu_dis" value="0" '.(($s["pay_wu"] == 0) ? 'checked ' : '').'/>
								    <label for="pay_wu_en" class="swt_on cb-enable'.(($s["pay_wu"] == 1) ? ' sel' : '').'"><span>'.$this->l('Yes').'</span></label>
								    <label for="pay_wu_dis" class="swt_off cb-disable'.(($s["pay_wu"] == 0) ? ' sel' : '').'"><span>'.$this->l('No').'</span></label>
								</p>
							</div>							
						</div>
					</div>					
				</div>				
				</div>
				<div class="footer_section">
					<input type="submit" name="resetThemeSettings" value="'.$this->l('Reset Settings').'" class="button" id="reset_sett" />
					<span>'.$this->versions.' | '.$this->l('Powered by').' <a target="_blank" href="http://promokit.eu">Promokit Co.</a></span></div>
			</fieldset>
		</form>';		
	}

	private function getCMSOptions($parent = 0, $depth = 1, $curr) {
		$id_lang = (int)Context::getContext()->language->id;		
		$pages = $this->getCMSPages((int)$parent, (int)$id_lang);

		$opts = "";
		foreach ($pages as $page) {
			$opts .= '<option '.(($page["id_cms"] == $curr) ? "selected": "").' value="'.$page['id_cms'].'">'.$page['meta_title'].'</option>';
		}
		return $opts;
	}
	private function getCMSPages($id_cms_category, $id_lang) {
		$id_shop = (int)Context::getContext()->shop->id;
		$sql = 'SELECT c.`id_cms`, cl.`meta_title`, cl.`link_rewrite`
			FROM `' . _DB_PREFIX_ . 'cms` c
			INNER JOIN `' . _DB_PREFIX_ . 'cms_shop` cs
			ON (c.`id_cms` = cs.`id_cms`)
			INNER JOIN `' . _DB_PREFIX_ . 'cms_lang` cl
			ON (c.`id_cms` = cl.`id_cms`)
			WHERE c.`id_cms_category` = ' . (int)$id_cms_category . '
			AND cs.`id_shop` = ' . (int)$id_shop . '
			AND cl.`id_lang` = ' . (int)$id_lang . '
			AND c.`active` = 1
			ORDER BY `position`';

		return Db::getInstance()->executeS($sql);
	}

	function prepareResult($control, $theme_settings=null) {
		
		if (!$theme_settings) $theme_settings = $this->getOptions();

		$view = $theme_settings['view'];
		$imageSize = $theme_settings['productImageSize'];
		
		$sizes = array();
		for ($i = 15; $i < $this->maxSize; $i++) {
			$sizes[$i] = $i;
		}

		if ($view == 0) {
			$view = "view_grid";
		} else {
			$view = "view_list";
		}

		if ($imageSize == 0) {
			$imageSize = "view_small";
		} else {
			$imageSize = "view_big";
		}	

		$comments["install"] = $this->isInst("productcomments");
		$comments["enable"] = $this->isEn("productcomments");

		$i = 0;
		foreach ($this->defaultFonts as $type => $font) {
			$dFonts[$i] = $font;
			$i++;
		}
		if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== FALSE) { $browser = "IE"; } else { $browser = ""; }

		$theme_settings["view"] = $view;
		$theme_settings['imageSize'] = $imageSize;
		$theme_settings['logo_font'] = $this->fontNameAdaptation($theme_settings['logo_font']);
		$theme_settings['fonts_list'] = $this->fontslist();
		$theme_settings['sizes'] = $sizes;
		$theme_settings['patternsQuantity'] = $this->patternsQuantity;
		$theme_settings['defaultFonts'] = $dFonts;
		$theme_settings['systemFonts'] = $this->systemFonts;		
		$theme_settings['selectors'] = $this->selectors;
		$theme_settings['browser'] = $browser;
		$theme_settings['protocol'] = Tools::getShopProtocol();
		$theme_settings['version'] = $this->versions;
		$theme_settings['comments_module'] = $comments;
		$theme_settings['manager'] = $this->name;
		
		return $theme_settings;

	}

	public function isInst($name) {	

		if (Module::isInstalled($name)) {
			return "installed";
		} else {
			return "not_installed";
		}		
	}

	public function isEn($name) {	

		if (Module::isEnabled($name)) {
			return "enabled";
		} else {
			return "disabled";
		}		
	}

	public static function isCustomerFavoriteProduct($id_customer, $id_product, Shop $shop = null)
	{
		if (!$id_customer)
			return false;

		if (!$shop)
			$shop = Context::getContext()->shop;

		return (bool)Db::getInstance()->getValue('
			SELECT COUNT(*)
			FROM `'._DB_PREFIX_.'favorite_product`
			WHERE `id_customer` = '.(int)$id_customer.'
			AND `id_product` = '.(int)$id_product.'
			AND `id_shop` = '.(int)$shop->id);
	}	

	public function getUserWishlists($id_customer, $shop_id, $prod_id) { // check is product in customer wishlist

		$wishlist_ids = Db::getInstance()->ExecuteS('SELECT `id_wishlist` FROM `'._DB_PREFIX_.'wishlist` WHERE `id_customer` = '.$id_customer);
		$ids = array();
		foreach ($wishlist_ids as $wishlist_id => $wishlist) {
			foreach ($wishlist as $key => $id) {
				 $data = Db::getInstance()->ExecuteS('SELECT `id_product` FROM `'._DB_PREFIX_.'wishlist_product` 			WHERE `id_wishlist` = '.(int)$id);					 
				 foreach ($data as $k => $pid) {				 
					$ids[$wishlist_id."-".$k] = $pid["id_product"];					
				 }				 
			}
		}
		if (in_array($prod_id, $ids)) {
			$inWishList = true;
		} else {
			$inWishList = false;
		}

		return $inWishList;
	}

	public function getImLink($link_rewrite, $img_id, $imgName) {	

		return $this->context->link->getImageLink($link_rewrite, $img_id, $imgName);
		
	}

	public function getImg($product_id, $link_rewrite, $imgName, $imgAttr = false) {	
		if ($imgAttr == false) {
			$imgAttr = Image::getCover($product_id);	
			$imgAttr = $imgAttr["id_image"];
		}
		$img = $this->getImLink($link_rewrite, (int)$imgAttr["id_image"], $imgName);
		return $img;
	}	
	public function getImgByAttr($product_id, $imgAttr, $link_rewrite) {
		$img = $this->getImLink($link_rewrite, (int)$cover["id_image"], $imgName);
		return $img;
	}	
	
	public function getSocialAccouts($ts) {	
		$soc = array();
		if ($ts["soc_fb"] == 1) $soc["facebook"] = $ts["email_fb_acc"];
		if ($ts["soc_tw"] == 1) $soc["twitter"] = $ts["email_tw_acc"];
		if ($ts["soc_gp"] == 1) $soc["gplus"] = $ts["email_gp_acc"];
		if ($ts["soc_yt"] == 1) $soc["youtube"] = $ts["email_yt_acc"];
		if ($ts["soc_fl"] == 1) $soc["flickr"] = $ts["email_fl_acc"];
		if ($ts["soc_ig"] == 1) $soc["instagramm"] = $ts["email_ig_acc"];
		if ($ts["soc_pi"] == 1) $soc["pinterest"] = $ts["email_pi_acc"];
		if ($ts["soc_li"] == 1) $soc["linkedin"] = $ts["email_li_acc"];
		return $soc;
	}
	public function getPaymentIcons($ts) {	
		$pay = array();
		if ($ts["pay_visa"] == 1) $pay["visa"] = $ts["pay_visa"];
		if ($ts["pay_am_exp"] == 1) $pay["am_exp"] = $ts["pay_am_exp"];
		if ($ts["pay_mastercard"] == 1) $pay["mastercard"] = $ts["pay_mastercard"];
		if ($ts["pay_paypal"] == 1) $pay["paypal"] = $ts["pay_paypal"];
		if ($ts["pay_maestro"] == 1) $pay["maestro"] = $ts["pay_maestro"];
		if ($ts["pay_discover"] == 1) $pay["discover"] = $ts["pay_discover"];
		if ($ts["pay_cirrus"] == 1) $pay["cirrus"] = $ts["pay_cirrus"];
		if ($ts["pay_direct"] == 1) $pay["direct"] = $ts["pay_direct"];
		if ($ts["pay_solo"] == 1) $pay["solo"] = $ts["pay_solo"];
		if ($ts["pay_switch"] == 1) $pay["switch"] = $ts["pay_switch"];
		if ($ts["pay_wu"] == 1) $pay["wu"] = $ts["pay_wu"];
		return $pay;
	}
	/* CUSTOM HOOKS */
	public function hookfreePosition($params) {

		$this->smarty->assign(
			'maintenance_date', $this->prepareResult("maintenance")
		);

		return ($this->display(__FILE__, $this->name.'.tpl'));

	}
	public function hookcomingsoon($params)
	{
		$theme_settings = $this->getOptions();
		$cs["min"] = sprintf('%02d', $theme_settings["ready_min"]);
	    $cs["hour"] = sprintf('%02d', $theme_settings["ready_hour"]);
	    $cs["day"] = sprintf('%02d', $theme_settings["ready_day"]);
	    $cs["mon"] = sprintf('%02d', $theme_settings["ready_month"]);
	    $cs["year"] = sprintf('%02d', $theme_settings["ready_year"]);
	    $cs["mes"] = $theme_settings["ready_text"];
	    $cs["status"] = Configuration::get('PS_SHOP_ENABLE');
	    $cs["date_set"] = $theme_settings["date_set"];
	    $cs["addr"] = $theme_settings["location"];
	    $cs["location_lat"] = $theme_settings["location_lat"];
	    $cs["location_lng"] = $theme_settings["location_lng"];

	    $cms = new CMS($theme_settings["maintanance_cms_page"], (int)Context::getContext()->language->id);

	   	$soc = $this->getSocialAccouts($theme_settings);	   	
	   	
	   	$facebookCacheFile = _PS_MODULE_DIR_.'pk_facebooklike/fans_'.(int)Context::getContext()->shop->id.'.tpl';
	   	if (file_exists($facebookCacheFile)) {
	   		$fileHolder = $facebookCacheFile;	
	   	} else {
	   		$fileHolder = false;	
	   	}

		$this->context->smarty->assign(
		  array(
		      'cs' => $cs,
		      'soc' => $soc,		      
		      'name' => Configuration::get('PS_SHOP_NAME'),
		      'module_dir' => $this->context->link->getModuleLink($this->name),
		      'facebooklike' => $fileHolder,
		      'mainURL' => __PS_BASE_URI__,
		      'aboutus' => $cms->content
		  )
		);	
		return $this->display(__FILE__, 'views/frontend/comingsoon.tpl');
	}

	public function hookfooter_bottom($params)
	{
		$ts = $this->getOptions();
		$pay = $this->getPaymentIcons($ts);
		$soc = $this->getSocialAccouts($ts);
		$this->context->smarty->assign(array(
			"soc" => $soc,
			'pay' => $pay,
			"module_dir" => $this->context->link->getModuleLink($this->name)
		));	
		return $this->display(__FILE__, 'views/frontend/footer_bottom.tpl');
	}
	/*	search request	*/
	public function getSearchResult($pID, $cRewrite, $image_name) {
		$currency = Context::getContext()->currency->id;
		if (isset($pID)) {
			$cover = Image::getCover($pID);		
		}
		$img = (int)$cover['id_product'].'-'.(int)$cover['id_image'];
		if (isset($cRewrite)) {
			$data["link"] = $this->context->link->getImageLink($cRewrite, $img, $image_name);
		} 
		$data["price"] = Tools::displayPrice(Product::getPriceStatic($pID), $currency);
		print_r(json_encode($data));
	}

	public function productVideo($params) {
		$s = $this->getOptions();
		$id_product = Tools::getValue('id_product');
		$sid = (int)Context::getContext()->shop->id;
		$lid = $this->context->language->id;
		$getVideo = Db::getInstance()->ExecuteS('SELECT `video` FROM `'._DB_PREFIX_.'pk_product_extratabs` WHERE  id_product = '.$id_product.' AND shop_id = '.$sid.' AND lang_id = '.$lid);
		//if (isset($getVideo[0]["video"]) && $getVideo[0]["video"] != "") {
	        $this->context->smarty->assign(array(
	            'pk_video_id' => $getVideo[0]["video"],
	            'id_lang' => $lid,
	            'languages' => Language::getLanguages(true)
	        ));
	    //}
        if ($s["product_video"] == 1) {
	        return $this->display(__FILE__, 'views/frontend/video.tpl');
	    } else {
	    	return false;
	    }

	}		

	/*	BACKOFFICE HOOKS	*/
	public function hookdisplayAdminHomeQuickLinks($params) {

		echo "<li id=\"ts_block\" style=\"background:#F8F8F8 url('../modules/".$this->name."/logo.png') no-repeat 50% 20px\">
			<a href=\"index.php?controller=adminmodules&configure=".$this->name."&token=".Tools::getAdminTokenLite('AdminModules')."\">
				<h4><strong>Theme Settings</strong></h4>
				<p>Customize your theme</p>
			</a>
		</li>";
	}

	public function hookDisplayAdminProductsExtra($params) {
		$s = $this->getOptions();
		$id_product = Tools::getValue('id_product');
		$languages = Language::getLanguages(true);

        $id_lang_default = Configuration::get('PS_LANG_DEFAULT');     

        $custom_tab = $custom_tab_name = $video_id = array();
        foreach ($languages as $id => $language) {       

        	$getData = Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'pk_product_extratabs` WHERE  id_product = '.$id_product.' AND shop_id = '.(int)Context::getContext()->shop->id.' AND lang_id = '.$language["id_lang"]); 	

        	if (isset($getData[0])) {
        		$custom_tab[$language["id_lang"]] = $getData[0]["custom_tab"];
        		$custom_tab_name[$language["id_lang"]] = $getData[0]["custom_tab_name"];
        		$video_id[$language["id_lang"]] = $getData[0]["video"];
			}
        	if ($language["id_lang"] == $id_lang_default) {
        		$languages[$id]["is_default"] = true;
        	} else {
        		$languages[$id]["is_default"] = false;
        	}       	
        }      
        $this->context->smarty->assign(array(
        	'show_video' => $s["product_video"],
        	'show_custom_tab' => $s["custom_tab"],
            'pk_video_id' => $video_id,
            'pk_custom_tab' => $custom_tab,
            'pk_custom_tab_name' => $custom_tab_name,
            'languages' => $languages,
            'id_lang' => $id_lang_default,
            'iso_tiny_mce' => (Tools::file_exists_cache(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.Context::getContext()->language->iso_code.'.js') ? Context::getContext()->language->iso_code : 'en'),
			'ad' => dirname($_SERVER['PHP_SELF'])
        ));
        
        return $this->display(__FILE__, 'views/admin/video.tpl');
    }
    
    public function hookActionProductUpdate($params) {
    	$sql = array();
        $id_product = Tools::getValue('id_product');
        $languages = Language::getLanguages();        
        foreach ($languages as $id_lang => $language) {
        	$custom_tab = Tools::getValue('custom_tab_'.$language["id_lang"]);
        	$custom_tab_name = Tools::getValue('custom_tab_name_'.$language["id_lang"]);
        	$video_id = Tools::getValue('video_id_'.$language["id_lang"]); 

        	$check = Db::getInstance()->ExecuteS('SELECT id_pet FROM `'._DB_PREFIX_.'pk_product_extratabs` WHERE id_product = '.$id_product.' AND shop_id = '.(int)Context::getContext()->shop->id.' AND lang_id = '.$language["id_lang"].';');
        	if (!empty($check)) {
        		$sql[] = 'UPDATE `'._DB_PREFIX_.'pk_product_extratabs` SET custom_tab_name = "'.$custom_tab_name.'", custom_tab = \''.$custom_tab.'\', video="'.$video_id.'" WHERE id_product = '.$id_product.' AND shop_id = '.(int)Context::getContext()->shop->id.' AND lang_id = '.$language["id_lang"].';';
    		} else {
    			$sql[] = 'INSERT INTO `'._DB_PREFIX_.'pk_product_extratabs` (`id_product`, `shop_id`, `lang_id`, `video`, `custom_tab_name`, `custom_tab`) VALUES ('.$id_product.', '.(int)Context::getContext()->shop->id.', '.$language["id_lang"].', "'.$video_id.'", "'.$custom_tab_name.'", "'.$custom_tab.'")';	
    			}        		       			
        }                
        $this->runSql($sql);
    }
    /*	FRONTPAGE HOOKS	*/
    public function hookProductTab($params) {
    	$s = $this->getOptions();
        $id_product = Tools::getValue('id_product');
        $sid = (int)Context::getContext()->shop->id;
		$lid = $this->context->language->id;
        $getCustomTab = Db::getInstance()->ExecuteS('SELECT `custom_tab_name` FROM `'._DB_PREFIX_.'pk_product_extratabs` WHERE  id_product = '.$id_product.' AND shop_id = '.$sid.' AND lang_id = '.$lid);        
        $tab = "";
        if (($s["custom_tab"]) && isset($getCustomTab[0]["custom_tab_name"]) && $getCustomTab[0]["custom_tab_name"] != "") {
            $tab .= "<h3 data-title=\"12\">".$getCustomTab[0]["custom_tab_name"]."</h3>";
        }
        if ($s["product_video"] == 1) {
            $tab .= "<h3 data-title=\"13\">".$this->l('Video')."</h3>";
        }
	    return $tab;
    }

    public function hookProductTabContent($params) {
    	$s = $this->getOptions();
        $id_product = Tools::getValue('id_product');
        $sid = (int)Context::getContext()->shop->id;
		$lid = $this->context->language->id;
        $getCustomTab = Db::getInstance()->ExecuteS('SELECT `custom_tab`, `custom_tab_name` FROM `'._DB_PREFIX_.'pk_product_extratabs` WHERE  id_product = '.$id_product.' AND shop_id = '.$sid.' AND lang_id = '.$lid);
       	$getVideo = Db::getInstance()->ExecuteS('SELECT `video` FROM `'._DB_PREFIX_.'pk_product_extratabs` WHERE  id_product = '.$id_product.' AND shop_id = '.$sid.' AND lang_id = '.$lid);
        if (isset($getCustomTab[0])) {
            $this->context->smarty->assign(array(
                'pk_custom_tab' => $getCustomTab[0]["custom_tab"],
                'pk_custom_tab_name' => $getCustomTab[0]["custom_tab_name"],
                'pk_video_id' => $getVideo[0]["video"]
            ));
        }
	    return $this->display(__FILE__, 'views/frontend/customcontent.tpl');	    
    }

	function hookHeader($params)
	{	
		
		$theme_settings = $this->getOptions();
		$themeUrl = __PS_BASE_URI__.'themes/'._THEME_NAME_.'/css/';
		$this->context->controller->addCSS(($this->_path).'css/themesettings.css', 'all');
		$this->context->controller->addCSS(($this->_path).'css/chosen.css', 'all');		
		$this->context->controller->addCSS($themeUrl.'alysum.css', 'all');
		if ($theme_settings["responsive"] == 1) {
			$this->context->controller->addCSS($themeUrl.'responsive-all-mobile-devices.css', 'all');
			$this->context->controller->addCSS($themeUrl.'responsive-tablet-landscape.css', 'all');
			$this->context->controller->addCSS($themeUrl.'responsive-tablet-portrait.css', 'all');			
			$this->context->controller->addCSS($themeUrl.'responsive-phone-landscape.css', 'all');
			$this->context->controller->addCSS($themeUrl.'responsive-phone-portrait.css', 'all');			
		}		
		if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== FALSE) { 
			$this->context->controller->addCSS(($this->_path).'css/ie.css', 'all');
		}
		$this->context->controller->addCSS(($this->_path).'css/customsettings'.(int)Context::getContext()->shop->id.'.css', 'all');
		if ($theme_settings["responsive"] == 1) {
			$this->context->controller->addJS(($this->_path).'js/modernizr.custom.08176.js');		
		}
		$this->context->controller->addJS(($this->_path).'js/jquery.colorpicker.js');
		$this->context->controller->addJS(($this->_path).'js/impromtu.js');
		$this->context->controller->addJS(($this->_path).'js/commonscripts.js');
		$this->context->controller->addJS(($this->_path).'js/jquery.bxslider.js');
		$this->context->controller->addJS(($this->_path).'js/jquery.flexisel.js');
		if ($theme_settings["toTop"] == 1) {
			$this->context->controller->addJS(_PS_JS_DIR_.'jquery/plugins/jquery.scrollTo.js');
		}

		$this->context->smarty->assign(
			'theme_settings', $this->prepareResult("head", $theme_settings)
		);
		$this->context->smarty->assign(
			'shopID', (int)$this->context->shop->id
		);
		
		return ($this->display(__FILE__, 'views/frontend/scripts.tpl'));
		
	}

}