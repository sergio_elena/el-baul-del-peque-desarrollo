$(document).ready(function() {    

    var currentBreakpoint; // default's to blank so it's always analysed on first load
    var didResize  = true; // default's to true so it's always analysed on first load
    // on window resize, set the didResize to true
    $(window).resize(function() { didResize = true; });

    // every 1/2 second, check if the browser was resized
    // we throttled this because some browsers fire the resize even continuously during resize
    // that causes excessive processing, this helps limit that
    setInterval(function() {
        if(didResize) {

            didResize = false;
            var newBreakpoint = $(window).width();

            if (newBreakpoint > 1000) 
                newBreakpoint = "breakpoint_1";
            else if ((newBreakpoint <= 1000) && (newBreakpoint >= 728)) 
                newBreakpoint = "breakpoint_2";
            else if ((newBreakpoint <= 727) && (newBreakpoint >= 480)) 
                newBreakpoint = "breakpoint_3";
            else if (newBreakpoint <= 479) 
                newBreakpoint = "breakpoint_4";

            if (currentBreakpoint != newBreakpoint) {            

                if (newBreakpoint === 'breakpoint_1') {// min-width: 1000px

                    currentBreakpoint = 'breakpoint_1';
                    $(".product_list > li").removeClass("no-right-margin");
                    $(".view_grid .product_list > li:nth-child(4n+4)").addClass("no-right-margin");
                    $("body").addClass("desctop-scr").removeClass("mobile-scr");     
                }            
                if (newBreakpoint === 'breakpoint_2') {//max-width: 1000px
                    currentBreakpoint = 'breakpoint_2';
                    $("body").addClass("desctop-scr").removeClass("mobile-scr");     

                }               
                if (newBreakpoint === 'breakpoint_3') {// max-width: 768px
                    currentBreakpoint = 'breakpoint_3';
                    $("body").addClass("mobile-scr").removeClass("desctop-scr");

                }               
                if (newBreakpoint === 'breakpoint_4') {//max-width: 479px
                    currentBreakpoint = 'breakpoint_4';
                    $("body").addClass("mobile-scr").removeClass("desctop-scr");

                }   
            }
        }
      }, 500);                                   

    
	var is_touch_device = 'ontouchstart' in document.documentElement;
    if (is_touch_device) {
        //$("html").addClass("touch");
        // dropdown elements 
		$('.dd_el').toggle(
			function () {$(this).find('.dd_container').stop().slideDown(300, 'easeOutQuint');}, 
			function () {$(this).find('.dd_container').stop().slideUp(300, 'easeOutQuint');
		});
        $('.dd_el_mobile').toggle(
            function () {
                $(this).parent().addClass('active');
                $(this).parent().find('.dd_container_mobile').stop().slideDown(300, 'easeOutQuint');}, 
            function () {
                $(this).parent().removeClass('active');
                $(this).parent().find('.dd_container_mobile').stop().slideUp(300, 'easeOutQuint');
        });

    } else {
        //$("html").addClass("no-touch");  

        // dropdown elements 
		$('.dd_el').hover(
			function () {$(this).find('.dd_container').stop().slideDown(500, 'easeOutQuint');}, 
			function () {$(this).find('.dd_container').stop().slideUp(200, 'easeOutQuint');
		});	
        $('.dd_el_mobile').toggle(
            function () {
                $(this).parent().addClass('active')
                $(this).parent().find('.dd_container_mobile').stop().slideDown(500, 'easeOutQuint');}, 
            function () {
                $(this).parent().removeClass('active');
                $(this).parent().find('.dd_container_mobile').stop().slideUp(200, 'easeOutQuint');
        });   
                    
    }

    // remove old <style> tag from <head>
    $("style#logofont_styles").remove();
    $("style#sloganfont_styles").remove();
    $("style#headings_style").remove();
    $("style#subheadings_style").remove();
    $("style#text_style").remove();
    $("style#buttonfont_styles").remove();

    // social login
    if ($("#login-link").length != 0) {
        var p = $("#login-link");
        var position = p.position();
        $(".interfacecontainerdiv").css({ 'left': position.left-15 })
        $(p).live({
            mouseenter:
               function(){$(".interfacecontainerdiv").show('fast');},
            mouseleave:
               function(){$(".interfacecontainerdiv").hide();}
           });
        $('.interfacecontainerdiv').live({
            mouseenter:
               function(){$(".interfacecontainerdiv").show(); },
            mouseleave:
               function(){$(".interfacecontainerdiv").hide(); }
        });
    }
    
    // compare (product-list)
    //$('.comparator').click(function(){
    $(".product_compare label").live('click', function() {    
	    $(this).parent().toggleClass('active', $(this).attr('checked'));
	});

	// grid/list view. change DOM
   $("#view_grid").click(function() {
         $("#view_list").removeClass("active");
         $(this).addClass("active");
         $("#listing_view").addClass("view_grid").removeClass("view_list"); // set class            
         $.cookie("listingView", "view_grid"); // set cookie
         $("#listing_view").animate({opacity: "0"}, 0);// set class         
         $("#listing_view").delay(200).animate({opacity: "1"}, 300);// set class      
    });
    $("#view_list").click(function() {
        $("#view_grid").removeClass("active");
         $(this).addClass("active");
         $("#listing_view").addClass("view_list").removeClass("view_grid"); // set class           
         $.cookie("listingView", "view_list"); // set cookie
         $("#listing_view").animate({opacity: "0"}, 0);// set class         
         $("#listing_view").delay(200).animate({opacity: "1"}, 300);// set class    
    });
             
    /* add/remove product to/from favorite */


    /*  CATEGORY PAGE       */
    //$("#product_list .product_like a").click(function(){        
    $(".registered #product_list .product_like a").live('click', function() {
        if ($(this).hasClass("addfav")) { 
            var fn = "add";
            var fav_url = favorite_products_url_add
        } else { 
            var fn = "rem";
            var fav_url = favorite_products_url_remove
        };
        var fav_pid = $(this).closest('.ajax_block_product').data("productid"); // get data    
        $.ajax({
            url: fav_url,
            type: "POST",
            data: {
              "id_product": fav_pid
            },
            success: function(result){
              if (result == '0')
              {
                var num = parseInt($(".favQty").text());
                if (fn == "add") {
                    $("#product_list").find("[data-productid='" + fav_pid + "'] .product_like").addClass("active");
                    $(".favQty").text(num+1);
                    $(".favoritelist").find("li.no-products").remove();
                    $(".favoritelist").css({"height":"auto"});
                    addFavoriteProduct('.p'+fav_pid+' .image-cover');
                } else if (fn == "rem") {
                    $("#product_list").find("[data-productid='" + fav_pid + "'] .product_like").removeClass("active");
                    $(".favQty").text(num-1);
                }                
              }
            }
        });
    });
    /*      PRODUCT PAGE  ADD TO FAVORITES    */
    $(".registered#product .product_like a").click(function(){        
        if ($(this).hasClass("addfav")) { 
            var fn = "add";
            var fav_url = favorite_products_url_add
        } else { 
            var fn = "rem";
            var fav_url = favorite_products_url_remove
        };        
        $.ajax({
            url: fav_url,
            type: "POST",
            data: {
              "id_product": id_product
            },
            success: function(result){
              if (result == '0')
              {
                var num = parseInt($(".favQty").text());
                if (fn == "add") {
                    $("#product .product_like").addClass("active");
                    $(".favQty").text(num+1);
                    $(".favoritelist li").each(function( index ) {
                        if ($(this).hasClass("no-products")) {
                            $(this).remove();                            
                        }
                    });
                    $(".favoritelist").css({"height":"auto"});
                    addFavoriteProduct("#bigpic");
                } else if (fn == "rem") {
                    $("#product .product_like").removeClass("active");
                    $(".favQty").text(num-1);
                }
              }
            }
        });
        return false;
    });
    $(".guest .product_like a").live('click', function() {
        $.prompt("You should be logined to add products to favorites");
        return false;
    });
    function addFavoriteProduct(e) {
        var $element = $(e);
        if (!$element.length)
            var $element = $('.product_like');
        var $picture = $element.clone();
        var pictureOffsetOriginal = $element.offset();
        $picture.css({'position': 'absolute', 'top': pictureOffsetOriginal.top, 'left': pictureOffsetOriginal.left});
        var pictureOffset = $picture.offset();
        //$('.favoritelist').stop().slideDown(500, 'easeOutQuint');
        var BlockOffset = $('.pk_favorites').offset();

        $picture.appendTo('body');
        $picture.css({ 'position': 'absolute', 'top': $picture.css('top'), 'left': $picture.css('left'), 'z-index': 4242 }).removeAttr("height").animate({ 'opacity':1, 'width': 14, 'height':'auto', 'top': BlockOffset.top, 'left': BlockOffset.left }, 1500, 'easeInOutSine').addClass('animatedImage')
            .fadeOut(400, function() {
                //ajaxCart.updateCartInformation(jsonData, addedFromProductPage);
                //displayNewProducts(data);
        });
            //console.log($picture);
        var content = '<li class="favoriteproduct clearfix"><a href="#" class="favProdImage"><img src="'+$picture[0].src+'" alt=""/></a><div class="text_desc"><a href="#">'+$picture[0].alt+'</a></div></li>';
        $('.favoritelist ul').append(content);
    }
    // wishlist button    
    $(".registered #wishlist_button").click(function(){
        if (!$(this).hasClass("active")) { 
            $(this).addClass("active");
        }
    });     

    /* order page tabs */
    $(".shipping-and-taxes-tab").click(function(){    
        $('.additional-cart-tabs div').removeClass('active');
        $(this).addClass("active");        
        $(".shipping-and-taxed-content").show('fast');
        $(".vouchers-content").hide('fast');
    });
    $(".vouchers-tab").click(function(){    
        $('.additional-cart-tabs div').removeClass('active');
        $(this).addClass("active");        
        $(".shipping-and-taxed-content").hide('fast');
        $(".vouchers-content").show('fast');
    });

    /* product page*/
    $(".tab-titles h3").click(function(){
        $(".tab-titles h3").removeClass("active-tab");
        $(this).addClass("active-tab");
        var num = $(this).data("title");
        $("#pb-left-column section").removeClass("active-section");
        $("#pb-left-column").find("[data-section='" + num + "']").addClass("active-section");
    });
    /* end product page*/

    //ACCORDION BUTTON ACTION (ON CLICK DO THE FOLLOWING)
    function pk_accordion() {
        $( ".more_info_block_mobile .accordionButton, .carouselMobile .accordionButton" ).unbind( "click" );
        $('.more_info_block_mobile .accordionButton, .carouselMobile .accordionButton').click(function() {
            $('.carouselMobile .forStart .accordionContent').hide();
            $('#productsCarousel .wht-bg').removeClass('forStart');            
            //REMOVE THE ON CLASS FROM ALL BUTTONS
            $('.more_info_block_mobile .accordionButton, .carouselMobile .accordionButton').removeClass('on');          
            //NO MATTER WHAT WE CLOSE ALL OPEN SLIDES
            $('.more_info_block_mobile .accordionContent, .carouselMobile .accordionContent').slideUp('normal').removeClass('activeContent');            
            //IF THE NEXT SLIDE WASN'T OPEN THEN OPEN IT
            if($(this).next().is(':hidden') == true) {
                //ADD THE ON CLASS TO THE BUTTON
                $(this).addClass('on');
                //OPEN THE SLIDE
                $(this).next().slideDown('normal').addClass('activeContent');
             } 
         });
        /*** REMOVE IF MOUSEOVER IS NOT REQUIRED ***/
        //ADDS THE .OVER CLASS FROM THE STYLESHEET ON MOUSEOVER 
        $('.more_info_block_mobile .accordionButton, .carouselMobile .accordionButton').mouseover(function() {
            $(this).addClass('over');
        //ON MOUSEOUT REMOVE THE OVER CLASS
        }).mouseout(function() {
            $(this).removeClass('over');                                        
        });
        /*** END REMOVE IF MOUSEOVER IS NOT REQUIRED ***/    
        /********************************************************************************************************************
        CLOSES ALL S ON PAGE LOAD
        ********************************************************************************************************************/   
        $('.more_info_block_mobile .accordionContent').hide();
    }

    //style-my-tootltips by malihu (http://manos.malihu.gr)
    //plugin home http://manos.malihu.gr/style-my-tooltips-jquery-plugin  
     $.fn.style_my_tooltips = function(options) {  
        var defaults = {  
            tip_follows_cursor: "on", 
            tip_delay_time: 200
        };
        var options = $.extend(defaults, options);
        $("body").append("<div id='s-m-t-tooltip'></div>"); //create the tooltip container
        smtTip=$("#s-m-t-tooltip"); 
        smtTip.hide(); //hide it
        smtTip_delay = 0;
        return this.each(function() {  
            function smtMouseMove(e){
                smtMouseCoordsX=e.pageX;
                smtMouseCoordsY=e.pageY;
                smtTipPosition();
            }
            function smtTipPosition(){
                var cursor_tip_margin_x=0; //horizontal space between the cursor and tooltip
                var cursor_tip_margin_y=24; //vertical space between the cursor and tooltip
                var leftOffset=smtMouseCoordsX+cursor_tip_margin_x+$(smtTip).outerWidth();
                var topOffset=smtMouseCoordsY+cursor_tip_margin_y+$(smtTip).outerHeight();
                if(leftOffset<=$(window).width()){
                    smtTip.css("left",smtMouseCoordsX+cursor_tip_margin_x);
                } else {
                    var thePosX=smtMouseCoordsX-(cursor_tip_margin_x)-$(smtTip).width();
                    smtTip.css("left",thePosX);
                }
                if(topOffset<=$(window).height()){
                    smtTip.css("top",smtMouseCoordsY+cursor_tip_margin_y);
                } else {
                    var thePosY=smtMouseCoordsY-(cursor_tip_margin_y)-$(smtTip).height();
                    smtTip.css("top",thePosY);
                }
            }
            $(this).hover(function(e) {  
                // mouseover
                var $this=$(this);          
                $this.data("smtTitle",$this.attr("title")); //store title 
                smtTip_delay = setInterval(0, 50); //set tooltip delay
                var theTitle=$this.data("smtTitle");            
                if ((theTitle).length > 0) {
                    $this.attr("title",""); //remove title to prevent native tooltip showing
                    smtTip.empty().append(theTitle).hide(); //set tooltip text and hide it              
                    smtTip_delay = setInterval(smtTip_fadeIn, options.tip_delay_time); //set tooltip delay
                    if(options.tip_follows_cursor=="off"){
                        smtMouseMove(e);
                    } else {
                        $(document).bind("mousemove", function(event){
                            smtMouseMove(event); 
                        });
                    }
                }
            }, function() {  
                // mouseout
                var $this=$(this);
                if(options.tip_follows_cursor!="off"){
                    $(document).unbind("mousemove");
                }
                clearInterval(smtTip_delay);
                if(smtTip.is(":animated")){ 
                    smtTip.hide();
                } else {
                    smtTip.fadeOut("fast");
                }
                $this.attr("title",$this.data("smtTitle")); //add back title
            });
            function smtTip_fadeIn(){
                smtTip.fadeIn("fast");
            }

        });     
     };
    /**
     * jQuery Cookie plugin
     *
     * Copyright (c) 2010 Klaus Hartl (stilbuero.de)
     * Dual licensed under the MIT and GPL licenses:
     * http://www.opensource.org/licenses/mit-license.php
     * http://www.gnu.org/licenses/gpl.html
     *
     */
    jQuery.cookie = function (key, value, options) {

        // key and at least value given, set cookie...
        if (arguments.length > 1 && String(value) !== "[object Object]") {
            options = jQuery.extend({}, options);

            if (value === null || value === undefined) {
                options.expires = -1;
            }

            if (typeof options.expires === 'number') {
                var days = options.expires, t = options.expires = new Date();
                t.setDate(t.getDate() + days);
            }

            value = String(value);

            return (document.cookie = [
                encodeURIComponent(key), '=',
                options.raw ? value : encodeURIComponent(value),
                options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
                options.path ? '; path=' + options.path : '',
                options.domain ? '; domain=' + options.domain : '',
                options.secure ? '; secure' : ''
            ].join(''));
        }

        // key and possibly options given, get cookie...
        options = value || {};
        var result, decode = options.raw ? function (s) { return s; } : decodeURIComponent;
        return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? decode(result[1]) : null;
    };

    // theme settings panel

    $(function() {

        var current     = 1;
        var stepsWidth  = 0;
        var widths      = new Array();
        $('#identifier .section').each(function(i){
            var $step       = $(this);
            widths[i]       = stepsWidth;
            stepsWidth      += $step.width();
        });
        $('#identifier').width(stepsWidth);
        var sectHeight = $("ul").find("[data-cid='1']").height();
        $(".ts-wrapper").animate({ height: sectHeight }, 300);

        $('#navigation a').bind('click',function(e){
            var $this   = $(this);
            titleID = $this.data("sid");
            var sectHeight = $("ul").find("[data-cid='" + titleID + "']").height();
            $(".ts-wrapper").delay(500).animate({ height: sectHeight }, 300);
            var prev    = current;
            $this.closest('ul').find('li').removeClass('selected');
            $this.parent().addClass('selected');

            current = $this.parent().index() + 1;

            $('#identifier').stop().animate({
                marginLeft: '-' + widths[current-1] + 'px'
            },500);
            e.preventDefault();
        });
            
    });

});

function DropDown(el) {
    this.dd = el;
    this.placeholder = this.dd.children('span');
    this.opts = this.dd.find('ul.dropdown > li');
    this.val = '';
    this.index = -1;
    this.initEvents();
}
DropDown.prototype = {
    initEvents : function() {
        var obj = this;

        obj.dd.on('click', function(event){
            $(this).toggleClass('active');
            return false;
        });

        obj.opts.on('click',function(){
            var opt = $(this);
            obj.val = opt.text();
            obj.index = opt.index();
            obj.placeholder.text(obj.val);
        });
    },
    getValue : function() {
        return this.val;
    },
    getIndex : function() {
        return this.index;
    }
}