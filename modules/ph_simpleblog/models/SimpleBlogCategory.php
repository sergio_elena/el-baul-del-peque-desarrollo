<?php
class SimpleBlogCategory extends ObjectModel
{
    public $id;

    public $id_shop;

    public $id_simpleblog_category;

    public $name;

    public $active = 1;

    public $description;

    public $link_rewrite;

    public $meta_title;

    public $meta_keywords;

    public $meta_description;

    public $date_add;

    public $date_upd;

    protected static $_links = array();

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'simpleblog_category',
        'primary' => 'id_simpleblog_category',
        'multilang' => true,
        //'multilang_shop' => true,
        'fields' => array(
            'active' =>             array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
            'date_add' =>           array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'date_upd' =>           array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            //'id_shop_default' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),

            // Lang fields
            'name' =>               array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCatalogName', 'required' => true, 'size' => 64),
            'link_rewrite' =>       array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isLinkRewrite', 'required' => true, 'size' => 64),
            'description' =>        array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml'),
            'meta_title' =>         array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 128),
            'meta_description' =>   array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 255),
            'meta_keywords' =>      array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 255),
        ),
    );

    public  function add($autodate = true, $null_values = false)
    {
        foreach ($this->name as $k => $value)
            if (preg_match('/^[1-9]\./', $value))
                $this->name[$k] = '0'.$value;
        $ret = parent::add($autodate, $null_values);
        return $ret;
    }

    public  function update($null_values = false)
    {
        foreach ($this->name as $k => $value)
            if (preg_match('/^[1-9]\./', $value))
                $this->name[$k] = '0'.$value;
        return parent::update($null_values);
    }

    /**
      * Return available categories
      *
      * @param integer $id_lang Language ID
      * @param boolean $active return only active categories
      * @return array Categories
      */
    public static function getCategories($id_lang, $active = true)
    {
        if (!Validate::isBool($active))
            die(Tools::displayError());
        
        $context = Context::getContext();

        $sql = '
        SELECT *
        FROM `'._DB_PREFIX_.'simpleblog_category` c
         '.Shop::addSqlAssociation('simpleblog_category', 'c').'
        LEFT JOIN `'._DB_PREFIX_.'simpleblog_category_lang` cl ON c.`id_simpleblog_category` = cl.`id_simpleblog_category`
        WHERE `id_lang` = '.(int)$id_lang.'
        '.($active ? 'AND `active` = 1' : '').'
        ORDER BY `name` ASC';

        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

        $categories = array();

        foreach ($result as $row)
        {
            $categories[$row['id_simpleblog_category']]['name'] = $row['name'];
            $categories[$row['id_simpleblog_category']]['url'] = self::getLink($row['link_rewrite'], $id_lang);
            $categories[$row['id_simpleblog_category']]['id'] = $row['id_simpleblog_category'];
        }
            
        return $categories;
    }

    public static function getSimpleCategories($id_lang)
    {
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
        SELECT c.`id_simpleblog_category`, cl.`name`
        FROM `'._DB_PREFIX_.'simpleblog_category` c
        LEFT JOIN `'._DB_PREFIX_.'simpleblog_category_lang` cl ON (c.`id_simpleblog_category` = cl.`id_simpleblog_category`)
        WHERE cl.`id_lang` = '.(int)$id_lang.'
        ORDER BY cl.`name`');
    }

    public static function getByRewrite($rewrite = null, $id_lang = false)
    {
        if(!$rewrite) return;

        $sql = new DbQuery();
        $sql->select('l.id_simpleblog_category');
        $sql->from('simpleblog_category_lang', 'l');

        if($id_lang)
            $sql->where('l.link_rewrite = \''.$rewrite.'\' AND l.id_lang = '.(int)$id_lang);
        else
            $sql->where('l.link_rewrite = \''.$rewrite.'\'');

        $category = new SimpleBlogCategory(Db::getInstance()->getValue($sql), $id_lang);
        return $category;
    }

    public static function getRewriteByCategory($id_simpleblog_category, $id_lang)
    {
        $sql = new DbQuery();
        $sql->select('l.link_rewrite');
        $sql->from('simpleblog_category_lang', 'l');
        $sql->where('l.id_simpleblog_category = '.$id_simpleblog_category.' AND l.id_lang = '.(int)$id_lang);

        return Db::getInstance()->getValue($sql);
    }

    public static function getLink($rewrite, $id_lang = null, $id_shop = null)
    {
        $url = ph_simpleblog::myRealUrl();

        $dispatcher = Dispatcher::getInstance();
        $params = array();
        $params['sb_category'] = $rewrite;
        return $url.$dispatcher->createUrl('module-ph_simpleblog-category', $id_lang, $params);
    }

}


