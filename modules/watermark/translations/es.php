<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{watermark}prestashop>watermark_ee20bb60493f049175fc10c35acd2272'] = 'Marca de agua';
$_MODULE['<{watermark}prestashop>watermark_fa214007826415a21a8456e3e09f999d'] = '¿Está seguro de que desea eliminar sus datos?';
$_MODULE['<{watermark}prestashop>watermark_e7bbd7019d9f73bc63f406de1422d989'] = 'La imagen de la Marca de agua (filigrana) tiene que ser cargada para que este módulo funcione correctamente.';
$_MODULE['<{watermark}prestashop>watermark_d31c2d99614d8e16430d2d8c99c1f2b0'] = 'Se necesita alineación Y.';
$_MODULE['<{watermark}prestashop>watermark_8fe9c39f4bf87082caabcf3650970c71'] = 'Alineación Y no está en el rango permitido.';
$_MODULE['<{watermark}prestashop>watermark_c2cf8e33c907a4cc689881dc8fa571c2'] = 'Se necesita alineación X.';
$_MODULE['<{watermark}prestashop>watermark_3a1f788dbe8957be92048606cf0d3fcb'] = 'Alineación X está en el rango permitido.';
$_MODULE['<{watermark}prestashop>watermark_a9cac4be0fa0b815376b96f49e1435d7'] = 'Se requiere al menos un tipo de imagen.';
$_MODULE['<{watermark}prestashop>watermark_130aab6764f25267c79cef371270eb2a'] = 'Ocurrió un error al cargar marca de agua: %1$s en %2$s';
$_MODULE['<{watermark}prestashop>watermark_cc99ba657a4a5ecf9d2d7cb974d25596'] = 'Una vez que haya ajustado el módulo, tiene que regenerar las imágenes con la herramienta \"Imágenes\" en Preferencias. Sin embargo, la filigrana se añadirá automáticamente a las nuevas imágenes.';
