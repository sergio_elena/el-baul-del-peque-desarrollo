<?php
/**
 * MagniGenie Prestashop Module.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * 
 * @author    MagniGenie <info@magnigenie.com>
 * @category  Front Office Features
 * @package   Auto Complete Search Module
 * @copyright Copyright (c)  2014  (http://magnigenie.com)
 * @license   http://magnigenie.com
 */

if (!defined('_PS_VERSION_'))
	exit;

class AutoCompleteSearch extends Module
{
	public function __construct()
	{
		$this->name = 'autocompletesearch';
		$this->tab = 'front_office_features';
		$this->version = '1.2';
		$this->author = 'Magnigenie';
		$this->need_instance = 0;
		$this->bootstrap = true;
		parent::__construct();
		$this->displayName = $this->l('Auto Complete Search');
		$this->description = $this->l('This module will help users to search products,category and manufacturers
			in different way');
		$this->confirmUninstall = $this->l('Are you sure you want to uninstall Auto Complete Search module?');
	}

	public function install()
	{
		if (!parent::install() || !$this->registerHook('top') || !$this->registerHook('header')
				|| !$this->registerHook('displayHome') || !$this->registerHook('displayMobileTopSiteMap') 
				|| !Configuration::updateValue('enable_autocomplete_search', '0')
				|| !Configuration::updateValue('search_class_id', '#search_query_top')
				|| !Configuration::updateValue('result_height', '200')
				|| !Configuration::updateValue('show_product_search', '1')
				|| !Configuration::updateValue('minimum_characters', '3')
				|| !Configuration::updateValue('number_products_search', '4')
				|| !Configuration::updateValue('matching_products_heading', 'Matching Products Found')
				|| !Configuration::updateValue('products_error_message', 'Sorry No Products Found')
				|| !Configuration::updateValue('show_addtocart', '1')
				|| !Configuration::updateValue('show_price', '1')
				|| !Configuration::updateValue('show_view_button', '1')
				|| !Configuration::updateValue('show_description', '1')
				|| !Configuration::updateValue('show_view_all', '0')
				|| !Configuration::updateValue('product_quantity', '0')
				|| !Configuration::updateValue('show_category_search', '0')
				|| !Configuration::updateValue('number_of_category', '4')
				|| !Configuration::updateValue('matching_category_heading', 'Matching Category Found')
				|| !Configuration::updateValue('category_error_message', 'Sorry No Category Found')
				|| !Configuration::updateValue('show_manufacturer_search', '0')
				|| !Configuration::updateValue('number_of_manufacturer', '4')
				|| !Configuration::updateValue('matching_manufacturer_heading', 'Matching Manufacturer Found')
				|| !Configuration::updateValue('manufacturer_error_message', 'Sorry No Matching Manufacturer Found')
				|| !Configuration::updateValue('preloader_right_position', '285')
				|| !Configuration::updateValue('preloader_top_position', '9')
				|| !Configuration::updateValue('search_results_top_position', '45')
				|| !Configuration::updateValue('search_results_left_position', '0')
				|| !Configuration::updateValue('autocomplete_background_color', '#FFFFFF')
				|| !Configuration::updateValue('item_heading_color', '#002821')
				|| !Configuration::updateValue('item_description_color', '#777777')
				|| !Configuration::updateValue('product_price_color', '#228700')
				|| !Configuration::updateValue('product_old_price_color', '#c31f00')
				|| !Configuration::updateValue('item_matching_color', '#777777')
				|| !Configuration::updateValue('item_not_found_message_color', '#583f00'))
			return false;
			return true;
	}


	public function uninstall()
	{
		if (!parent::uninstall())
			return false;
			return true;
	}


	public function getContent()
	{
		$this->_html = '';
		//Autocomplete values
		$enable_autocomplete_search = Tools::getValue('enable_autocomplete_search');
		$search_class_id = Tools::getValue('search_class_id');
		$result_height = Tools::getValue('result_height');
		$show_product_search = Tools::getValue('show_product_search');		
		$number_products_search = Tools::getValue('number_products_search');
		$minimum_characters = Tools::getValue('minimum_characters');
		$show_addtocart = Tools::getValue('show_addtocart');
		$show_price = Tools::getValue('show_price');
		$product_quantity = Tools::getValue('product_quantity');
		$matching_products_heading = Tools::getValue('matching_products_heading');
		$products_error_message = Tools::getValue('products_error_message');
		$show_view_button = Tools::getValue('show_view_button');
		$show_description = Tools::getValue('show_description');
		$show_category_search = Tools::getValue('show_category_search');
		$number_of_category = Tools::getValue('number_of_category');
		$show_manufacturer_search = Tools::getValue('show_manufacturer_search');
		$number_of_manufacturer = Tools::getValue('number_of_manufacturer');
		$manufacturer_heading = Tools::getValue('matching_manufacturer_heading');
		$manufacturer_error_message = Tools::getValue('manufacturer_error_message');

		//Autocomplete appearance settings
		$preloader_right_position = Tools::getValue('preloader_right_position');
		$preloader_top_position = Tools::getValue('preloader_top_position');
		$search_results_top_position = Tools::getValue('preloader_top_position');
		$search_results_left_position = Tools::getValue('preloader_top_position');
		$autocomplete_background_color = Tools::getValue('autocomplete_background_color');
		$item_heading_color = Tools::getValue('item_heading_color');
		$item_description_color = Tools::getValue('item_description_color');
		$product_price_color = Tools::getValue('product_price_color');
		$product_old_price_color = Tools::getValue('product_old_price_color');
		$item_matching_color = Tools::getValue('item_matching_color');
		$item_not_found_message_color = Tools::getValue('item_not_found_message_color');

		if (Tools::isSubmit('save_autocomplete_settings'))
		{
			Configuration::updateValue('enable_autocomplete_search', Tools::getValue('enable_autocomplete_search'));
			Configuration::updateValue('search_class_id', Tools::getValue('search_class_id'));
			Configuration::updateValue('result_height', Tools::getValue('result_height'));
			Configuration::updateValue('show_product_search', Tools::getValue('show_product_search'));
			Configuration::updateValue('minimum_characters', Tools::getValue('minimum_characters'));
			Configuration::updateValue('number_products_search', Tools::getValue('number_products_search'));
			Configuration::updateValue('show_addtocart', Tools::getValue('show_addtocart'));
			Configuration::updateValue('product_quantity', Tools::getValue('product_quantity'));
			Configuration::updateValue('matching_products_heading', Tools::getValue('matching_products_heading'));
			Configuration::updateValue('show_price', Tools::getValue('show_price'));
			Configuration::updateValue('show_view_button', Tools::getValue('show_view_button'));
			Configuration::updateValue('show_description', Tools::getValue('show_description'));
			Configuration::updateValue('show_category_search', Tools::getValue('show_category_search'));
			Configuration::updateValue('number_of_category', Tools::getValue('number_of_category'));
			Configuration::updateValue('matching_category_heading', Tools::getValue('matching_category_heading'));
			Configuration::updateValue('category_error_message', Tools::getValue('category_error_message'));
			Configuration::updateValue('show_manufacturer_search', Tools::getValue('show_manufacturer_search'));
			Configuration::updateValue('number_of_manufacturer', Tools::getValue('number_of_manufacturer'));	
			Configuration::updateValue('matching_manufacturer_heading', Tools::getValue('matching_manufacturer_heading'));
			Configuration::updateValue('manufacturer_error_message', Tools::getValue('manufacturer_error_message'));	
		}

		if (Tools::isSubmit('save_autocomplete_appearance__settings'))
		{
			Configuration::updateValue('preloader_right_position', Tools::getValue('preloader_right_position'));
			Configuration::updateValue('preloader_top_position', Tools::getValue('preloader_top_position'));
			Configuration::updateValue('search_results_top_position', Tools::getValue('search_results_top_position'));
			Configuration::updateValue('search_results_left_position', Tools::getValue('search_results_left_position'));
			Configuration::updateValue('autocomplete_background_color', Tools::getValue('autocomplete_background_color'));
			Configuration::updateValue('item_heading_color', Tools::getValue('item_heading_color'));
			Configuration::updateValue('item_description_color', Tools::getValue('item_description_color'));
			Configuration::updateValue('product_price_color', Tools::getValue('product_price_color'));
			Configuration::updateValue('product_old_price_color', Tools::getValue('product_old_price_color'));
			Configuration::updateValue('item_matching_color', Tools::getValue('item_matching_color'));
			Configuration::updateValue('item_not_found_message_color', Tools::getValue('item_not_found_message_color'));
		}

		$this->_html .= $this->renderForm();
		return $this->_html;
	}

	protected function renderForm()
	{
		$autocomplete_search_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Autocomplete Search Settings'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => (_PS_VERSION_ > 1.5) ? 'switch' : 'radio',
						'label' => $this->l('Enable Auto Complete Search'),
						'name' => 'enable_autocomplete_search',
						'class' => 'hello',
						'is_bool' => true,
						'desc' => $this->l('Want to use Auto Complete Search in store?'),
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => true,
								'label' => $this->l('Enabled')
							),
							array(
								'id' => 'active_off',
								'value' => false,
								'label' => $this->l('Disabled')
							)
						),
					),
					array(
						'col' => 4,
						'type' => 'text',
						'label' => $this->l('Enter class or id name'),
						'suffix' => 'Class or Id Name',
						'name' => 'search_class_id',
						'desc' => $this->l('Enter the class name or id name followed by dot (.) or hash (#) '),
					),
					array(
						'col' => 4,
						'type' => 'text',
						'label' => $this->l('Max Height For the box'),
						'suffix' => 'height in px',
						'name' => 'result_height',
						'desc' => $this->l('The value entered will be the height for search result box, Enter 0 for auto height'),
					),										
					array(
						'type' => (_PS_VERSION_ > 1.5) ? 'switch' : 'radio',
						'label' => $this->l('Show Product In Search'),
						'name' => 'show_product_search',
						'is_bool' => true,
						'desc' => $this->l('Want to show product names in search?'),
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => true,
								'label' => $this->l('Enabled')
							),
							array(
								'id' => 'active_off',
								'value' => false,
								'label' => $this->l('Disabled')
							)
						),
					),
					array(
						'col' => 4,
						'type' => 'text',
						'label' => $this->l('Minimum Characters'),
						'name' => 'minimum_characters',
						'suffix' => 'Minimum Characters',
						'desc' => $this->l('Minimum number of characters required to trigger autosuggest. Default: 1.'),
					),						
					array(
						'col' => 4,
						'type' => 'text',
						'label' => $this->l('Show number of products in search'),
						'suffix' => 'Number of Products',
						'name' => 'number_products_search',
						'desc' => $this->l('Enter the number of products that you want to show in search. If the field
							is left blank then all the products within the matching letters will be shown'),
					),
					array(
						'col' => 4,
						'type' => 'text',
						'label' => $this->l('Heading for matching products block'),
						'name' => 'matching_products_heading',
						'desc' => $this->l('This will be the text for the matching products heading'),
					),
					array(
						'col' => 4,
						'type' => 'text',
						'label' => $this->l('Products not found message'),
						'name' => 'products_error_message',
						'desc' => $this->l('This will be the message when no matching products found'),
					),										
					array(
						'type' => (_PS_VERSION_ > 1.5) ? 'switch' : 'radio',
						'label' => $this->l('Show Add to Cart'),
						'name' => 'show_addtocart',
						'is_bool' => true,
						'desc' => $this->l('Want to show Add to Cart in search?'),
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => true,
								'label' => $this->l('Enabled')
							),
							array(
								'id' => 'active_off',
								'value' => false,
								'label' => $this->l('Disabled')
							)
						),
					),
					array(
						'type' => (_PS_VERSION_ > 1.5) ? 'switch' : 'radio',
						'label' => $this->l('Show Price'),
						'name' => 'show_price',
						'is_bool' => true,
						'desc' => $this->l('Want to show Price for the Product?'),
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => true,
								'label' => $this->l('Enabled')
							),
							array(
								'id' => 'active_off',
								'value' => false,
								'label' => $this->l('Disabled')
							)
						),
					),
					array(
						'type' => (_PS_VERSION_ > 1.5) ? 'switch' : 'radio',
						'label' => $this->l('Show View Button'),
						'name' => 'show_view_button',
						'is_bool' => true,
						'desc' => $this->l('Want to show View button for the Product?'),
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => true,
								'label' => $this->l('Enabled')
							),
							array(
								'id' => 'active_off',
								'value' => false,
								'label' => $this->l('Disabled')
							)
						),
					),
					array(
						'type' => (_PS_VERSION_ > 1.5) ? 'switch' : 'radio',
						'label' => $this->l('Show Description'),
						'name' => 'show_description',
						'is_bool' => true,
						'desc' => $this->l('Want to show description for the Product?'),
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => true,
								'label' => $this->l('Enabled')
							),
							array(
								'id' => 'active_off',
								'value' => false,
								'label' => $this->l('Disabled')
							)
						),
					),
					array(
						'type' => (_PS_VERSION_ > 1.5) ? 'switch' : 'radio',
						'label' => $this->l('Show product quantity'),
						'name' => 'product_quantity',
						'is_bool' => true,
						'desc' => $this->l('This will be the input field where a customer will add amount of product to his cart'),
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => true,
								'label' => $this->l('Enabled')
							),
							array(
								'id' => 'active_off',
								'value' => false,
								'label' => $this->l('Disabled')
							)
						),
					),																															
					array(
						'type' => (_PS_VERSION_ > 1.5) ? 'switch' : 'radio',
						'label' => $this->l('Show Category In Search'),
						'name' => 'show_category_search',
						'is_bool' => true,
						'desc' => $this->l('Want to show category names in search?'),
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => true,
								'label' => $this->l('Enabled')
							),
							array(
								'id' => 'active_off',
								'value' => false,
								'label' => $this->l('Disabled')
							)
						),
					),
					array(
						'col' => 4,
						'type' => 'text',
						'label' => $this->l('Show number of category in search'),
						'name' => 'number_of_category',
						'suffix' => 'Number of Category',
						'desc' => $this->l('Enter the number of category that you want to show in search. If the field
							is left blank then all the category within the matching letters will be shown'),
					),
					array(
						'col' => 4,
						'type' => 'text',
						'label' => $this->l('Heading for matching category block'),
						'name' => 'matching_category_heading',
						'desc' => $this->l('This will be the text for the matching category heading'),
					),
					array(
						'col' => 4,
						'type' => 'text',
						'label' => $this->l('Category not found message'),
						'name' => 'category_error_message',
						'desc' => $this->l('This will be the message when no matching category found'),
					),															
					array(
						'type' => 'switch',
						'label' => $this->l('Show Manufacturer In Search'),
						'name' => 'show_manufacturer_search',
						'is_bool' => true,
						'desc' => $this->l('Want to show manufacturer names in search?'),
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => true,
								'label' => $this->l('Enabled')
							),
							array(
								'id' => 'active_off',
								'value' => false,
								'label' => $this->l('Disabled')
							)
						),
					),
					array(
						'col' => 4,
						'type' => 'text',
						'label' => $this->l('Show number of manufacturer in search'),
						'suffix' => 'Number of Manufacturer',
						'name' => 'number_of_manufacturer',
						'desc' => $this->l('Enter the number of manufacturer that you want to show in search. If the field
							is left blank then all the manufacturer within the matching letters will be shown'),
					),
					array(
						'col' => 4,
						'type' => 'text',
						'label' => $this->l('Heading for matching manufacturer block'),
						'name' => 'matching_manufacturer_heading',
						'desc' => $this->l('This will be the text for the matching manufacturer heading'),
					),
					array(
						'col' => 4,
						'type' => 'text',
						'label' => $this->l('Manufacturer not found message'),
						'name' => 'manufacturer_error_message',
						'desc' => $this->l('This will be the message when no matching category found'),
					),																														
				),
			'submit' => array(
				'title' => $this->l('Save Settings'),
				'class' => 'button pull-right',
				'name' => 'save_autocomplete_settings',
			)
		),
		);
		
		$autocomplete_appearance_settings = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Autocomplete Appearance Settings'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'col' => 2,
						'suffix' => 'in px',
						'type' => 'text',
						'label' => $this->l( 'Preloader position from right' ),
						'name' => 'preloader_right_position',
						'desc' => $this->l( 'Enter pixel from the right where the preloader will be shown' ),
					),
					array(
						'col' => 2,
						'suffix' => 'in px',
						'type' => 'text',
						'label' => $this->l( 'Preloader position from top' ),
						'name' => 'preloader_top_position',
						'desc' => $this->l( 'Enter pixel from the top where the preloader will be shown' ),
					),
					array(
						'col' => 2,
						'suffix' => 'in px',
						'type' => 'text',
						'label' => $this->l( 'Seach results position from top' ),
						'name' => 'search_results_top_position',
						'desc' => $this->l( 'Enter pixel from the top where the search results will be shown' ),
					),
					array(
						'col' => 2,
						'suffix' => 'in px',
						'type' => 'text',
						'label' => $this->l( 'Seach results position from left' ),
						'name' => 'search_results_left_position',
						'desc' => $this->l( 'Enter pixel from the left where the search results will be shown' ),
					),					
					array(
						'type' => 'color',
						'label' => $this->l( 'AutoComplete Search Background Color' ),
						'value' => 'cccccc',
						'name' => 'autocomplete_background_color',
						'desc' => $this->l( 'Select a background color that you want to use in AutoComplete search result background color.' ),
					),
					array(
						'type' => 'color',
						'label' => $this->l( 'Item heading color' ),
						'value' => '777777',
						'name' => 'item_heading_color',
						'desc' => $this->l( 'Select a color that you want as a color for the item heading' ),
					),
					array(
						'type' => 'color',
						'label' => $this->l( 'Item description color' ),
						'value' => '777777',
						'name' => 'item_description_color',
						'desc' => $this->l( 'Select a color that you want as a color for the item description text' ),
					),
					array(
						'type' => 'color',
						'label' => $this->l( 'Product price color' ),
						'value' => '777777',
						'name' => 'product_price_color',
						'desc' => $this->l( 'Select a color that you want as a color for the product price' ),
					),
					array(
						'type' => 'color',
						'label' => $this->l( 'Product old price color' ),
						'value' => '777777',
						'name' => 'product_old_price_color',
						'desc' => $this->l( 'Select a color that you want as a color for the product old price' ),
					),															
					array(
						'type' => 'color',
						'label' => $this->l( 'Items matching text color' ),
						'value' => '777777',
						'name' => 'item_matching_color',
						'desc' => $this->l( 'Select a color that you want as a color for the items matching heading' ),
					),
					array(
						'type' => 'color',
						'label' => $this->l( 'Items not found message color' ),
						'value' => '777777',
						'name' => 'item_not_found_message_color',
						'desc' => $this->l( 'Select a color that you want as a color for the items not found message color' ),
					),																		
				),
			'submit' => array(
				'title' => $this->l('Save Settings'),
				'class' => 'button pull-right',
				'name' => 'save_autocomplete_appearance__settings',
			)
		),
		);

		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();
		$helper->submit_action = 'submit';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).
			'&configure='.$this->name.'&tab_module='.$this->tab.
			'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');

		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);

		return $helper->generateForm(array($autocomplete_search_form, $autocomplete_appearance_settings));
	}

	public function getConfigFieldsValues()
	{
		return array(
			// Autocomplete settings
			'enable_autocomplete_search' => Tools::getValue('enable_autocomplete_search', Configuration::get('enable_autocomplete_search')),
			'search_class_id' => Tools::getValue('search_class_id', Configuration::get('search_class_id')),
			'result_height' =>  Tools::getValue('result_height', Configuration::get('result_height')),
			'show_product_search' => Tools::getValue('show_product_search', Configuration::get('show_product_search')),
			'minimum_characters' => Tools::getValue('minimum_characters', Configuration::get('minimum_characters')),
			'number_products_search' => Tools::getValue('number_products_search', Configuration::get('number_products_search')),
			'show_addtocart' => Tools::getValue('show_addtocart', Configuration::get('show_addtocart')),
			'show_description' => Tools::getValue('show_description', Configuration::get('show_description')),
			'matching_products_heading' => Tools::getValue('matching_products_heading', Configuration::get('matching_products_heading')),
			'products_error_message' => Tools::getValue('products_error_message', Configuration::get('products_error_message')),
			'show_view_button' => Tools::getValue('show_view_button', Configuration::get(' show_view_button')),			
			'show_price' => Tools::getValue('show_price', Configuration::get('show_price')),
			'show_view_all' => Tools::getValue('show_view_all', Configuration::get('show_view_all')),
			'product_quantity' => Tools::getValue('product_quantity', Configuration::get('product_quantity')),
			'show_category_search' => Tools::getValue('show_category_search', Configuration::get('show_category_search')),			
			'number_of_category' => Tools::getValue('number_of_category', Configuration::get('number_of_category')),
			'matching_category_heading' => Tools::getValue('matching_category_heading', Configuration::get('matching_category_heading')),
			'category_error_message' => Tools::getValue('category_error_message', Configuration::get('category_error_message')),
			'show_manufacturer_search' => Tools::getValue('show_manufacturer_search', Configuration::get('show_manufacturer_search')),
			'number_of_manufacturer' => Tools::getValue('number_of_manufacturer', Configuration::get('number_of_manufacturer')),
			'matching_manufacturer_heading' => Tools::getValue('matching_manufacturer_heading', Configuration::get('matching_manufacturer_heading')),
			'manufacturer_error_message' => Tools::getValue('manufacturer_error_message', Configuration::get('manufacturer_error_message')),
			//autocomplete styles
			'preloader_right_position' => Tools::getValue('preloader_right_position', Configuration::get('preloader_right_position')),
			'preloader_top_position' => Tools::getValue('preloader_top_position', Configuration::get('preloader_top_position')),
			'search_results_top_position' => Tools::getValue('search_results_top_position', Configuration::get('search_results_top_position')),
			'search_results_left_position' => Tools::getValue('search_results_left_position', Configuration::get('search_results_left_position')),
			'autocomplete_background_color' => Tools::getValue('autocomplete_background_color', Configuration::get('autocomplete_background_color')),
			'item_heading_color' => Tools::getValue('item_heading_color', Configuration::get('item_heading_color')),
			'item_description_color' => Tools::getValue('item_description_color', Configuration::get('item_description_color')),
			'product_price_color' => Tools::getValue('product_price_color', Configuration::get('product_price_color')),
			'product_old_price_color' => Tools::getValue('product_old_price_color', Configuration::get('product_old_price_color')),
			'item_matching_color' => Tools::getValue('item_matching_color', Configuration::get('item_matching_color')),
			'item_not_found_message_color' => Tools::getValue('item_not_found_message_color', Configuration::get('item_not_found_message_color')),
		);
	}

	public function hookHeader()
	{
		$this->context->controller->addJS($this->_path.'js/jquery.autocomplete.js');
		$this->context->controller->addJS($this->_path.'js/search-autocomplete.js');
		$this->context->controller->addCSS($this->_path.'css/autocomplete-styles.css');
	}

	public function AutoCompleteProducts($id_lang, $search_query ) 
	{
			$id_lang = $this->context->cookie->id_lang;
			$min_length = Configuration::get('min_length');
			$module_enable = Configuration::get('enable_autocomplete_search');
			$show_product_search = Configuration::get('show_product_search');
			$numbers_of_product = Configuration::get('number_products_search');

			//Sanitize input texts
			$search_words = explode(',', Search::sanitize($search_query, $id_lang));
			foreach ($search_words as $key => $word)
				if (!empty($word) && Tools::strlen($word) >= $min_length)
				{
					$word = str_replace('%', '\\%', $word);
					$word = str_replace('_', '\\_', $word);
					$product_id_array = 'SELECT a.`id_product` FROM '._DB_PREFIX_.'product b INNER JOIN 
						'._DB_PREFIX_.'product_lang a ON a.`id_product` = b.`id_product` INNER JOIN  
						'._DB_PREFIX_.'product_shop c ON a.`id_product` = c.`id_product` WHERE a.name LIKE "%'.$word.'%" AND c.active = 1 and c.`visibility` IN ("both", "search")
AND c.indexed = 1 AND a.id_lang = '.$id_lang.' ';
				}
				else
					unset($search_words[$key]);
		
				if (!count($search_words))
					return array();

				//Get products id whose search is enabled and product is active
				$results = Db::getInstance()->executeS($product_id_array);
				$products = array();
				foreach( $results as $result )
					$products[] = $result['id_product'];

				$product_id_list = '';
				foreach ($products as $id_product)
					if ($id_product)
					$product_id_list .= (int)$id_product.',';
				if (empty($product_id_list))
					return array();
				$product_id_list = ((strpos($product_id_list, ',') === false) ? (' = '.(int)$product_id_list.' ') : (' IN ('.rtrim($product_id_list, ',').') '));

				// Query for getting products properties
				$query = 'SELECT p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity,
				pl.`description_short`, pl.`available_now`, pl.`available_later`, pl.`link_rewrite`, pl.`name`,
				image_shop.`id_image`, il.`legend`, m.`name` manufacturer_name , product_attribute_shop.`id_product_attribute`,
				DATEDIFF(
				p.`date_add`,
				DATE_SUB(
				NOW(),
				INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY
				)
				) > 0 new
				FROM '._DB_PREFIX_.'product p
				'.Shop::addSqlAssociation('product', 'p').'
				INNER JOIN `'._DB_PREFIX_.'product_lang` pl ON (
				p.`id_product` = pl.`id_product`
				AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').'
				)
				LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa	ON (p.`id_product` = pa.`id_product`)
				'.Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.`default_on` = 1').'
				'.Product::sqlStock('p', 'product_attribute_shop', false, Context::getContext()->shop).'
				LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON m.`id_manufacturer` = p.`id_manufacturer`
				LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product`)'.
				Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
				LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$id_lang.')
				WHERE p.`id_product` '.$product_id_list.'
				AND ((image_shop.id_image IS NOT NULL OR i.id_image IS NULL) OR (image_shop.id_image IS NULL AND i.cover=1))
				AND (pa.id_product_attribute IS NULL OR product_attribute_shop.id_shop='.(int)Context::getContext()->shop->id.') ORDER BY 
				p.`id_product` LIMIT '.$numbers_of_product.' ';
				$result = Db::getInstance()->executeS($query);

				if (!$result)
					$result_properties = false;
				else
					$result_properties = Product::getProductsProperties((int)$id_lang, $result);
				  	if( $module_enable == '1' && $show_product_search == '1')
				  	{
				  		return $result_properties;
				  	}
				 
		}

	public function AutoCompleteCategory($id_lang, $search_query)
	{
			$id_lang = $this->context->cookie->id_lang;	
			$category_id_array = array();
			$min_length = Configuration::get('min_length');
			$show_number_category = Configuration::get('number_of_category');
			$show_category = Configuration::get('show_category_search');
			$module_enable = Configuration::get('enable_autocomplete_search');

			//Sanitize input texts
			$search_words = explode(' ', Search::sanitize($search_query, $id_lang));
				foreach ($search_words as $key => $word)
					if (!empty($word) && Tools::strlen($word) >= $min_length)
					{
						$word = str_replace('%', '\\%', $word);
						$word = str_replace('_', '\\_', $word);
						$category_id_array[] = 'SELECT a.`id_category`,a.`name` FROM '._DB_PREFIX_.'category b INNER JOIN
						'._DB_PREFIX_.'category_lang a on a.`id_category` = b.`id_category` WHERE a.name LIKE "'.$word.'%" AND 
						b.active=1 AND a.id_lang = '.$id_lang.'  ORDER BY  id_category DESC LIMIT '.$show_number_category.' ';
					}
					else
						unset($search_words[$key]);
		
				if (!count($search_words))
					return array();
	
				foreach( $category_id_array as $query ) {
					$category = array();
					foreach( Db::getInstance()->executeS($query) as $row )
						$category[] = $row['id_category'];
				}
				
				$category_pool = '';
				foreach( $category as $id_category ) 
					if($id_category)
						$category_pool .= (int)$id_category.',';
					if (empty($category_pool))
						return array();
					$category_pool = ((strpos($category_pool, ',') === false) ? (' = '.(int)$category_pool.' ') : (' IN ('.rtrim($category_pool, ',').') '));
					$sql_query = 'SELECT * FROM '._DB_PREFIX_.'category_lang WHERE `id_category` '.$category_pool.' AND `id_lang` = '.$id_lang.' ';
					$categories = Db::getInstance()->executeS($sql_query);
					if($show_category == '1' && $module_enable == '1') {
						return $categories;
					}
					
	}

	public function AutoCompleteManufacturer($id_lang, $search_query) {
		$id_lang = $this->context->cookie->id_lang;
		$min_length = Configuration::get('min_length');
		$number_of_manufacturer = Configuration::get('number_of_manufacturer');
		$show_manufacturer = Configuration::get('show_manufacturer_search');
		$module_enable = Configuration::get('enable_autocomplete_search');
		$manufacturer_id_array = array();

		$search_words = explode(' ', Search::sanitize($search_query, $id_lang));
		foreach ($search_words as $key => $word)
			if (!empty($word) && Tools::strlen($word) >= $min_length) {
				$word = str_replace('%', '\\%', $word);
				$word = str_replace('_', '\\_', $word);
				$manufacturer_id_array[] = 'SELECT a.`id_manufacturer`,b.`name` FROM '._DB_PREFIX_.'manufacturer b INNER JOIN
						'._DB_PREFIX_.'manufacturer_lang a on a.`id_manufacturer` = b.`id_manufacturer` WHERE b.name 
						LIKE "'.$word.'%" AND b.active=1 AND a.id_lang = '.$id_lang.'  ORDER BY  a.`id_manufacturer` DESC LIMIT '.$number_of_manufacturer.' ';
			}
			else
			unset($search_words[$key]);
		
			if (!count($search_words))
				return array();	
			foreach( $manufacturer_id_array as $query ) {
				$manufacturer = array();
				foreach( Db::getInstance()->executeS($query) as $row )
					$manufacturer[] = $row['id_manufacturer'];
			}
			$manufacturer_pool = '';
			foreach( $manufacturer as $id_manufacturer ) 
				if($id_manufacturer)
					$manufacturer_pool .= (int)$id_manufacturer.',';
					if (empty($manufacturer_pool))
						return array();
					$manufacturer_pool = ((strpos($manufacturer_pool, ',') === false) ? (' = '.(int)$manufacturer_pool.' ') : (' IN ('.rtrim($manufacturer_pool, ',').') '));
					$sql_query = 'SELECT * FROM '._DB_PREFIX_.'manufacturer WHERE `id_manufacturer` '.$manufacturer_pool.' ';
					$manufacturers = Db::getInstance()->executeS($sql_query);
					if($show_manufacturer == '1' && $module_enable == '1') {
						return $manufacturers;
					}			
				
	}

	public function get_settings() {
		$id_lang = $this->context->cookie->id_lang;
		
		$this->smarty->assign(array(
			'search_class_id' => Configuration::get('search_class_id'),
			'enable_autocomplete_search' => Configuration::get('enable_autocomplete_search'),
			'minimum_characters' => Configuration::get('minimum_characters'),
			'preloader_right_position' => Configuration::get('preloader_right_position'),
			'preloader_top_position' => Configuration::get('preloader_top_position'),
			));
		return $this->display(__FILE__, 'views/templates/hook/autocomplete-search.tpl');
	}

		public function hookTop()
	{
		return $this->get_settings();
	}

	public function hookdisplayMobileTopSiteMap()
	{
		return $this->get_settings();
	}		

}
?>