<?php
/**
 * MagniGenie Prestashop Module.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * 
 * @author    MagniGenie <info@magnigenie.com>
 * @category  Front Office Features
 * @package   Auto Complete Search Module
 * @copyright Copyright (c)  2014  (http://magnigenie.com)
 * @license   http://magnigenie.com
 */

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');
include_once(dirname(__FILE__).'/autocompletesearch.php');

$autocomplete_search = new AutoCompleteSearch();
if (!empty($_REQUEST['query']))
	$query = $_REQUEST['query'];
else
	$query = '';

//check prestashop version for add to cart
if(_PS_VERSION_ > 1.5)
	$custom_ajax_cart = 'enabled';
else 
	$custom_ajax_cart = 'disabled';

$search_results = $autocomplete_search->AutoCompleteProducts(Context::getContext()->language->id, $query);
$search_category = $autocomplete_search->AutoCompleteCategory(Context::getContext()->language->id, $query);
$search_manufacturer = $autocomplete_search->AutoCompleteManufacturer(Context::getContext()->language->id, $query);


Context::getContext()->smarty->assign(array(
			'customAjaxCart' => $custom_ajax_cart,
			'searchResults' => $search_results,
			'searchCategory' => $search_category,
			'searchManufacturer' => $search_manufacturer,
			'search_class_id' => Configuration::get('search_class_id'),
			'result_height' => Configuration::get('result_height'),
			'show_product_search' => Configuration::get('show_product_search'),
			'minimum_characters' => Configuration::get('minimum_characters'),
			'show_addtocart' => Configuration::get('show_addtocart'),
			'matching_products_heading' => Configuration::get('matching_products_heading'),
			'products_error_message' => Configuration::get('products_error_message'),
			'show_price' => Configuration::get('show_price'),
			'product_quantity' => Configuration::get('product_quantity'),
			'show_view_button' => Configuration::get('show_view_button'),
			'show_description' => Configuration::get('show_description'),
			'show_category_search' => Configuration::get('show_category_search'),
			'matching_category_heading' => Configuration::get('matching_category_heading'),
			'category_error_message' => Configuration::get('category_error_message'),
			'manufacturer_search' => Configuration::get('show_manufacturer_search'),
			'manufacturer_heading' => Configuration::get('matching_manufacturer_heading'),
			'manufacturer_error_message' => Configuration::get('manufacturer_error_message'),
			'preloader_right_position' => Configuration::get('preloader_right_position'),
			'preloader_top_position' => Configuration::get('preloader_right_position'),
			'search_results_top_position' => Configuration::get('search_results_top_position'),
			'search_results_left_position' => Configuration::get('search_results_left_position'),
			'autocomplete_background_color' => Configuration::get('autocomplete_background_color'),
			'item_heading_color' => Configuration::get('item_heading_color'),
			'item_description_color' => Configuration::get('item_description_color'),
			'product_old_price_color' => Configuration::get('product_old_price_color'),
			'product_price_color' => Configuration::get('product_price_color'),
			'item_matching_color' => Configuration::get('item_matching_color'),
			'item_not_found_message_color' => Configuration::get('item_not_found_message_color'),
		));
echo $autocomplete_search->display(dirname(__FILE__).'/autocompletesearch.php', 'autocomplete-search-result.tpl');