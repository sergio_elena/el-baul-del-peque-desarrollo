﻿/*
 * MagniGenie Prestashop Module.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * 
 * @author    MagniGenie <info@magnigenie.com>
 * @category  Front Office Features
 * @package   Auto Complete Search Module
 * @copyright Copyright (c)  2014  (http://magnigenie.com)
 * @license   http://magnigenie.com
 */

function autocompletesearch(searchTerm) {
	$.post(
	    jQuery('#autocomplete-search-url').val(), 
		{query: searchTerm},
		function(data) { 
			$('#autocomplete_search_results').html(data);			
			$('.autocomplete-ajax-cart').unbind('click').click(function(){				
				var idProduct = $(this).attr('data-product-id');
				var qty = $(this).parents('.cart-wrap').find('input.product-cart-quantity').val();
				ajaxCart.add(idProduct, null, false, this, qty);
			});

			$('.default-add-to-cart').click(function(){
			var idProduct =  $(this).attr('id');
				if ($(this).attr('disabled') != 'disabled')
						ajaxCart.add(idProduct, null, false, this);
				});		
		});
	}
	$(document).mouseup(function (e) {
		var container = $("#autocompletesearch_content_result");
	    if (!container.is(e.target) && container.has(e.target).length === 0) {
	    	container.hide();
	    }
});

