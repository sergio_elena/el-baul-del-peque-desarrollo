{*
* magnigenie Prestashop Module.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
*
/*
* 
* @author    magnigenie <info@magnigenie.com>
* @category  Front Office Features
* @package 	 Auto Complete Search Module
* @copyright Copyright (c)  2014 SPM LLC. (http://magnigenie.com)
* @license   http://magnigenie.com
*}
		<style type="text/css">
	{$search_class_id|escape:htmlall}.showLoader{
		background-image: url('{$base_dir_ssl|escape:htmlall}/modules/autocompletesearch/img/preloader.gif') !important;
		background-position: {literal}{/literal} {$preloader_right_position|escape:htmlall}px  {$preloader_top_position|escape:htmlall}px 
		 !important;
		background-repeat: no-repeat !important;
	{literal}{/literal}
	</style>
	<input type="hidden" id="autocomplete-search-url" value="{$base_dir_ssl|escape:htmlall}modules/autocompletesearch/autocomplete-controller.php">

	<script type="text/javascript">
	 jQuery(document).ready(function() {
		jQuery(document).on('focus', ':input', function() {
    	$(this).attr('autocomplete', 'off');
    });
	 	var content_result = "<div id='autocompletesearch_content_result'><div id='autocomplete_search_results'></div></div>";

	 	jQuery(content_result).insertAfter('{$search_class_id|escape:htmlall}').parent('form').find('.button-search, .button');
		
	 	var minLength = '{$minimum_characters|escape:htmlall}';
	 	if('{$enable_autocomplete_search|escape:htmlall}' == 1) {
			jQuery('{$search_class_id|escape:htmlall}').keyup(function() {
				val = jQuery(this).val();	
				if( val.length > 0 ) {
					$('{$search_class_id|escape:htmlall}').addClass('showLoader');
				}
				else {
					$('{$search_class_id|escape:htmlall}').removeClass('showLoader');
				}
				if(val.length < minLength) {
					$('div#autocompletesearch_content_result').hide();
				}

				if(val.length >= minLength) {
					if(('#autocomplete_search_results').length > 0 ) {
						$('div#autocompletesearch_content_result').addClass('show-border');
						$('{$search_class_id|escape:htmlall}').removeClass('showLoader');
					}
					else {
						$('div#autocompletesearch_content_result').removeClass('show-border');
						$('{$search_class_id|escape:htmlall}').addClass('showLoader');
					}
	 				autocompletesearch(this.value);
	 				jQuery('#autocompletesearch_content_result').show();
	 			}
	 		});


		}
	 });
	</script>