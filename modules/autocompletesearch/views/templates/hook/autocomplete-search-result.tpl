{*
* magnigenie Prestashop Module.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
*
/*
* 
* @author    magnigenie <info@magnigenie.com>
* @category  Front Office Features
* @package 	 Auto Complete Search Module
* @copyright Copyright (c)  2014 SPM LLC. (http://magnigenie.com)
* @license   http://magnigenie.com
*}
<style>
	div#autocompletesearch_content_result {literal}{{/literal}
		background-color: {$autocomplete_background_color|escape:htmlall} !important;
		top: {$search_results_top_position|escape:htmlall}px;
		left: {$search_results_left_position|escape:htmlall}px;
		{literal}}{/literal}
	div#autocomplete_search_results div.item-name > a {literal}{{/literal}
		color: {$item_heading_color|escape:htmlall} !important;
	{literal}}{/literal}
	div#autocomplete_search_results div.description {literal}{{/literal}
		color: {$item_description_color|escape:htmlall} !important;
	{literal}}{/literal}
	div#autocomplete_search_results span.actual-price {literal}{{/literal}
		color: {$product_price_color|escape:htmlall} !important;
	{literal}}{/literal}
	div#autocomplete_search_results span.old-price {literal}{{/literal}
		color: {$product_old_price_color|escape:htmlall} !important;
	{literal}}{/literal}
	div#autocomplete_search_results div.item-type {literal}{{/literal}
		color: {$item_matching_color|escape:htmlall} !important;
	{literal}}{/literal}
	div#autocomplete_search_results div.empty_result {literal}{{/literal}
		color: {$item_not_found_message_color|escape:htmlall} !important;
	{literal}}{/literal}							
</style>
{if $result_height == '0' }
<style>
div#autocomplete_search_results {literal}{{/literal}
	height: auto;
{literal}{{/literal}
</style>
{else}
<style>
div#autocomplete_search_results {literal}{{/literal}
	max-height: {$result_height|escape:htmlall}px;
	overflow-y: scroll;
{literal}{{/literal}
</style>
{/if}
<div class="results-wrap">
{if $show_product_search == '1'}
	{if !empty($searchResults)}
		<div class="item-type">{$matching_products_heading|escape:htmlall}</div>
		{foreach from=$searchResults item=item key=key}
			<div class="result-item">
				<div class="item-image col-sm-4">
					<a href="{$item.link}" class="product-details" title="{$item.name|escape:'htmlall':'UTF-8'}">
						<img class="item-image" src="{$link->getImageLink($item.link_rewrite,$item.id_image ,'medium_default')}" alt="{$item.legend|escape:'htmlall':'UTF-8'}" />					
					</a>
				</div>	
			<div class="item-name col-sm-5">
				<a href="{$item.link}" class="product_img_link" title="{$item.name|escape:'htmlall':'UTF-8'}">
					{$item.name|escape:htmlall}
				</a>
			</div>
			<div class="price col-sm-3">
			{if $show_price == '1'}
				{if isset($item.on_sale) && $item.on_sale && isset($item.show_price) && $item.show_price && !$PS_CATALOG_MODE}
					<span class="on_sale">{l s='On sale!' mod='autocompletesearch'}</span>
					{elseif isset($item.reduction) && $item.reduction && isset($item.show_price) && $item.show_price && !$PS_CATALOG_MODE}
				{/if}
				{if (!$PS_CATALOG_MODE AND ((isset($item.show_price) && $item.show_price) || (isset($item.available_for_order) && $item.available_for_order)))}
					<div class="item-price">
						{if isset($item.show_price) && $item.show_price && !isset($restricted_country_mode)}
					 		<span class="actual-price" style="display: inline;">
							{if !$priceDisplay}
								{convertPrice price=$item.price}
							{else}
								{convertPrice price=$item.price_tax_exc}
							{/if}
					 		</span>
						{/if}					
						{if $item.reduction}
							<span class="old-price">{convertPrice price=$item.price_without_reduction}</span>
						{/if}
					</div>				
				{/if}
			{/if}
			</div>			
			{if $show_description == '1'}
				<div class="description col-sm-8">
					{$item.description_short|strip_tags:'UTF-8'|truncate:180:'...'}			
				</div>
			{/if}

			<div class="cart-wrap col-sm-12">
				<div class="product-quantity col-sm-4">
				{if $product_quantity == '1'}
					{if ($item.quantity > 0)}
						<input type="number" class="product-cart-quantity" min="1" name="quantity_to_cart_{$item.id_product|intval}" 
						id="quantity_to_cart_{$item.id_product|intval}" value="{if isset($quantityBackup)}{$quantityBackup|intval}{else}{if $item->minimal_quantity > 1}{$item->minimal_quantity}{else}1{/if}{/if}"/>
					{/if}
				{/if}
				</div>
				

				<div class="cart col-sm-4">
				{if $show_addtocart == '1'}
					{if customAjaxCart == 'enabled'}
						{if ($item.allow_oosp || $item.quantity > 0)}
							{if isset($static_token)}
								<a class="button ajax_add_to_cart_button btn btn-default" href="{$base_dir_ssl|escape:htmlall}cart?add=1&amp;id_product={$item.id_product|escape:htmlall}" rel="nofollow" title="Add to cart" data-id-product="{$item.id_product|escape:htmlall}">
											<span>{l s='Add to Cart' mod='autocompletesearch'}</span>
										</a>
							{else}
								<a class="button ajax_add_to_cart_button btn btn-default" href="{$base_dir_ssl|escape:htmlall}cart?add=1&amp;id_product={$item.id_product|escape:htmlall}" rel="nofollow" title="Add to cart" data-id-product="{$item.id_product|escape:htmlall}">
											<span>{l s='Add to Cart' mod='autocompletesearch'}</span>
										</a>									
							{/if}						
						{else}
							<span class="exclusive"><span></span>{l s='Out of stock' mod='autocompletesearch'}</span><br />
					{/if}
					{else}
					<!-- for ps 1.5 -->
						{if ($item.allow_oosp || $item.quantity > 0)}
							{if isset($static_token)}
								<a class="button ajax_add_to_cart_button exclusive btn btn-default" rel="ajax_id_product_{$item.id_product|intval}" id="{$item.id_product}" href="javascript:void(0);"><span>{l s='Add to Cart' mod='autocompletesearch'}</span></a>
							{else}
								{if $product_quantity== '1'}
									<a class="button  ajax_add_to_cart_button autocomplete-ajax-cart" data-product-id="{$item.id_product}">
										<span>{l s='Add to Cart' mod='autocompletesearch'}</span>
									</a>
								{else}
								<a class="button ajax_add_to_cart_button exclusive btn btn-default default-add-to-cart" rel="ajax_id_product_{$item.id_product|intval}" id="{$item.id_product}" href="javascript:void(0);"><span>{l s='Add to Cart' mod='autocompletesearch'}</span></a>
								{/if}
							{/if}					
						{else}
							<span class="exclusive"><span></span>{l s='Out of stock' mod='autocompletesearch'}</span><br />
					{/if}
					<!-- ps 1.5 finished -->					
					{/if}
			{/if}
			</div> <!--.cart-->

			<div class="view col-sm-4">	
			{if $show_view_button == '1'}
					<a class="button lnk_view btn btn-default"  id="{$item.id_product|escape:htmlall}" href="{$item.link|escape:htmlall}"><span>{l s='view details' mod='autocompletesearch'}</span></a>	
			{/if}	
			</div>
			

			</div><!--.cart-wrap-->
		</div>
		<div class="result-separator"></div>
	{/foreach}
	
{else}
	<div class="empty_result">{$products_error_message|escape:htmlall}</div>
{/if}
{/if}
{if $show_category_search == '1'}
	{if !empty($searchCategory)}
		<div class="item-type">{$matching_category_heading|escape:htmlall}</div>
		{foreach from=$searchCategory item=item key=key}
			<div class="result-item result-item-list">
				<div class="item-image col-sm-6">
					<a href="{$base_dir_ssl|escape:htmlall}index.php?id_category={$item.id_category|escape:htmlall}&controller=category">
						<img src="{$base_dir_ssl|escape:htmlall}/img/c/{$item.id_category}.jpg" class="item-image" alt="{$item.name|escape:htmlall}">
					</a>
				</div>
				<div class="item-name col-sm-5">
					<a href="{$base_dir_ssl|escape:htmlall}index.php?id_category={$item.id_category|escape:htmlall}&controller=category">{$item.name|escape:htmlall}</a>
				</div>
				<div class="category-desc">{$item.description|escape:htmlall}</div>
				<div class="clear-fix-items"></div>
			</div>
		{/foreach}
	{else}
		<div class="empty_result">{$category_error_message|escape:htmlall}</div>
	{/if}
{/if}

{if $manufacturer_search == '1'}
	{if !empty($searchManufacturer)}
		<div class="item-type">{$manufacturer_heading|escape:htmlall}</div>
		{foreach $searchManufacturer item=item key=key}
		<div class="result-item result-item-list manufacturer-image">
			<div class="item-image col-sm-6 ">
				<img src="{$base_dir_ssl|escape:htmlall}/img/m/{$item.id_manufacturer|escape:htmlall}-small_default.jpg" 
				alt="{$item.name|escape:htmlall}">
			</div>
			<div class="item-name col-sm-6">
				<a href="{$base_dir_ssl|escape:htmlall}index.php?id_manufacturer={$item.id_manufacturer|escape:htmlall}&controller=manufacturer">{$item.name}</a>
			</div>
		</div>
		{/foreach}
	{else}
	<div class="empty_result">{$manufacturer_error_message|escape:htmlall}</div>
	{/if}
{/if}
</div>

