<!-- Block customlinks module -->
<div id="pk_customlinks">	
	<ul>
		{if ($wishlist_link != "")}
		<li class="pk_wishlist dd_elem">
			<a href="{$wishlist_link}">{l s='My Wishlist' mod='pk_customlinks'} (<span class="wlQty">{if !empty($pk_wishlist["wishlist_products"])}{$pk_wishlist["wishlist_products"]|count}{else}0{/if}</span>)</a>
			<div id="pk_wishlist" class="dd_cont">
				<div class="indent">
				<dl>
				{if $pk_wishlist["wishlist_products"]}			
					{foreach from=$pk_wishlist["wishlist_products"] item=product name=i}							
						<dt class="clearfix {if $smarty.foreach.i.first}first_item{elseif $smarty.foreach.i.last}last_item{else}item{/if}">
							<a href="{$link->getProductLink($product.id_product, $product.link_rewrite, $product.category_rewrite)|escape:'html'}" class="WishListProdImage">
								<img src="{$link->getImageLink($product.link_rewrite, $product.image, 'medium_alysum')|escape:'htmlall':'UTF-8'}" alt=""/>
							</a>
							<div class="text_desc">
								<span class="quantity-formated"><span class="quantity">{$product.quantity|intval}</span>x</span>
								<a class="cart_block_product_name"
								href="{$link->getProductLink($product.id_product, $product.link_rewrite, $product.category_rewrite)|escape:'html'}" title="{$product.name|escape:'htmlall':'UTF-8'}">{$product.name|truncate:30:'...'|escape:'htmlall':'UTF-8'}</a>
								{if !isset($restricted_country_mode) AND !$PS_CATALOG_MODE}<div class="price">{convertPrice price=$product.price}</div>{/if}
								<a class="ajax_cart_block_remove_link" href="javascript:;" onclick="javascript:WishlistCart('wishlist_block_list', 'delete', '{$product.id_product}', {$product.id_product_attribute}, '0', '{$token}'); $(this).closest('dt').hide('fast'); $('.wlQty').text((parseInt($('.wlQty').text()))-1);$('#pk_wishlist').css({ 'height':'auto' });" title="{l s='remove this product from my wishlist' mod='pk_customlinks'}"></a>
							</div>
						</dt>						
					{/foreach}
				{else}
					<dt class="no-products">{l s='No products' mod='pk_customlinks'}</dt>
				{/if}
				</dl>
				</div>
			</div>
		</li>
		{/if}
		{if ($favorite_module == 1)}
		<li class="pk_favorites dd_elem">
			<a href="{$link->getModuleLink('favoriteproducts', 'account')|escape:'htmlall':'UTF-8'}">{l s='My Favorites' mod='pk_customlinks'} (<span class="favQty">{if !empty($pk_wishlist["wishlist_products"])}{$pk_favoriteProducts|count}{else}0{/if}</span>)</a>
			<div class="favoritelist dd_cont">
				<div class="indent">
				<ul>
				{if $pk_favoriteProducts}								
					{foreach from=$pk_favoriteProducts item=favoriteProduct}
					<li class="favoriteproduct clearfix">
						<a href="{$link->getProductLink($favoriteProduct.id_product, null, null, null, null, $favoriteProduct.id_shop)|escape:'htmlall':'UTF-8'}" class="favProdImage">
							<img src="{$link->getImageLink($favoriteProduct.link_rewrite, $favoriteProduct.image, 'medium_alysum')|escape:'htmlall':'UTF-8'}" alt=""/>
						</a>
						<div class="text_desc">
							<a href="{$link->getProductLink($favoriteProduct.id_product, null, null, null, null, $favoriteProduct.id_shop)|escape:'htmlall':'UTF-8'}">{$favoriteProduct.name|escape:'htmlall':'UTF-8'}</a>
							{if !isset($restricted_country_mode) AND !$PS_CATALOG_MODE}<div class="price">{convertPrice price=$favoriteProduct.price}</div>{/if}
							<a href="#" onclick="return false" class="remove" rel="ajax_id_favoriteproduct_{$favoriteProduct.id_product}" data-buttonID="{$favoriteProduct.id_product}">&nbsp</a>
						</div>
					</li>
					{/foreach}				
				{else}
					<li class="no-products">{l s='No favorite products have been determined just yet. ' mod='pk_customlinks'}</li>
				{/if}					
				</ul>
				</div>
			</div>
		</li>
		{/if}
		<li class="pk_watchlist dd_elem"><a href="#">{l s='Watch List' mod='pk_customlinks'} ({count($watchlist)})</a>
			<div class="watchlist dd_cont">
				<div class="indent">
				<span>{l s='Recently viewed products' mod='pk_customlinks'}</span>
				<ul>

					{if $watchlist}
					{foreach from=$watchlist item=viewedProduct name=myLoop}
						<li class="clearfix{if $smarty.foreach.myLoop.last} last_item{elseif $smarty.foreach.myLoop.first} first_item{else} item{/if}">
							<a href="{$viewedProduct->product_link|escape:'html'}" title="{l s='About' mod='pk_customlinks'} {$viewedProduct->name|escape:html:'UTF-8'}" class="content_img">
							<img src="{if isset($viewedProduct->id_image) && $viewedProduct->id_image}{$link->getImageLink($viewedProduct->link_rewrite, $viewedProduct->cover, 'medium_alysum')}{else}{$img_prod_dir}{$lang_iso}-default-medium_avena.jpg{/if}" alt="{$viewedProduct->legend|escape:html:'UTF-8'}" />
							</a>
							<div class="text_desc">
								<a href="{$viewedProduct->product_link|escape:'html'}" title="{l s='About' mod='pk_customlinks'} {$viewedProduct->name|escape:html:'UTF-8'}">{$viewedProduct->name}</a>
								{if !isset($restricted_country_mode) AND !$PS_CATALOG_MODE}<div class="price">{convertPrice price=$viewedProduct->price}</div>{/if}
							</div>
						</li>
					{/foreach}
					{else}
						<li>{l s='No viewed products yet.' mod='pk_customlinks'}</li>
					{/if}
				</ul>
			</div>
			</div>
		</li>
		{foreach from=$customlinks_links item=blocklink_link}
			{if isset($blocklink_link.$lang)}
				<li class="dd_elem">
					<a href="{$blocklink_link.url|escape}"{if $blocklink_link.newWindow} onclick="window.open(this.href);return false;"{/if}>{$blocklink_link.$lang|escape}</a>
				</li>			
			{/if}
		{/foreach}
	</ul>
	<script type="text/javascript">
	$('document').ready(function(){
		$('a[rel^=ajax_id_favoriteproduct_]').click(function()
		{
			var idFavoriteProduct =  $(this).attr('rel').replace('ajax_id_favoriteproduct_', '');
			var parent = $(this).parent().parent();

			$.ajax({
				url: "{$link->getModuleLink('favoriteproducts', 'actions', ['process' => 'remove'], true)|addslashes}",
				type: "POST",
				data: {
					'id_product': idFavoriteProduct,
					'ajax': true
				},
				success: function(result)
				{
					if (result == '0')
					{
						parent.fadeOut("normal", function()
						{
							parent.remove();
							var num = parseInt($(".favQty").text());
							$(".favQty").text(num-1);
							var buttonID = $(this).data("buttonID") // get data
							var favprodID = $(this).data("favprodID") // get data
							if (buttonID == favprodID) $(".product_like").removeClass("active");
							if ($(".favoritelist li").length == 0) {
	                        	$(".favoritelist ul").append('<li class="no-products">No favorite products have been determined just yet.</li>');
	                        }
						});
					}
	 		 	}
			});
		});
		$('.dd_elem').hover(
			function () { $(this).find('.dd_cont').stop().slideDown(800, 'easeOutQuint');}, 
			function () { $(this).find('.dd_cont').stop().slideUp(200, 'easeOutQuint');
		});	
	});
	</script>
</div>
<!-- /Block customlinks module -->
