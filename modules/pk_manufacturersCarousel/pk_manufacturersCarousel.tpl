{if $page_name == "index"}
{if $theme_settings.manufacturers_carousel == 1}
<!-- Manufacturers Carousel module -->
	<div id="manufacturersCarousel">
		<div class="manufacturersCarouselWrapper">
			{if $manufacturers}
				<div class="mc_container">
					<div class="mc_sub_container">
						<ul id="m-list">
						{foreach from=$manufacturers item=manufacturer name=manufacturer_list}
							{if $smarty.foreach.manufacturer_list.iteration <= $text_list_nb}		
							<li class="{if $smarty.foreach.manufacturer_list.last}last_item{elseif $smarty.foreach.manufacturer_list.first}first_item{else}item{/if}">
								<a href="{$link->getmanufacturerLink($manufacturer.id_manufacturer, $manufacturer.link_rewrite)}" title="{l s='More about' mod='pk_manufacturersCarousel'} {$manufacturer.name}">
								<img src="{$img_manu_dir}{$manufacturer.id_manufacturer}-manu_alysum.jpg" alt="" />
								{if isset($show_title) AND $show_title == 1}<br/>
								{$manufacturer.name|escape:'htmlall':'UTF-8'}
								{/if}
								</a>
							</li>
							{/if}
						{/foreach}
						</ul>	
					</div>
				</div>
			{else}
				<p>{l s='No manufacturer' mod='pk_manufacturersCarousel'}</p>
			{/if}
		</div>
		{if $manufacturers && ($manufacturers|count >= 7)}
		<script>
		$("#m-list").flexisel({
			pref: "mnf",
	        visibleItems: 7,
	        animationSpeed: 1000,
	        autoPlay: false,
	        autoPlaySpeed: 3000,            
	        pauseOnHover: true,
	        enableResponsiveBreakpoints: true,
	        responsiveBreakpoints: { 
	            portrait: { 
	                changePoint:480,
	                visibleItems: 2
	            }, 
	            landscape: { 
	                changePoint:728,
	                visibleItems: 3
	            },
	            tablet: { 
	                changePoint:980,
	                visibleItems: 6
	            }
	        }
	    });
		</script>
	{/if}
	</div>	
<!-- /Manufacturers Carousel module -->
{/if}
{/if}