<?php
/*
*
*  @author Promokit Co. <support@promokit.eu>
*  @copyright  2011-2012 Promokit Co.
*  @version  Release: $Revision: 0 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of Promokit Co.
*/

if (!defined('_PS_VERSION_'))
	exit;

class pk_manufacturersCarousel extends Module
{
    public function __construct()
    {
        $this->name = 'pk_manufacturersCarousel';
        $this->tab = 'front_office_features';
        $this->version = 1.0;
		$this->author = '';
		$this->need_instance = 0;
		$this->bootstrap = true;

        parent::__construct();

		$this->displayName = $this->l('Manufacturers Carousel');
        $this->description = $this->l('Displays a block of manufacturers/brands');
    }

	public function install()
	{
		Configuration::updateValue('MCAROUSEL_DISPLAY_TITLE', 1);
		Configuration::updateValue('MCAROUSEL_DISPLAY_TEXT_NB', 8);
        return parent::install() && $this->registerHook('hook_home_07') && $this->registerHook('header');
    }

	public function hookHook_home_07($params)
	{
		$this->smarty->assign(array(
			'manufacturers' => Manufacturer::getManufacturers(),
			'text_list_nb' => Configuration::get('MCAROUSEL_DISPLAY_TEXT_NB'),
			'show_title' => Configuration::get('MCAROUSEL_DISPLAY_TITLE'),
			'display_link_manufacturer' => Configuration::get('PS_DISPLAY_SUPPLIERS'),
		));
		return $this->display(__FILE__, $this->name.'.tpl');
	}


	public function getContent()
	{
		$output = '<h2>'.$this->displayName.'</h2>';
		if (Tools::isSubmit('submitBlockManufacturers'))
		{
			$text_list = (int)(Tools::getValue('text_list'));
			$text_nb = (int)(Tools::getValue('text_nb'));
			if ($text_list && !Validate::isUnsignedInt($text_nb))
				$errors[] = $this->l('Invalid number of elements');			
			else
			{
				Configuration::updateValue('MCAROUSEL_DISPLAY_TEXT_NB', $text_nb);
				Configuration::updateValue('MCAROUSEL_DISPLAY_TITLE', $text_list);
			}
			if (isset($errors) && count($errors))
				$output .= $this->displayError(implode('<br />', $errors));
			else
				$output .= $this->displayConfirmation($this->l('Settings updated'));
		}
		return $output.$this->displayForm();
	}

	public function displayForm()
	{
		$output = '
		<form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" method="post">
			<fieldset><legend><img src="'.$this->_path.'logo.gif" alt="" title="" />'.$this->l('Settings').'</legend>
				<label>'.$this->l('Show Manufacturer Title').'</label>
				<div class="margin-form">
					<input type="radio" name="text_list" id="text_list_on" value="1" '.(Tools::getValue('text_list', Configuration::get('MCAROUSEL_DISPLAY_TITLE')) ? 'checked="checked" ' : '').'/>
					<label class="t" for="text_list_on"> <img src="../img/admin/enabled.gif" alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" /></label>
					<input type="radio" name="text_list" id="text_list_off" value="0" '.(!Tools::getValue('text_list', Configuration::get('MCAROUSEL_DISPLAY_TITLE')) ? 'checked="checked" ' : '').'/>
					<label class="t" for="text_list_off"> <img src="../img/admin/disabled.gif" alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" /></label>
				</div><br/>
				<div class="margin-form">
					'.$this->l('Display').' <input type="text" size="2" name="text_nb" style="width:40px; display:inline-block" value="'.(int)(Tools::getValue('text_nb', Configuration::get('MCAROUSEL_DISPLAY_TEXT_NB'))).'" /> '.$this->l('elements').'
				</div>	<br/>
				<input type="submit" name="submitBlockManufacturers" value="'.$this->l('Save').'" class="button" />	
			</fieldset>
		</form>';
		return $output;
	}

	public function hookHeader($params)
	{
		$this->context->controller->addCSS(($this->_path).$this->name.'.css', 'all');
		$this->context->controller->addJS($this->_path.'js/jquery.flexisel.js');
	}
	public function hookHook_home_01($params)
	{
		return $this->hookHook_home_07($params);
	}
	public function hookHook_home_02($params)
	{
		return $this->hookHook_home_07($params);
	}
	public function hookHook_home_03($params)
	{
		return $this->hookHook_home_07($params);
	}
	public function hookHook_home_04($params)
	{
		return $this->hookHook_home_07($params);
	}
	public function hookHook_home_05($params)
	{
		return $this->hookHook_home_07($params);
	}	
	public function hookHook_home_06($params)
	{
		return $this->hookHook_home_07($params);
	}	
	public function hookHook_home_04a($params)
	{
		return $this->hookHook_home_07($params);
	}
}
