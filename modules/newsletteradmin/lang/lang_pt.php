<?php

$_LANG = array();
$_LANG['BACKUPS Are you sure to delete this newsletter'] = 'Tem certeza que deseja apagar esta newsletter';
$_LANG['BACKUPS Delete this newsletter'] = 'Apagar esta newsletter';
$_LANG['BACKUPS Manage your backups'] = 'Gerenciar os seus backups';
$_LANG['BACKUPS View this newsletter'] = 'Ver esta newsletter';
$_LANG['BACKUPS Your backups'] = 'Os seus backups';
$_LANG['BACKUPS Your backups directory is empty'] = 'O directório de backups esta vazio';
$_LANG['BOUNCES Bounce e-mail is electronic mail that is returned to the sender because it cannot be delivered for some reason. There are two kinds of bounce e-mail: hard bounce and soft bounce. - Hard bounce e-mail is permanently bounced back to the sender because the address is invalid. - Soft bounce e-mail is recognized by the recipient\'s mail server but is returned to the sender because the recipient\'s mailbox is full, the mail server is temporarily unavailable, or the recipient no longer has an e-mail account at that address.'] = 'Devoluções (Bouces) é um e-mail que é devolvido ao remetente, pois não pode ser entregue por algum motivo. Existem dois tipos de devoluções de-mail: devolução alta e devolução baixa - Devoluções altas de e-mail são devolvidas ao remetente porque o endereço não é válido. - Devolução baixa pelo servidor de correio do destinatário, é devolvido ao remetente porque a caixa de correio do destinatário está cheia, o servidor de correio está temporariamente indisponível, ou o destinatário já não tem uma conta de e-mail nesse endereço.';
$_LANG['BOUNCES Bounces cleaning'] = 'Limpar devoluçoes';
$_LANG['BOUNCES Clean database'] = 'Limpar Base dados';
$_LANG['BOUNCES Cleaning email addresses imported invalid in the database'] = 'Limpeza de endereços de e-mail inválidos importados da base de dados';
$_LANG['BOUNCES Deleted entries'] = 'Eliminar entradas';
$_LANG['BOUNCES Extract emails'] = 'Extrair e-mails';
$_LANG['BOUNCES No customer address is deleted'] = 'Sem endereço de cliente para apagar';
$_LANG['BOUNCES No, I\'m not sure...'] = 'Nao, Nao tenho a certeza';
$_LANG['BOUNCES Number of addresses added in the base'] = 'Número de endereços adicionados na base dados';
$_LANG['BOUNCES Operation canceled'] = 'Operação cancelada';
$_LANG['BOUNCES Please copy/paste your text below with'] = 'Por favor, copie / cole o texto abaixo com';
$_LANG['BOUNCES Result of the operation on the database'] = 'Por favor, copie / cole o texto abaixo com';
$_LANG['BOUNCES Summary import email addresses'] = 'Resumo da importaçao de endereços de e-mail';
$_LANG['BOUNCES The insertion of data in the database is completed successfully'] = 'A inserção de dados na base de dados foi concluída com êxito';
$_LANG['BOUNCES Warning this action is irreversible'] = 'Aviso esta acção é irreversível';
$_LANG['BOUNCES Your text does not contain any email address'] = 'O seu texto não contém qualquer endereço de e-mail';
$_LANG['BOUNCES to analyze'] = 'analisar';
$_LANG['CRON Are you sure to delete this cron task'] = 'Tem certeza que deseja apagar esta tarefa cron';
$_LANG['CRON Back'] = 'Voltar';
$_LANG['CRON Click here to read this email in your browser'] = 'Clique aqui para ler este e-mail no seu navegador';
$_LANG['CRON Close'] = 'Fechar';
$_LANG['CRON Delete this task'] = 'Apagar tarefa';
$_LANG['CRON File'] = 'Ficheiro';
$_LANG['CRON Info: You have crashed an existing backup!'] = 'Info: Voce crashou um backup existente!';
$_LANG['CRON Manage your Cron tasks'] = 'Gerenciar suas tarefas Cron';
$_LANG['CRON Mr'] = 'Sr';
$_LANG['CRON Ms'] = 'Sra';
$_LANG['CRON Sending messages finished at'] = 'O envio de mensagens acabara em';
$_LANG['CRON Sending newsletter planned for'] = 'Enviar newsletter planeada para';
$_LANG['CRON Sending scheduled from a cron job'] = 'Enviar programaçao desde uma tarefa cron job';
$_LANG['CRON Sending the mail,on:'] = 'Envio de e-mail, em:';
$_LANG['CRON Subscribe'] = 'Subscreva';
$_LANG['CRON Total number of messages sent'] = 'Total de mensagens enviadas';
$_LANG['CRON Unsubscribe'] = 'Remover';
$_LANG['CRON WARNING: You have one or more errors, see your logs.'] = 'AVISO: Você tem um ou mais erros, ver os seus logs.';
$_LANG['CRON You do not have to send newsletter today'] = 'Você não tem que enviar newsletter hoje';
$_LANG['CRON Your cron directory is empty'] = 'A directoria de cron jobs esta vazia';
$_LANG['CRON Your cron pending tasks'] = 'Tarefas pendentes de cron job';
$_LANG['CRON Your session has been destroyed'] = 'Sua sessão foi destruída';
$_LANG['CRON at'] = 'as';
$_LANG['CRON on'] = 'em';
$_LANG['HISTORY Date'] = 'Data';
$_LANG['HISTORY Impact'] = 'Impacto';
$_LANG['HISTORY Name'] = 'Nome';
$_LANG['HISTORY Received'] = 'Recebido';
$_LANG['HISTORY Results of the latest campaigns'] = 'Resultados das ultimas campanhas';
$_LANG['HISTORY Sent'] = 'Enviar';
$_LANG['HISTORY Time'] = 'Tempo';
$_LANG['IMPORT Back'] = 'Voltar';
$_LANG['IMPORT Database error'] = 'Erro base dados';
$_LANG['IMPORT ERROR! No file...'] = 'ERRO! No ficheiro...';
$_LANG['IMPORT Emails imported to the group'] = 'Emails importados para o grupo';
$_LANG['IMPORT Error extension file, please choose only a csv or txt file'] = 'Erro na extensão do arquivo, por favor, escolha apenas um arquivo CSV ou txt';
$_LANG['IMPORT IMPORT SUCCESS'] = 'IMPORTADO COM SUCESSO';
$_LANG['IMPORT Import CSV File'] = 'Importar ficheiro CSV';
$_LANG['IMPORT Import csv file into your database'] = 'Importar ficheiro csv para a base dados';
$_LANG['IMPORT Import file'] = 'Importar ficheiro';
$_LANG['IMPORT Import stopped'] = 'Importaçao parou';
$_LANG['IMPORT Name of your file'] = 'Nome do ficheiro';
$_LANG['IMPORT Please, choose a file'] = 'Por favor, escolha o ficheiro';
$_LANG['IMPORT Save'] = 'Guardar';
$_LANG['IMPORT Size'] = 'Tamanho';
$_LANG['IMPORT Temporarily stored in'] = 'Guardado temporariamente em';
$_LANG['IMPORT This address will be not imported because the email syntax is invalid'] = 'Este endereço não será importado porque a sintaxe e-mail é inválida';
$_LANG['IMPORT This group name is already in use, please choose another one!'] = 'Este nome de grupo já está em uso, por favor, escolha outro!';
$_LANG['IMPORT Type'] = 'Tipo';
$_LANG['IMPORT Warning, last lines of your file are empty'] = 'Atenção, última linha do arquivo está vazio';
$_LANG['IMPORT Writing in database'] = 'Escrevendo na basse de dados';
$_LANG['IMPORT You must choose a name for this group'] = 'Você deve escolher um nome para o grupo';
$_LANG['IMPORT Your file is not a csv or txt file'] = 'Seu arquivo não é um arquivo csv ou txt';
$_LANG['IMPORT imported address'] = 'endereço importado';
$_LANG['LOGS Are you sure to delete this logs'] = 'Tem certeza que deseja apagar este registo';
$_LANG['LOGS Delete this log'] = 'Apagar este registo';
$_LANG['LOGS See and manage your logs'] = 'Ver e gerenciar seus registos';
$_LANG['LOGS View this log'] = 'Ver este registo';
$_LANG['LOGS Your logs'] = 'Os seus registos';
$_LANG['LOGS Your logs directory is empty'] = 'O directório de registos esta vazio';
$_LANG['TEMPLATES Are you sure to delete this template'] = 'Tem certeza que deseja apagar este modelo';
$_LANG['TEMPLATES Delete this template'] = 'Apagar este modelo';
$_LANG['TEMPLATES Manage your templates'] = 'Gerenciar modelos';
$_LANG['TEMPLATES View this template'] = 'Ver este modelo';
$_LANG['TEMPLATES Your template directory is empty'] = 'O directório de modelos esta vazio';
$_LANG['TEMPLATES Your templates'] = 'Os seus modelos';
$_LANG['TRACK Campaign number'] = 'Campanha numero';
$_LANG['TRACK Not enough campaigns'] = 'Campanhas insuficientes';
$_LANG['TRACK Oldest campaign'] = 'Campanhas antigas';
$_LANG['TRACK This campaign not exist!'] = 'Esta campanha nao existe!';
$_LANG['TRACK This result is already registred'] = 'Esse resultado já foi registado';
$_LANG['TRACK This subject not exist!'] = 'Este assunto não existe!';
$_LANG['TRACK Your IP is'] = 'O seu  IP é';
$_LANG['TRANSLATE Google suggestion'] = 'Sugestao Google';
$_LANG['TRANSLATIONS Back'] = 'Voltar';
$_LANG['TRANSLATIONS Edit your files translations'] = 'Edite seus arquivos de traduções ';
$_LANG['TRANSLATIONS Suggest with Google ?'] = 'Sugerido pelo Google ?';
$_LANG['TRANSLATIONS The fields can be empty, thank you for filling them'] = 'Os campos nao podem estar vazios, obrigado por os ter preenchido';
$_LANG['TRANSLATIONS Translations'] = 'Traduçoes';
$_LANG['TRANSLATIONS Update translations'] = 'Actualizar traduçoes';
$_LANG['TRUNCATE Deleting campaigns'] = 'Apagar campanhas';
$_LANG['TRUNCATE Report'] = 'Relatório';
$_LANG['TRUNCATE The table'] = 'A tabela';
$_LANG['TRUNCATE has been cleared'] = 'foi limpa';
$_LANG['TRUNCATE has not been emptied of its contents'] = 'não tenha sido limpa do seu conteúdo';
$_LANG['UPDATE Back'] = 'Voltar';
$_LANG['UPDATE Edit your files translations'] = 'Editar as suas traduçoes';
$_LANG['UPDATE Record'] = 'Gravar';
$_LANG['UPDATE Translations'] = 'Traduçoes';
$_LANG['UPDATE You have not entered all information'] = 'Não introduziu toda a informaçao';
$_LANG['UPDATE translations updated!'] = 'traduções actualizadas!';
$_LANG['UPGRADE Checking the latest version'] = 'Verificar ultima versao';
$_LANG['UPGRADE Downloading file'] = 'Descarregar ficheiro';
$_LANG['UPGRADE End of installation'] = 'Fim da instalaçao';
$_LANG['UPGRADE Error!'] = 'Erro!';
$_LANG['UPGRADE Extracting file'] = 'Extrair ficheiro';
$_LANG['UPGRADE Last version online'] = 'ultima versao online';
$_LANG['UPGRADE Module Updater'] = 'Actualizar modulo';
$_LANG['UPGRADE Ok, downloading files'] = 'Ok, a descarregar ficheiros';
$_LANG['UPGRADE Please wait'] = 'Por favor aguarde';
$_LANG['UPGRADE Resetting the updated module in progress'] = 'Reajuste do módulo atualizado em andamento';
$_LANG['UPGRADE Server Error, Please try later'] = 'Erro servidor, tente mais tarde';
$_LANG['UPGRADE Updating Newsletteradmin Module'] = 'Actualizar o modulo Newsletteradmin';
$_LANG['UPGRADE Your files have been restored, Update canceled'] = 'Os seus ficheiros foram restaurados, Actualização cancelada';
$_LANG['UPGRADE Zip Archive missing on the server, Please try later'] = 'Arquivo Zip faltando no servidor, por favor, tente mais tarde';
$_LANG['UPGRADE files were unpacked in the directory'] = 'arquivos foram descompactados no diretório';
$_LANG['UPGRADE xml request error! Try later'] = 'erro de pedido xml! Tente mais tarde';
?>