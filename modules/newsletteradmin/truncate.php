<?php
/**
 * Nettoyage des tables pour le module newsletteradmin  
 * @category admin 
 * @copyright eolia@o2switch.net
 * @Author Eolia  08/07/2013  compatible only PS >= 1.5.x.x 
 * @version 2.5
 *
 */

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/functions.php');
	if(intval(Configuration::get('PS_REWRITING_SETTINGS')) === 1)
		$rewrited_url = __PS_BASE_URI__;
	
	$filename = 'TRUNCATE';	
	$Key = Configuration::get('NEWSLETTER_KEY_CODE');
	
	$table1   = 'mailing_history';  
	$table2   = 'mailing_track';  

  function vider_table($table){
 
    if(Db::getInstance()->Execute("DELETE FROM "._DB_PREFIX_.$table." WHERE id_shop = ".$_GET['id_shop']))
	{
      // SUCCES
      echo trans('The table').' '.$table.' '.trans('has been cleared').' !<br/><br/>'; 
    }else{
      // ECHEC
      echo trans('The table').' '.$table.' '.trans('has not been emptied of its contents').'.<br/><br/>';
	}
  }
  

	echo'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta http-equiv="content-type" content="text/html; charset=utf-8" />
			<link rel="stylesheet" type="text/css" href="views/css/newsletteradmin.css"/>
			<title>'.trans('Deleting campaigns').'</title>
		</head>
			<body>
				<div class="newsfield" >
					<legend>'.trans('Report').'</legend>';
					
	//Vidage des tables
		Db::getInstance()->Execute("DELETE FROM "._DB_PREFIX_."mailing_sent WHERE id_shop = ".$_GET['id_shop']); 			
		vider_table($table1);
		vider_table($table2);
			
	echo '			<script type="text/javascript">opener.location.reload();setTimeout(\'window.close()\', 1000);</script>
				</div>
			</body>';
?>	