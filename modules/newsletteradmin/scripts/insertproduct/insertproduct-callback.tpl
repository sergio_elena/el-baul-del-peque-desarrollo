<return value="ok">
{if isset($products)}
	{foreach from=$products item=product name=products}
		<product id="{$product.id_product}" img="{$product.image_mini}">
			<name>
				<![CDATA[{$product.name}]]>
			</name>
			<html>
				<![CDATA[
				<h3 style="text-align:left">{if isset($product.new) && $product.new == 1}
						<span class="new">{l_tinyMCE s='new' mod='product-list'}</span>
					{/if}
					<a href="{$product.link|escape:'htmlall':'UTF-8'}" style="text-decoration:none;">{$product.name|truncate:35:'...'|escape:'htmlall':'UTF-8'}</a>
				</h3>
				<div class="center_block" style="width:30%;float:left;">
				<a href="{$product.link|escape:'htmlall':'UTF-8'}" class="product_img_link" title="{$product.name|escape:'htmlall':'UTF-8'}"><img src="{$product.image_link|escape:'html'}" style="max-width:100%;border: 1px solid #d6d4d4;" alt="{$product.name|escape:'htmlall':'UTF-8'}" /></a>	
				</div>
				<div class="right_block" style="margin-left: 35%;padding-top:55px;">
				{if $product.on_sale}
					<span class="on_sale">{l_tinyMCE s='On sale!' mod='product-list'}</span>{/if}
				{if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0 }
					<span class="discount">
					{if version_compare($smarty.const._PS_VERSION_,'1.6','<=')} {l_tinyMCE s='Price lowered!' mod='product-list'}
					{else}{l_tinyMCE s='Reduced price!' mod='product'}
					{/if}</span>
				{/if}
				<div class="price" style="font-weight: 700; font-size: 24px;text-align: left;">{$product.price}</div>
				<br/>
				<div class="availability" style="text-align:left">
					{if ($product.allow_oosp OR $product.quantity > 0)}
						{if version_compare($smarty.const._PS_VERSION_,'1.6','<=')} {l_tinyMCE s='Available' mod='product-list'}
						{else}{l_tinyMCE s='In Stock' mod='product-list'}
						{/if}
					{else}
						{l_tinyMCE s='Out of stock' mod='product-list'}
					{/if}
				</div>
				<br/>
				<div style="text-align:left">
				{if $add_to_cart}
					{if ($product.allow_oosp OR $product.quantity > 0) && $product.customizable != 2}
						<a class="button" style="margin-right: 50px;" rel="ajax_id_product_{$product.id_product|intval}" href="{$base_dir}index.php?controller=cart&amp;add&amp;id_product={$product.id_product|intval}">{l_tinyMCE s='Add to cart' mod='product-list'}</a>
					{else}
						<span style="margin-right: 30px;" class="exclusive button">{l_tinyMCE s='Add to cart' mod='product-list'}</span>
					{/if}
				{/if}
				<a class="button" href="{$product.link|escape:'htmlall':'UTF-8'}" title="{l_tinyMCE s='View' mod='product-list'}">{l_tinyMCE s='View' mod='product-list'}</a>
				</div>
				</div>
				<p class="product_desc" style="clear: both;padding: 10px 0px;text-align: left;"><a href="{$product.link|escape:'htmlall':'UTF-8'}">{$product.description_short|strip_tags:'UTF-8'|truncate:360:'...'}</a></p>	
				]]>
			</html>
		</product>
	{/foreach}
{/if}
</return>
