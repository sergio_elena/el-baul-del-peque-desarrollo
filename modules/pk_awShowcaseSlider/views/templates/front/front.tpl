{if isset($theme_settings.hook_home_04) && ($theme_settings.hook_home_04 == 1)}
{if $theme_settings.slider01 == 1}
{if $slides|@count != 0}
    {if $minicSlider.options.front == 1 && $page_name != 'index'}
        <!-- aw_Slider -->
    {else}
        <div id="aw_slider_container">   
            <ul id="aw_slider">
                {foreach from=$slides item=image name=singleimage}
                    <li {if $smarty.foreach.singleimage.index != 0}class="inactive"{/if}>
                    {if ($image.video == 0)}
                        {if $image.url != ''}<a href="{$image.url}" {if $image.target == 1}target="_blank"{/if}>{/if}
                            <img src="{$minicSlider.path.images}{$image.image}" class="slider_image" alt="{if $image.alt}{$image.alt}{/if}"/>
                        {if $image.url != ''}</a>{/if}
                        <div class="aw_slide-text">
                            {if $image.title != ''}<h3>{$image.title}</h3>{/if}
                            {if $image.caption != ''}<div>{$image.caption}</div>{/if}
                        </div>
                        <div class="showcase-tooltips">
                            {foreach from=$coordinates item=coord}
                                {if ($image.id_lang == $coord.id_lang) && ($image.id_shop == $coord.id_shop) && ($image.id_slide == $coord.id_slide)}                                
                                    <a href="{if $coord.point_type == 'product'}{$link->getProductLink($coord.id_product, $coord.product_link_rewrite)}{else}#{/if}" coords="{$coord.coordinateX*2+13}, {$coord.coordinateY*2+13}" class="showcase-plus-anchor" style="top:{$coord.coordinateY*2}px; left:{$coord.coordinateX*2}px">
                                        <div class="point_container">
                                            {if $coord.point_type == "product"}
                                            <img src="{$coord.product_image_link}" alt="{$coord.product_name}" />
                                            <span class="aw_pName">{$coord.product_name}</span>
                                            <span class="aw_price">{displayPrice price=$coord.price}</span>
                                            {/if}
                                            {if ($coord.point_type == "text") && ($coord.point_text != NULL)}
                                                <span class="aw_text">{$coord.point_text}</span>
                                            {/if}
                                        </div>
                                    </a>                                
                                {/if}
                            {/foreach}      
                        </div>
                    {else}
                        <iframe class="videoframe" src="//www.youtube.com/embed/{$image.video_url}" frameborder="0" allowfullscreen></iframe>                        
                    {/if}                   
                    </li>
                {/foreach}                
            </ul>
        </div> 
        <script type="text/javascript">
        $(document).ready(function(){
            var video = $(".videoframe").attr("src");
            var slider = $('#aw_slider').bxSlider({   
            mode: '{if $minicSlider.options.current != ''}{$minicSlider.options.current}{else}fade{/if}', 
            speed: {if $minicSlider.options.speed != ''}{$minicSlider.options.speed}{else}1000{/if}, 
            auto: {if $minicSlider.options.manual == 1}true{else}false{/if},
            pause: {if $minicSlider.options.pause != ''}{$minicSlider.options.pause}{else}3500{/if},
            randomStart: {if $minicSlider.options.random == 1}true{else}false{/if},
            controls: {if $minicSlider.options.buttons == 1}true{else}false{/if}, 
            pager: {if $minicSlider.options.control == 1}true{else}false{/if}, 
            autoHover: {if $minicSlider.options.hover == 1}true{else}false{/if}, 
            randomStart: {if $minicSlider.options.random == 1}true{else}false{/if},
            onSlideBefore: function() {
                $(".showcase-tooltips a").hide();                
            },
            onSlideAfter: function() {                   
               $(".showcase-tooltips a").fadeIn('500');
            }
          });
          $(".showcase-plus-anchor").mouseenter(function()
            {
                var y = (parseInt($(this).css('top').replace('px', '')) + (parseInt($(this).height()))/2);
                var x = (parseInt($(this).css('left').replace('px', '')) + (parseInt($(this).width()))/2);
                var content = $(this).html();           
                slider.animateTooltip(".bx-viewport", x, y, content);

            });
            $(".showcase-plus-anchor").mouseleave(function()
            {
                var y = (parseInt($(this).css('top').replace('px', '')) + (parseInt($(this).height()))/2);
                var x = (parseInt($(this).css('left').replace('px', '')) + (parseInt($(this).width()))/2);
                var content = $(this).html();
                slider.animateTooltip(".bx-viewport", x, y, content);
            });
            
        });
        </script>   
    {/if}
{/if}
{/if}
{/if}