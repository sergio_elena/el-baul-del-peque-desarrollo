{*
* Copyright (C) 2012  S.C Minic Studio S.R.L.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
* @author     S.C Minic Studio S.R.L.
* @copyright  Copyright S.C Minic Studio S.R.L. 2012. All rights reserved.
* @license    GPLv2 License http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
* @version    v3.0.0
*}

<fieldset id="bug" class="hidden-div">
    <legend>{l s='Bug report' mod='pk_promominicslider'}</legend>
    <form id="formBug" class="" method="post">
        <div id="bugInfoCont" class="container">
            <div>
                <label>{l s='Your name' mod='pk_promominicslider'}:</label>
                <input id="bugName" class="name ghost-text" type="text" name="name" value="{$slider.info.name}" size="50" />
            </div>
            <div>
                <label>{l s='Your email' mod='pk_promominicslider'}:</label>
                <input id="bugEmail" class="ghost-text" type="text" name="email" size="50" value="{$slider.info.email}" />
            </div>
            <div>
                <label>{l s='Site address' mod='pk_promominicslider'}:</label>
                <input id="bugSite" class="ghost-text" type="text" name="domain" size="50" value="{$slider.info.domain}" />
            </div>
            <div>
                <label>{l s='Message' mod='pk_promominicslider'}:</label>
                <textarea id="bugMessage" class="message" name="bugMessage" rows="10" cols="49"></textarea>
            </div>
            <div class="button_cont">
                <input id="submitBug" type="submit" name="submitBug" value="Send" class="button green large" />
                <input id="bugPsVersion" type="hidden" value="{$slider.info.psVersion}" name="psversion" />
                <input id="bugVersion" type="hidden" value="{$slider.info.version}" name="version" />
                <input id="bugServer" type="hidden" value="{$slider.info.server}" name="server" />
                <input id="bugPhp" type="hidden" value="{$slider.info.php}" name="php" />
                <input id="bugMysql" type="hidden" value="{$slider.info.mysql}" name="mysql" />
                <input id="bugTheme" type="hidden" value="{$slider.info.theme}" name="theme" />
                <input id="bugUserInfo" type="hidden" value="{$slider.info.userInfo}" name="userInfo" />
            </div>
        </div>
        <div class="comments"> 
            <h3>{l s='Notes' mod='pk_promominicslider'}</h3>
            <p>{l s='Please describe the bug with as much detail as it`s possible, so we can understand the problem easier.' mod='pk_promominicslider'}</p>
            <h3>{l s="Important!" mod='pk_promominicslider'}</h3>
            <p>{l s='By clicking to the "Send" button you agree that we will get some basic information. If you do not wish to send your e-mail address uncheck the checkbox.' mod='pk_promominicslider'}</p>
            <ul>
                <li>{l s='Your shop`s name' mod='pk_promominicslider'}: <b>{$slider.info.name}</b></li>
                <li>{l s='Your shops e-mail address. IMPORTANT: we just need in case of communicaton!' mod='pk_promominicslider'}: <b>{$slider.info.email}</b></li>
                <li>{l s='Your shops domain' mod='pk_promominicslider'}: <b>{$slider.info.domain}</b></li>
                <li>{l s='prestashop version' mod='pk_promominicslider'}: <b>{$slider.info.psVersion}</b></li>
                <li>{l s='module version' mod='pk_promominicslider'}: <b>{$slider.info.version}</b></li>
                <li>{l s='server version' mod='pk_promominicslider'}: <b>{$slider.info.server}</b></li>
                <li>{l s='php version' mod='pk_promominicslider'}: <b>{$slider.info.php}</b></li>
                <li>{l s='mysql version' mod='pk_promominicslider'}: <b>{$slider.info.mysql}</b></li>
                <li>{l s='theme name' mod='pk_promominicslider'}: <b>{$slider.info.theme}</b></li>
                <li>{l s='browser version' mod='pk_promominicslider'}: <b>{$slider.info.userInfo}</b></li>
            </ul>
        </div>
    </form>
    <div class="response" style="display:none;">
        <span class="conf">{l s='Message sent successfull! Thank you for your time.' mod='pk_promominicslider'}</span>
        <span class="error">{l s='Something went wrong, please try again later.' mod='pk_promominicslider'}</span>
        <span class="empty">{l s='Name and Message is required!' mod='pk_promominicslider'}</span>
    </div>
</fieldset>