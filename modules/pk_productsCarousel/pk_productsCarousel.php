<?php
class pk_productsCarousel extends Module
{
	private $_html = '';
	private $_postErrors = array();

	function __construct()
	{
		$this->name = 'pk_productsCarousel';
		$this->tab = 'front_office_features';
		$this->version = '';
		$this->author = '';
		$this->need_instance = 0;
		$this->bootstrap = true;

		parent::__construct();

		$this->page = basename(__FILE__, '.php');
		$this->displayName = $this->l('Products Carousel');
		$this->description = $this->l('Displays Products Carousel in Your Homepage');
	}

	function install()
	{
		if (!Configuration::updateValue('CAROUSEL_PRODUCTS_NUMBER', 8) OR 
			!Configuration::updateValue('PRODUCTS_TYPE', 0) OR 
			!Configuration::updateValue('PRODUCTS_VISIBLE', 5) OR 
			!Configuration::updateValue('PRODUCTS_TO_SCROLL', 1) OR 
			!Configuration::updateValue('CELL_WIDTH', 164) OR 
			!parent::install() OR 
			!$this->registerHook('header') OR
			!$this->registerHook('hook_home_05')
			) return false;
		return true;
	}


	public function uninstall()
	{
		return (parent::uninstall());
	}

	public function getContent()
	{
		$output = '<div><h2>'.$this->displayName.'</h2>';
		if (Tools::isSubmit('submitHomeFeatured'))
		{
			Configuration::updateValue('PRODUCTS_TYPE', intval(Tools::getValue('products_type')));
			$nbr = intval(Tools::getValue('nbr'));
			$products_visible = intval(Tools::getValue('products_visible'));
			$products_to_scroll = intval(Tools::getValue('products_to_scroll'));
			$cell_width = intval(Tools::getValue('cell_width'));
			if (!$nbr OR $nbr <= 0 OR !Validate::isInt($nbr))
				$errors[] = $this->l('Invalid number of products');
			else
				Configuration::updateValue('CAROUSEL_PRODUCTS_NUMBER', $nbr);  /*---------------------*/
			if (!$products_visible OR $products_visible <= 0 OR !Validate::isInt($products_visible))
				$errors[] = $this->l('Invalid number of products number');
			else
				Configuration::updateValue('PRODUCTS_VISIBLE', $products_visible); /*---------------------*/
			if (!$products_to_scroll OR $products_to_scroll <= 0 OR !Validate::isInt($products_to_scroll))
				$errors[] = $this->l('Invalid number of visible products');
			else
				Configuration::updateValue('PRODUCTS_TO_SCROLL', $products_to_scroll);/*---------------------*/
			if (!$products_to_scroll OR $products_to_scroll <= 0 OR !Validate::isInt($products_to_scroll))
				$errors[] = $this->l('Invalid number of products to scroll');
			else
				Configuration::updateValue('CELL_WIDTH', $cell_width);
			if (isset($errors) AND sizeof($errors))
				$output .= $this->displayError(implode('<br />', $errors));
			else
				$output .= $this->displayConfirmation($this->l('Settings updated'));
		}
		$output .= "</div>";
		return $output.$this->displayForm();
	}

	public function displayForm()
	{
		
		$output = '
	   	<form action="'.$_SERVER['REQUEST_URI'].'" method="post">
			<fieldset><legend><img src="'.$this->_path.'logo.png" width="16" height="16" alt="" title="" />&nbsp;'.$this->l('Carousel settings').'</legend>
			<h4>Products type</h4>
				<div style="overflow:hidden">
				<label style="text-align:right; width:200px;">'.$this->l('Featured products').'</label>
					<input type="radio" name="products_type" value="1" '.((Configuration::get('PRODUCTS_TYPE') == 1) ? 'checked="checked" ' : '').'/>
				</div>
				<div style="overflow:hidden">
				<label style="text-align:right; width:200px;">'.$this->l('New products').'</label>
						<input type="radio" name="products_type" value="0" '.((Configuration::get('PRODUCTS_TYPE') == 0) ? 'checked="checked" ' : '').'/>
				</div>
				<div style="overflow:hidden">
				<label style="text-align:right; width:200px;">'.$this->l('Special products').'</label>
						<input type="radio" name="products_type" value="2" '.((Configuration::get('PRODUCTS_TYPE') == 2) ? 'checked="checked" ' : '').'/>
				</div>
				<h4>Quantity Products</h4>
				<div style="overflow:hidden">
				<label style="text-align:right; width:200px;">'.$this->l('The number of all products').'</label>
					<input type="text" size="1" name="nbr" value="'.(Configuration::get('CAROUSEL_PRODUCTS_NUMBER')).'" />	
				</div>
				
				
				<br/><br/>
				<input type="submit" name="submitHomeFeatured" value="'.$this->l('Save').'" class="button" />
			</fieldset>
      	</form>';
		return $output;
	}

	public function getImages($products, $params) {

			$productData = array();
			foreach ($products as $key => $product) {
				if (!empty($product)) {
					$imgholder = Image::getImages((int)($params['cookie']->id_lang), $product["id_product"]);				
					foreach ($imgholder as $k => $img) {					
						if ($k < 2) {						
							if ($k == 0) {
								$productData[$key]["prodCover"] = Image::getCover($product["id_product"]); // get product cover image
								
							} else {
								$productData[$key]["image"] = $img["id_image"];							
							}				
							$productData[$key]["data"] = $product;												
						}
					}	
				} else {
					$productData[$key]["data"] = "";												
				}		
			}

			return $productData;
	}

	public function hookhook_home_05($params)
	{
		$idLang = (int)($params['cookie']->id_lang);
		$category = new Category(Context::getContext()->shop->getCategory(), Configuration::get('PS_LANG_DEFAULT'));
		$nb = (int)(Configuration::get('CAROUSEL_PRODUCTS_NUMBER'));

		$orderBy = Tools::getProductsOrder('by', Tools::getValue('orderby'));
   		$orderWay = Tools::getProductsOrder('way', Tools::getValue('orderway'));

		$new = Product::getNewProducts($idLang, 0, (int)(Configuration::get('CAROUSEL_PRODUCTS_NUMBER'))); /*get new products*/		
		$featured = $category->getProducts($idLang, 1, ($nb ? $nb : 10));	 /*		get featured products	*/
		$specials = Product::getPricesDrop($idLang, 0, (Configuration::get('CAROUSEL_PRODUCTS_NUMBER')), false, $orderBy, $orderWay); /* get special products */	

		$products = "";
		switch (Configuration::get('PRODUCTS_TYPE')) {
			case 0:
				if (!empty($new))
			  	$products = $this->getImages($new, $params);
			  break;
			case 1:
				if (!empty($featured))
			  	$products = $this->getImages($featured, $params);
			  break;
			case 2:
				if (!empty($specials))
			  	$products = $this->getImages($specials, $params);
			  break;
			default:
				if (!empty($featured))
			  	$products = $this->getImages($featured, $params);
		}		

		$this->smarty->assign(array(
			'pth' => $this->_path,
			'carouselProducts' => $products,
			'products_type' => Configuration::get('PRODUCTS_TYPE'),
			'visible_products' => Configuration::get('PRODUCTS_VISIBLE'),
			'products_to_scroll' => Configuration::get('PRODUCTS_TO_SCROLL')
		));

		return $this->display(__FILE__, $this->name.'.tpl');
	}

	public function hookHeader($params)
	{
		$this->context->controller->addCSS($this->_path.'css/'.$this->name.'.css', 'all');
		$this->context->controller->addJS($this->_path.'js/jquery.flexisel.js');
		$this->context->controller->addJS($this->_path.'js/jquery.hoverdir.js');
		$this->context->controller->addJS($this->_path.'js/modernizr.custom.97074.js'); // for hover effect		
	}

	public function hookHook_home_06($params)
	{
		return $this->hookHook_home_05($params);
	}

}