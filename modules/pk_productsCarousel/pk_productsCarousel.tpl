{if $page_name == "index"}
{if $theme_settings.product_carousel == 1}
<div class="clear"></div>
<!-- jCarousel library -->
<div class="preloadImg"><div></div></div>
<div id="productsCarousel" class="main">
  <div class="container_border">
    {if $products_type == 0}
      <h4>{l s='New products' mod='productsCarousel'}</h4>
    {elseif $products_type == 1}
      <h4>{l s='Featured products' mod='productsCarousel'}</h4>
    {elseif $products_type == 2}
      <h4>{l s='Special products' mod='productsCarousel'}</h4>
    {/if}
    <div class="viewport_container">
      <div class="viewport">
        <div class="jCarouselLite">
            {if $carouselProducts}            
                <ul id="da-thumbs" class="da-thumbs carouFredSel">
                {foreach from=$carouselProducts item=product name=products}          
                  {assign var='productLink' value=$link->getProductLink($product.data.id_product, $product.data.link_rewrite)}
                  <li class="block_product ajax_block_product slide">
                    <div class="carouselContainer">
                    <a href="{$productLink}" title="{$product.data.legend}" class="slide-animate product_image_crs">
                      <img src="{$link->getImageLink($product.data.link_rewrite, "{$product.prodCover.id_product}-{$product.prodCover.id_image}", 'home_alysum')}" alt="{$product.data.name|escape:html:'UTF-8'}" class="add_to_cart_image" />
                      {if isset($product.image)}
                        <div>                    
                          <img src="{$link->getImageLink($product.data.link_rewrite, "{$product.data.id_product}-{$product.image}", 'large_alysum')}" alt="{$product.data.name|escape:html:'UTF-8'}" />                    
                        </div>
                      {/if}
                    </a>
                    <a class="f_title" href="{$productLink}" title="{$product.data.legend}">{$product.data.name|escape:htmlall:'UTF-8'|truncate:35}</a>
                    {if $product.data.show_price AND !isset($restricted_country_mode) AND !$PS_CATALOG_MODE}
                    <div class="price">                      
                      <span>{displayPrice price=$product.data.price}</span>
                      {if ($product.data.id_product_attribute == 0) && $product.data.available_for_order && !isset($restricted_country_mode) && $product.data.minimal_quantity <= 1 && $product.data.customizable != 2 && !$PS_CATALOG_MODE}
                        <a class="exclusive ajax_add_to_cart_button" rel="ajax_id_product_{$product.data.id_product}" href="{$link->getPageLink('cart.php')}?qty=1&amp;id_product={$product.data.id_product}&amp;token={$static_token}&amp;add" title="{l s='Add to cart' mod='productsCarousel'}">{l s='Add to cart' mod='productsCarousel'}</a>
                      {else}
                        <a href="{$productLink}" class="button exclusive">{l s='View' mod='productsCarousel'}</a>
                      {/if}
                    </div>
                    {/if}
                  </div>
                  </li>
                {/foreach}
              </ul>
            {else}<b>{l s='No products' mod='productsCarousel'}</b>{/if}      
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $( document ).ready(function() {
    $(function() {      
      $('#da-thumbs > li > div.carouselContainer a').each( function() { $(this).hoverdir(); } );       
      $("#da-thumbs").flexisel({
        pref: "pcarousel",
          visibleItems: 5,
          animationSpeed: 1000,
          autoPlay: true,
          autoPlaySpeed: 3000,            
          pauseOnHover: true,
          enableResponsiveBreakpoints: true,
          responsiveBreakpoints: { 
              portrait: { 
                  changePoint:480,
                  visibleItems: 1
              }, 
              landscape: { 
                  changePoint:728,
                  visibleItems: 2
              },
              tablet: { 
                  changePoint:980,
                  visibleItems: 4
              }
          }
      });
    });
    });
</script>
{/if}
{/if}