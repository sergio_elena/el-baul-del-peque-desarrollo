<!-- Block user information module NAV  -->
{if $is_logged}
	<div class="header_user_info">
		<a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" title="{l s='View my customer account' mod='blockuserinfo'}" class="account" rel="nofollow">
			<span id="no-mobile">{$cookie->customer_firstname} {$cookie->customer_lastname}</span>
			<div id="mobile"><i class="icon-user"></i></div>
		</a>
	</div>
{/if}
<div class="header_user_info">
	{if $is_logged}
		<a class="logout" href="{$link->getPageLink('index', true, NULL, "mylogout")|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log me out' mod='blockuserinfo'}">
			<span id="no-mobile">{l s='Sign out' mod='blockuserinfo'}</span>
			<div id="mobile"><i class="icon-power-off"></i></div>
	
		</a>
	{else}
		<a class="login" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log in to your customer account' mod='blockuserinfo'}">
			<span id="no-mobile">{l s='Sign in' mod='blockuserinfo'}</span>
			<div id="mobile"><i class="icon-user"></i></div>
		</a>
	{/if}
</div>
<!-- /Block usmodule NAV -->
