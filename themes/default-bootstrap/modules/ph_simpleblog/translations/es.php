<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{ph_simpleblog}default-bootstrap>ph_simpleblog_69ded4cd2da92cd7e50980ba00430dec'] = 'Blog';
$_MODULE['<{ph_simpleblog}default-bootstrap>ph_simpleblog_422d50d9e1df52672e71857b8025ef33'] = 'Añadir un blog a tu tienda prestashop';
$_MODULE['<{ph_simpleblog}default-bootstrap>ph_simpleblog_d6f47e627cb8b9186caa435aba1c32ae'] = '¿Esta seguro de borrar este módulo?';
$_MODULE['<{ph_simpleblog}default-bootstrap>ph_simpleblog_af1b98adf7f686b84cd0b443e022b7a0'] = 'Categorias';
$_MODULE['<{ph_simpleblog}default-bootstrap>ph_simpleblog_5dc52ca9ffdc26147075682c92a0a853'] = 'Entradas';
$_MODULE['<{ph_simpleblog}default-bootstrap>ph_simpleblog_189f63f277cd73395561651753563065'] = 'Etiquetas';
$_MODULE['<{ph_simpleblog}default-bootstrap>ph_simpleblog_f4f70727dc34561dfde1a3c529b6205c'] = 'Ajustes';
$_MODULE['<{ph_simpleblog}default-bootstrap>list_02159783610a72be7dff8c005031d7ed'] = 'Escrito por';
$_MODULE['<{ph_simpleblog}default-bootstrap>list_be8df1f28c0abc85a0ed0c6860e5d832'] = 'Blog';
$_MODULE['<{ph_simpleblog}default-bootstrap>list_0746618781a2a85b5edf43bd3817064b'] = 'Link a ';
$_MODULE['<{ph_simpleblog}default-bootstrap>list_43340e6cc4e88197d57f8d6d5ea50a46'] = 'Leer más';
$_MODULE['<{ph_simpleblog}default-bootstrap>list_061fcdd72d275db7234f4d3a03e3f23c'] = 'No hay entradas ';
$_MODULE['<{ph_simpleblog}default-bootstrap>list_88a11208346ae5b4ca218d90fa29eb8b'] = ' ';
$_MODULE['<{ph_simpleblog}default-bootstrap>list_9e2d8077785925fb583c0c202538b924'] = ' ';
$_MODULE['<{ph_simpleblog}default-bootstrap>list_91401f053501b716b4a695b048c9b827'] = 'Autor';
$_MODULE['<{ph_simpleblog}default-bootstrap>list_32b502f33a535f75dcbf63f6753c631e'] = 'Etiquetas';
