<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockzopim}default-bootstrap>blockzopim_575f316b893c61eac1fc7ee1506a9dfc'] = 'Bloque zopim';
$_MODULE['<{blockzopim}default-bootstrap>blockzopim_27d6d864f4723a767e3be88ca1983034'] = 'Integrar la secuencia de comandos zopim en su sitio.';
$_MODULE['<{blockzopim}default-bootstrap>blockzopim_a8bf906a35ef0cc9970ebeadb7a0a626'] = 'Por encima de todas las cosas, suscríbase a';
$_MODULE['<{blockzopim}default-bootstrap>blockzopim_f4f70727dc34561dfde1a3c529b6205c'] = 'Configuración';
$_MODULE['<{blockzopim}default-bootstrap>blockzopim_1b5758236eb07111337c754bc7556b64'] = 'Zopim chat en vivo soluciones Crea tu cuenta';
$_MODULE['<{blockzopim}default-bootstrap>blockzopim_f347b158937b11952d6602a253855710'] = 'Zopim chat en vivo soluciones';
$_MODULE['<{blockzopim}default-bootstrap>blockzopim_75d81c29140ff78050d77bfbdcad4623'] = 'Para configurar este módulo, después de su registro en';
$_MODULE['<{blockzopim}default-bootstrap>blockzopim_a291e0a15469cbdb91f1e0d7084f0c60'] = 'obtener el código para insertar el guión y buscar el identificador de su sitio en rojo negrita representan el siguiente ejemplo. Ingresa el ID.';
$_MODULE['<{blockzopim}default-bootstrap>blockzopim_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
$_MODULE['<{blockzopim}default-bootstrap>blockzopim_4c5fb985ac3acf87b49c295c67baf03d'] = 'Donación';
$_MODULE['<{blockzopim}default-bootstrap>blockzopim_9a62fe02c0e404dd2e6233a8947e983f'] = 'Mediacom87 WebAgency';
$_MODULE['<{blockzopim}default-bootstrap>blockzopim_03d2854f8732b5b60e72d15bea4d0f4b'] = 'Este módulo fue desarrollado y ofrecido generosamente a la comunidad de PrestaShop por Mediacom87 WebAgency especializada en el apoyo de comercio electrónico.';
$_MODULE['<{blockzopim}default-bootstrap>blockzopim_ce3d68e5912e70a5d9e9471324da8260'] = 'Si desea apoyar la Mediacom87 de proceso, puede hacerlo mediante una donación.';
$_MODULE['<{blockzopim}default-bootstrap>blockzopim_1aa2e2ff6972016c11c692bfb5c43909'] = 'Anuncios';
$_MODULE['<{blockzopim}default-bootstrap>blockzopim_80a941307b59f5f17e1fbd31de94848d'] = 'Usted también puede apoyar nuestra agencia haciendo clic en la publicidad de abajo';
