{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7 " lang="{$lang_iso}"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7" lang="{$lang_iso}"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8" lang="{$lang_iso}"><![endif]-->
<!--[if gt IE 8]> <html class="no-js ie9" lang="{$lang_iso}"><![endif]-->
<html lang="{$lang_iso}">
	<head>
		<meta charset="utf-8" />
		<title>{$meta_title|escape:'html':'UTF-8'}</title>
{if isset($meta_description) AND $meta_description}
		<meta name="description" content="{$meta_description|escape:'html':'UTF-8'}" />
{/if}
{if isset($meta_keywords) AND $meta_keywords}
		<meta name="keywords" content="{$meta_keywords|escape:'html':'UTF-8'}" />
{/if}
	<!-- google webmaster tools -->
	<meta name="google-site-verification" content="3pWx_8QCEvdIxT9g6xI_BixidtG-O5bqm8EPtRNMJDM" />

		<meta name="generator" content="PrestaShop" />
		<meta name="robots" content="{if isset($nobots)}no{/if}index,{if isset($nofollow) && $nofollow}no{/if}follow" />
		<meta name="viewport" content="width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0" /> 
		<meta name="apple-mobile-web-app-capable" content="yes" /> 
		<link rel="icon" type="image/vnd.microsoft.icon" href="{$favicon_url}?{$img_update_time}" />
		<link rel="shortcut icon" type="image/x-icon" href="{$favicon_url}?{$img_update_time}" />
		<!-- Font awesoms -->
		<Link  href = "http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css"  rel = "stylesheet" >
		
{if isset($css_files)}
	{foreach from=$css_files key=css_uri item=media}
		<link rel="stylesheet" href="{$css_uri|escape:'html':'UTF-8'}" type="text/css" media="{$media|escape:'html':'UTF-8'}" />
	{/foreach}
	
{/if}
{if isset($js_defer) && !$js_defer && isset($js_files) && isset($js_def)}
	{$js_def}
	{foreach from=$js_files item=js_uri}
	<script type="text/javascript" src="{$js_uri|escape:'html':'UTF-8'}"></script>
	{/foreach}
{/if}
		{$HOOK_HEADER}
		<link href='http://fonts.googleapis.com/css?family=Courgette' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="http{if Tools::usingSecureMode()}s{/if}://fonts.googleapis.com/css?family=Open+Sans:300,600" type="text/css" media="all" />
		<meta name="google-site-verification" content="3pWx_8QCEvdIxT9g6xI_BixidtG-O5bqm8EPtRNMJDM" />
		<!--[if IE 8]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->

<script languaje="javascript">

$(window).scroll(function(){
    if ($(this).scrollTop() > 120)
        $('#menuhide').fadeIn();
    else
        $('#menuhide').fadeOut();
});

$(document).ready(function(){
    $('#backtotop').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });
});

</script>
 <script type="text/javascript">
	$(document).ready(function(){
		$('.alert').click(function(){
			$('.alert').css('display','none');
		});
	)};
</script>
 
	</head>
	
	
	<body{if isset($page_name)} id="{$page_name|escape:'html':'UTF-8'}"{/if} class="{if isset($page_name)}{$page_name|escape:'html':'UTF-8'}{/if}{if isset($body_classes) && $body_classes|@count} {implode value=$body_classes separator=' '}{/if}{if $hide_left_column} hide-left-column{/if}{if $hide_right_column} hide-right-column{/if}{if isset($content_only) && $content_only} content_only{/if} lang_{$lang_iso}">

{literal}
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-THSKBP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-THSKBP');</script>
<!-- End Google Tag Manager -->
{/literal}

	{if $page_name == 'index'}
	<!-- <div id="backupslider" style="background-color:#c2c2c2;">&nbsp;</div> -->
	{/if}
	
	{if !isset($content_only) || !$content_only}
		{if isset($restricted_country_mode) && $restricted_country_mode}
			<div id="restricted-country">
				<p>{l s='You cannot place a new order from your country.'} <span class="bold">{$geolocation_country|escape:'html':'UTF-8'}</span></p>
			</div>
		{/if}
		<div id="page">
			<div class="header-container">
				<header id="header">
					<div class="banner">
						<div class="container">
							<div class="row">
								{hook h="displayBanner"}
							</div>
						</div>
					</div>
					<!-- topinfo movil -->
					
					<div id="topinfomovil">
									<div id="envio_gratis" class="topinfo_item">
										<a href="http://www.elbauldelpeque.com/es/content/8-envio"><i class="fa fa-truck"></i> <span>Envio gratis</span></a>
									</div>
									<div id="devolucion_gratis" class="topinfo_item">
										<a href="http://www.elbauldelpeque.com/es/content/11-devoluciones"><i class="fa fa-money"></i> <span>Devolución gratis</span></a>
									</div>
									<div id="pago_seguro" class="topinfo_item">
										<a href="http://www.elbauldelpeque.com/es/content/5-pago-seguro"><i class="fa fa-credit-card"></i><span> Pago seguro</span></a>
									</div>
					</div>
					
					<!-- fin topinfo movil -->
					
					<div class="nav">
						<div class="container">
							<div class="row">
								<nav>
								<div id="topinfo">
									<div id="envio_gratis" class="topinfo_item">
										<a href="http://www.elbauldelpeque.com/es/content/8-envio"><i class="fa fa-truck"></i> <span>Envio gratis</span></a>
									</div>
									<div id="devolucion_gratis" class="topinfo_item">
										<a href="http://www.elbauldelpeque.com/es/content/11-devoluciones"><i class="fa fa-money"></i> <span>Devolución gratis</span></a>
									</div>
									<div id="pago_seguro" class="topinfo_item">
										<a href="http://www.elbauldelpeque.com/es/content/5-pago-seguro"><i class="fa fa-credit-card"></i><span> Pago seguro</span></a>
									</div>
								</div>
								{hook h="displayNav"}
								</nav>
							</div>
						</div>
					</div>
					<!-- Menu movil -->
					
					<div id="menumovil">
						<div id="logomovil">
							<a href="{$base_dir}" title="{$shop_name|escape:'html':'UTF-8'}">
								<img class="logo img-responsive" src="{$logo_url}" alt="{$shop_name|escape:'html':'UTF-8'}"{if isset($logo_image_width) && $logo_image_width} width="{$logo_image_width}"{/if}{if isset($logo_image_height) && $logo_image_height} height="{$logo_image_height}"{/if}/>
							</a>
						</div>
						<div id="row" class="menumovilitem">
							{hook::exec('displayTopMovilNav')}
						</div>
					</div>
					
					<!-- fin menu movil -->
					
					<div id="menuhide" style="display:none;position:fixed;width:100%;float:left;z-index:100;background-color:#fff;margin-top:45px;">
						{hook::exec('displaySubTop')}
					</div>
					<div>
						<div class="container">
							<div class="row" style="margin-top:37px;">

							   
								<div id="header_logo" style="float:left;width:20%;">
									<a href="{$base_dir}" title="{$shop_name|escape:'html':'UTF-8'}">
										<img class="logo img-responsive" src="{$logo_url}" alt="{$shop_name|escape:'html':'UTF-8'}"{if isset($logo_image_width) && $logo_image_width} width="{$logo_image_width}"{/if}{if isset($logo_image_height) && $logo_image_height} height="{$logo_image_height}"{/if}/>
									</a>
								</div>
								<div id="topheaderY" style="float:left;width:80%;">
									{if isset($HOOK_TOP)}{$HOOK_TOP}{/if}
								
									<div id="header_phone">
										<span>¿podemos ayudarte en la compra?</span>
										<a href="tel:+34952491304"><i class="fa fa-phone"></i> 952 49 13 04</a>
										<span class="minitext">Llámanos de 11 a 13:30 y de 17:30 a 20:30, S&aacute;bados mañana</span>
									</div>
								</div>
								
								
								
							</div>
							<div class="subtop">
								{hook::exec('displaySubTop')}
							</div>
						</div>
					</div>
				</header>
			</div>

			<div id="slider_row" class="row">
					<div id="top_column" class="center_column col-xs-12 col-sm-12">{hook h="displayTopColumn"}</div>
			</div>
			<div class="columns-container">
				
				
				<div id="columns" class="container">
					{if $page_name !='index' && $page_name !='pagenotfound'}
						{include file="$tpl_dir./breadcrumb.tpl"}
					{/if}
					
					<div class="row" style="margin-top:25px;">
						{if isset($left_column_size) && !empty($left_column_size)}
						<div id="left_column" class="column col-xs-12 col-sm-{$left_column_size|intval}">{$HOOK_LEFT_COLUMN}</div>
						{/if}
						{if isset($left_column_size) && isset($right_column_size)}{assign var='cols' value=(12 - $left_column_size - $right_column_size)}{else}{assign var='cols' value=12}{/if}
						<div id="center_column" class="center_column col-xs-12 col-sm-{$cols|intval}">
	{/if}
