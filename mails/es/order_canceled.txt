
[{shop_url}] 

Hola {firstname} {lastname}, 

Su pedido con la referencia {order_name} de {shop_name} ha sido
cancelado. 

Usted puede revisar su pedido y descargar la factura desde
"Historial de pedidos" [{history_url}] de su cuenta
de cliente, haga clic en [{my_account_url}] en
nuestra tienda. 



