<?php /* Smarty version Smarty-3.1.14, created on 2016-01-10 00:55:15
         compiled from "/var/www/clients/client6/web5/web/themes/default-bootstrap/modules/blocksearch/blocksearch-top.tpl" */ ?>
<?php /*%%SmartyHeaderCode:43347566456919de3bded41-77760131%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3cebb0009d2aee8668782c9c191eb86c762f0318' => 
    array (
      0 => '/var/www/clients/client6/web5/web/themes/default-bootstrap/modules/blocksearch/blocksearch-top.tpl',
      1 => 1445472328,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '43347566456919de3bded41-77760131',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
    'search_query' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_56919de3bfdc69_93281942',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56919de3bfdc69_93281942')) {function content_56919de3bfdc69_93281942($_smarty_tpl) {?>
<!-- Block search module TOP -->
<div id="search_block_top" class="col-sm-2 clearfix" style="margin-top:4px;">
	<form id="searchbox" method="get" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('search'), ENT_QUOTES, 'UTF-8', true);?>
" >
		<input type="hidden" name="controller" value="search" />
		<input type="hidden" name="orderby" value="position" />
		<input type="hidden" name="orderway" value="desc" />
		<input class="search_query form-control" style="width:120px;float:left;" type="text" id="search_query_top" name="search_query" placeholder="<?php echo smartyTranslate(array('s'=>'Search','mod'=>'blocksearch'),$_smarty_tpl);?>
" value="<?php echo stripslashes(mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['search_query']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'));?>
" />
		<button type="submit" name="submit_search" class="btn btn-default button-search" style="height:27px;">
			<span><i class="icon-search"></i></span>
		</button>
	</form>
</div>
<!-- /Block search module TOP -->
<?php }} ?>