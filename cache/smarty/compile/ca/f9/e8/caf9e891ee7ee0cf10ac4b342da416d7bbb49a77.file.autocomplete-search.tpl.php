<?php /* Smarty version Smarty-3.1.14, created on 2016-01-10 00:55:15
         compiled from "/var/www/clients/client6/web5/web/modules/autocompletesearch/views/templates/hook/autocomplete-search.tpl" */ ?>
<?php /*%%SmartyHeaderCode:202316156356919de3b54251-13615780%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'caf9e891ee7ee0cf10ac4b342da416d7bbb49a77' => 
    array (
      0 => '/var/www/clients/client6/web5/web/modules/autocompletesearch/views/templates/hook/autocomplete-search.tpl',
      1 => 1439892135,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '202316156356919de3b54251-13615780',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'search_class_id' => 0,
    'base_dir_ssl' => 0,
    'preloader_right_position' => 0,
    'preloader_top_position' => 0,
    'minimum_characters' => 0,
    'enable_autocomplete_search' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_56919de3bb73a3_36758321',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56919de3bb73a3_36758321')) {function content_56919de3bb73a3_36758321($_smarty_tpl) {?>
		<style type="text/css">
	<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['search_class_id']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
.showLoader{
		background-image: url('<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['base_dir_ssl']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
/modules/autocompletesearch/img/preloader.gif') !important;
		background-position:  <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['preloader_right_position']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
px  <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['preloader_top_position']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
px 
		 !important;
		background-repeat: no-repeat !important;
	
	</style>
	<input type="hidden" id="autocomplete-search-url" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['base_dir_ssl']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
modules/autocompletesearch/autocomplete-controller.php">

	<script type="text/javascript">
	 jQuery(document).ready(function() {
		jQuery(document).on('focus', ':input', function() {
    	$(this).attr('autocomplete', 'off');
    });
	 	var content_result = "<div id='autocompletesearch_content_result'><div id='autocomplete_search_results'></div></div>";

	 	jQuery(content_result).insertAfter('<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['search_class_id']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
').parent('form').find('.button-search, .button');
		
	 	var minLength = '<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['minimum_characters']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
';
	 	if('<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['enable_autocomplete_search']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
' == 1) {
			jQuery('<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['search_class_id']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
').keyup(function() {
				val = jQuery(this).val();	
				if( val.length > 0 ) {
					$('<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['search_class_id']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
').addClass('showLoader');
				}
				else {
					$('<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['search_class_id']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
').removeClass('showLoader');
				}
				if(val.length < minLength) {
					$('div#autocompletesearch_content_result').hide();
				}

				if(val.length >= minLength) {
					if(('#autocomplete_search_results').length > 0 ) {
						$('div#autocompletesearch_content_result').addClass('show-border');
						$('<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['search_class_id']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
').removeClass('showLoader');
					}
					else {
						$('div#autocompletesearch_content_result').removeClass('show-border');
						$('<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['search_class_id']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
').addClass('showLoader');
					}
	 				autocompletesearch(this.value);
	 				jQuery('#autocompletesearch_content_result').show();
	 			}
	 		});


		}
	 });
	</script><?php }} ?>