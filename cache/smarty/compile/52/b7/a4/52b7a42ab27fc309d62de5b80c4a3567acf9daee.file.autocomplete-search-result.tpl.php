<?php /* Smarty version Smarty-3.1.14, created on 2016-01-10 01:00:59
         compiled from "/var/www/clients/client6/web5/web/modules/autocompletesearch/views/templates/hook/autocomplete-search-result.tpl" */ ?>
<?php /*%%SmartyHeaderCode:142038330456919f3b2c7277-53783806%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '52b7a42ab27fc309d62de5b80c4a3567acf9daee' => 
    array (
      0 => '/var/www/clients/client6/web5/web/modules/autocompletesearch/views/templates/hook/autocomplete-search-result.tpl',
      1 => 1437298378,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '142038330456919f3b2c7277-53783806',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'autocomplete_background_color' => 0,
    'search_results_top_position' => 0,
    'search_results_left_position' => 0,
    'item_heading_color' => 0,
    'item_description_color' => 0,
    'product_price_color' => 0,
    'product_old_price_color' => 0,
    'item_matching_color' => 0,
    'item_not_found_message_color' => 0,
    'result_height' => 0,
    'show_product_search' => 0,
    'searchResults' => 0,
    'matching_products_heading' => 0,
    'item' => 0,
    'link' => 0,
    'show_price' => 0,
    'PS_CATALOG_MODE' => 0,
    'restricted_country_mode' => 0,
    'priceDisplay' => 0,
    'show_description' => 0,
    'product_quantity' => 0,
    'quantityBackup' => 0,
    'show_addtocart' => 0,
    'static_token' => 0,
    'base_dir_ssl' => 0,
    'show_view_button' => 0,
    'products_error_message' => 0,
    'show_category_search' => 0,
    'searchCategory' => 0,
    'matching_category_heading' => 0,
    'category_error_message' => 0,
    'manufacturer_search' => 0,
    'searchManufacturer' => 0,
    'manufacturer_heading' => 0,
    'manufacturer_error_message' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_56919f3b58cd73_71726998',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56919f3b58cd73_71726998')) {function content_56919f3b58cd73_71726998($_smarty_tpl) {?>
<style>
	div#autocompletesearch_content_result {
		background-color: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['autocomplete_background_color']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
 !important;
		top: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['search_results_top_position']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
px;
		left: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['search_results_left_position']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
px;
		}
	div#autocomplete_search_results div.item-name > a {
		color: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item_heading_color']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
 !important;
	}
	div#autocomplete_search_results div.description {
		color: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item_description_color']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
 !important;
	}
	div#autocomplete_search_results span.actual-price {
		color: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product_price_color']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
 !important;
	}
	div#autocomplete_search_results span.old-price {
		color: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product_old_price_color']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
 !important;
	}
	div#autocomplete_search_results div.item-type {
		color: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item_matching_color']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
 !important;
	}
	div#autocomplete_search_results div.empty_result {
		color: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item_not_found_message_color']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
 !important;
	}							
</style>
<?php if ($_smarty_tpl->tpl_vars['result_height']->value=='0'){?>
<style>
div#autocomplete_search_results {
	height: auto;
{
</style>
<?php }else{ ?>
<style>
div#autocomplete_search_results {
	max-height: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['result_height']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
px;
	overflow-y: scroll;
{
</style>
<?php }?>
<div class="results-wrap">
<?php if ($_smarty_tpl->tpl_vars['show_product_search']->value=='1'){?>
	<?php if (!empty($_smarty_tpl->tpl_vars['searchResults']->value)){?>
		<div class="item-type"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['matching_products_heading']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</div>
		<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['searchResults']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
			<div class="result-item">
				<div class="item-image col-sm-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['link'];?>
" class="product-details" title="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
						<img class="item-image" src="<?php echo $_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['item']->value['link_rewrite'],$_smarty_tpl->tpl_vars['item']->value['id_image'],'medium_default');?>
" alt="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['legend'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" />					
					</a>
				</div>	
			<div class="item-name col-sm-5">
				<a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['link'];?>
" class="product_img_link" title="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
					<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>

				</a>
			</div>
			<div class="price col-sm-3">
			<?php if ($_smarty_tpl->tpl_vars['show_price']->value=='1'){?>
				<?php if (isset($_smarty_tpl->tpl_vars['item']->value['on_sale'])&&$_smarty_tpl->tpl_vars['item']->value['on_sale']&&isset($_smarty_tpl->tpl_vars['item']->value['show_price'])&&$_smarty_tpl->tpl_vars['item']->value['show_price']&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value){?>
					<span class="on_sale"><?php echo smartyTranslate(array('s'=>'On sale!','mod'=>'autocompletesearch'),$_smarty_tpl);?>
</span>
					<?php }elseif(isset($_smarty_tpl->tpl_vars['item']->value['reduction'])&&$_smarty_tpl->tpl_vars['item']->value['reduction']&&isset($_smarty_tpl->tpl_vars['item']->value['show_price'])&&$_smarty_tpl->tpl_vars['item']->value['show_price']&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value){?>
				<?php }?>
				<?php if ((!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value&&((isset($_smarty_tpl->tpl_vars['item']->value['show_price'])&&$_smarty_tpl->tpl_vars['item']->value['show_price'])||(isset($_smarty_tpl->tpl_vars['item']->value['available_for_order'])&&$_smarty_tpl->tpl_vars['item']->value['available_for_order'])))){?>
					<div class="item-price">
						<?php if (isset($_smarty_tpl->tpl_vars['item']->value['show_price'])&&$_smarty_tpl->tpl_vars['item']->value['show_price']&&!isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value)){?>
					 		<span class="actual-price" style="display: inline;">
							<?php if (!$_smarty_tpl->tpl_vars['priceDisplay']->value){?>
								<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['item']->value['price']),$_smarty_tpl);?>

							<?php }else{ ?>
								<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['item']->value['price_tax_exc']),$_smarty_tpl);?>

							<?php }?>
					 		</span>
						<?php }?>					
						<?php if ($_smarty_tpl->tpl_vars['item']->value['reduction']){?>
							<span class="old-price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['item']->value['price_without_reduction']),$_smarty_tpl);?>
</span>
						<?php }?>
					</div>				
				<?php }?>
			<?php }?>
			</div>			
			<?php if ($_smarty_tpl->tpl_vars['show_description']->value=='1'){?>
				<div class="description col-sm-8">
					<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate(strip_tags($_smarty_tpl->tpl_vars['item']->value['description_short']),180,'...');?>
			
				</div>
			<?php }?>

			<div class="cart-wrap col-sm-12">
				<div class="product-quantity col-sm-4">
				<?php if ($_smarty_tpl->tpl_vars['product_quantity']->value=='1'){?>
					<?php if (($_smarty_tpl->tpl_vars['item']->value['quantity']>0)){?>
						<input type="number" class="product-cart-quantity" min="1" name="quantity_to_cart_<?php echo intval($_smarty_tpl->tpl_vars['item']->value['id_product']);?>
" 
						id="quantity_to_cart_<?php echo intval($_smarty_tpl->tpl_vars['item']->value['id_product']);?>
" value="<?php if (isset($_smarty_tpl->tpl_vars['quantityBackup']->value)){?><?php echo intval($_smarty_tpl->tpl_vars['quantityBackup']->value);?>
<?php }else{ ?><?php if ($_smarty_tpl->tpl_vars['item']->value->minimal_quantity>1){?><?php echo $_smarty_tpl->tpl_vars['item']->value->minimal_quantity;?>
<?php }else{ ?>1<?php }?><?php }?>"/>
					<?php }?>
				<?php }?>
				</div>
				

				<div class="cart col-sm-4">
				<?php if ($_smarty_tpl->tpl_vars['show_addtocart']->value=='1'){?>
					<?php if ('customAjaxCart'=='enabled'){?>
						<?php if (($_smarty_tpl->tpl_vars['item']->value['allow_oosp']||$_smarty_tpl->tpl_vars['item']->value['quantity']>0)){?>
							<?php if (isset($_smarty_tpl->tpl_vars['static_token']->value)){?>
								<a class="button ajax_add_to_cart_button btn btn-default" href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['base_dir_ssl']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
cart?add=1&amp;id_product=<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['id_product'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" rel="nofollow" title="Add to cart" data-id-product="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['id_product'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
											<span><?php echo smartyTranslate(array('s'=>'Add to Cart','mod'=>'autocompletesearch'),$_smarty_tpl);?>
</span>
										</a>
							<?php }else{ ?>
								<a class="button ajax_add_to_cart_button btn btn-default" href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['base_dir_ssl']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
cart?add=1&amp;id_product=<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['id_product'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" rel="nofollow" title="Add to cart" data-id-product="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['id_product'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
											<span><?php echo smartyTranslate(array('s'=>'Add to Cart','mod'=>'autocompletesearch'),$_smarty_tpl);?>
</span>
										</a>									
							<?php }?>						
						<?php }else{ ?>
							<span class="exclusive"><span></span><?php echo smartyTranslate(array('s'=>'Out of stock','mod'=>'autocompletesearch'),$_smarty_tpl);?>
</span><br />
					<?php }?>
					<?php }else{ ?>
					<!-- for ps 1.5 -->
						<?php if (($_smarty_tpl->tpl_vars['item']->value['allow_oosp']||$_smarty_tpl->tpl_vars['item']->value['quantity']>0)){?>
							<?php if (isset($_smarty_tpl->tpl_vars['static_token']->value)){?>
								<a class="button ajax_add_to_cart_button exclusive btn btn-default" rel="ajax_id_product_<?php echo intval($_smarty_tpl->tpl_vars['item']->value['id_product']);?>
" id="<?php echo $_smarty_tpl->tpl_vars['item']->value['id_product'];?>
" href="javascript:void(0);"><span><?php echo smartyTranslate(array('s'=>'Add to Cart','mod'=>'autocompletesearch'),$_smarty_tpl);?>
</span></a>
							<?php }else{ ?>
								<?php if ($_smarty_tpl->tpl_vars['product_quantity']->value=='1'){?>
									<a class="button  ajax_add_to_cart_button autocomplete-ajax-cart" data-product-id="<?php echo $_smarty_tpl->tpl_vars['item']->value['id_product'];?>
">
										<span><?php echo smartyTranslate(array('s'=>'Add to Cart','mod'=>'autocompletesearch'),$_smarty_tpl);?>
</span>
									</a>
								<?php }else{ ?>
								<a class="button ajax_add_to_cart_button exclusive btn btn-default default-add-to-cart" rel="ajax_id_product_<?php echo intval($_smarty_tpl->tpl_vars['item']->value['id_product']);?>
" id="<?php echo $_smarty_tpl->tpl_vars['item']->value['id_product'];?>
" href="javascript:void(0);"><span><?php echo smartyTranslate(array('s'=>'Add to Cart','mod'=>'autocompletesearch'),$_smarty_tpl);?>
</span></a>
								<?php }?>
							<?php }?>					
						<?php }else{ ?>
							<span class="exclusive"><span></span><?php echo smartyTranslate(array('s'=>'Out of stock','mod'=>'autocompletesearch'),$_smarty_tpl);?>
</span><br />
					<?php }?>
					<!-- ps 1.5 finished -->					
					<?php }?>
			<?php }?>
			</div> <!--.cart-->

			<div class="view col-sm-4">	
			<?php if ($_smarty_tpl->tpl_vars['show_view_button']->value=='1'){?>
					<a class="button lnk_view btn btn-default"  id="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['id_product'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['link'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><span><?php echo smartyTranslate(array('s'=>'view details','mod'=>'autocompletesearch'),$_smarty_tpl);?>
</span></a>	
			<?php }?>	
			</div>
			

			</div><!--.cart-wrap-->
		</div>
		<div class="result-separator"></div>
	<?php } ?>
	
<?php }else{ ?>
	<div class="empty_result"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['products_error_message']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</div>
<?php }?>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['show_category_search']->value=='1'){?>
	<?php if (!empty($_smarty_tpl->tpl_vars['searchCategory']->value)){?>
		<div class="item-type"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['matching_category_heading']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</div>
		<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['searchCategory']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
			<div class="result-item result-item-list">
				<div class="item-image col-sm-6">
					<a href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['base_dir_ssl']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
index.php?id_category=<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['id_category'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
&controller=category">
						<img src="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['base_dir_ssl']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
/img/c/<?php echo $_smarty_tpl->tpl_vars['item']->value['id_category'];?>
.jpg" class="item-image" alt="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
					</a>
				</div>
				<div class="item-name col-sm-5">
					<a href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['base_dir_ssl']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
index.php?id_category=<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['id_category'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
&controller=category"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</a>
				</div>
				<div class="category-desc"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['description'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</div>
				<div class="clear-fix-items"></div>
			</div>
		<?php } ?>
	<?php }else{ ?>
		<div class="empty_result"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['category_error_message']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</div>
	<?php }?>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['manufacturer_search']->value=='1'){?>
	<?php if (!empty($_smarty_tpl->tpl_vars['searchManufacturer']->value)){?>
		<div class="item-type"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['manufacturer_heading']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</div>
		<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['searchManufacturer']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
		<div class="result-item result-item-list manufacturer-image">
			<div class="item-image col-sm-6 ">
				<img src="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['base_dir_ssl']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
/img/m/<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['id_manufacturer'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
-small_default.jpg" 
				alt="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
			</div>
			<div class="item-name col-sm-6">
				<a href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['base_dir_ssl']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
index.php?id_manufacturer=<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['id_manufacturer'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
&controller=manufacturer"><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</a>
			</div>
		</div>
		<?php } ?>
	<?php }else{ ?>
	<div class="empty_result"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['manufacturer_error_message']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</div>
	<?php }?>
<?php }?>
</div>

<?php }} ?>